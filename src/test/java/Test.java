import com.hb.util.HttpUtil;
import com.hbsoft.Application;

import com.hbsoft.util.WechatUtil;
import com.hbsoft.wechat.bean.ImportUserClockInInfo;
import com.hbsoft.wechat.controller.WechatAppletController;
import com.hbsoft.wechat.dao.WXUserDao;
import com.hbsoft.wechat.service.GetBattleService;
import com.hbsoft.wechat.service.WechatAppletService;
import com.hbsoft.wechat.vo.UserVo;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class Test {

    @Autowired
    WechatUtil wechatUtil;

    @Autowired
    GetBattleService getBattleService;

    @Autowired
    private WechatAppletController wechatAppletController;

    @Autowired
    private WXUserDao wxUserDao;

    @Autowired
    private WechatAppletService wechatAppletService;

    @org.junit.Test
    public void test(){
        wechatUtil.getWxAappQrCode("123");
    }

    @org.junit.Test
    public  void test2() throws  Exception{
        String path =  ResourceUtils.getURL("classpath:").getPath()+"static/qrcode/"+"s.txt";
        File file = new File(path);
        if (!file.exists()) {
            file.createNewFile();
        }
    }

    @org.junit.Test
    public void test3(){

        wechatAppletController.addSwanAndMadeQrCodeBatch();
    }



}
