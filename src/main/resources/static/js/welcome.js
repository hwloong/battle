renderDate()

$('#dateList').change(function() {
    getData()
})

Date.prototype.Format = function(fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

function renderDate() {
    $.ajax({
        url: "oa/getJXKHDWDate",
        type: "POST",
        data: {},
        success: function(res) {
            var data = res.data
            var html = ''
            for (var i = 0; i < data.length; i++) {
                html += '<option value="' + data[i] + '">' + data[i].substring(0, 10) + '</option>'
            }
            $("#dateList").html(html)
            getData()
        }
    });
}

function getData() {
    var date = $("#dateList").val()
    $.ajax({
        url: "oa/getJXKHTable",
        type: "POST",
        data: {
            businessDate: date
        },
        success: function(res) {

            if (res.code == 0) {
                var data = res.data
                renderChart(data)
            }
            console.log(res)
        }
    });
}

function renderChart(data) {
    var chart = []
    var deptList = []
    var unCompleted = []
    var completed = []
    for (var i = 0; i < data.detail.length; i++) {
        deptList.push(data.detail[i].dept)

        var all = data.detail[i].unCompleted + data.detail[i].completed


        unCompleted.push(data.detail[i].unCompleted / all*100)
        completed.push(data.detail[i].completed / all*100)
    }


    var pie = {
        center: [100, -80],
        size: 100,
        showInLegend: false,
        dataLabels: {
            enabled: true,
            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
            distance: -30,
            style: {
                fontWeight: 'normal',
                color: 'white',
                textShadow: 'normal'
            }
        },
        type: 'pie',
        name: '',
        data: [{
            name: '未打分',
            y: data.all.unCompleted
        }, {
            name: '已打分',
            y: data.all.completed
        }],

    }

    var column1 = {
        type: 'column',
        name: '未打分',
        data: unCompleted,
        dataLabels: {
            enabled: true,
            rotation: 0,
            color: '#FFFFFF',
            align: 'right',
            format: '未打分<br />{point.percentage:.0f} %', // :.1f 为保留 1 位小数
            y: 10,
            style: {
                fontWeight: 'normal',
                color: 'white',
                textShadow: 'normal'
            }
        },

    }
    var column2 = {
        type: 'column',
        name: '已打分',
        data: completed,

        dataLabels: {
            enabled: true,
            rotation: 0,
            color: '#FFFFFF',
            align: 'right',
            format: '已打分<br />{point.percentage:.0f} %', // :.1f 为保留 1 位小数
            y: 10,
            style: {
                fontWeight: 'normal',
                color: 'white',
                textShadow: 'normal'
            }
        }
    }

    chart[0] = pie
    chart[1] = column1
    chart[2] = column2


    var chart = Highcharts.chart('chart', {
        title: {
            text: '绩效考核情况统计图'
        },
        subtitle: {
            text: '上次刷新时间：' + new Date().Format("yyyy-MM-dd hh:mm:ss")
        },
        xAxis: {
            categories: deptList
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        tooltip: {
            shared: true,
            useHTML: true,
            headerFormat: '{point.key}<br>',
            pointFormat: '{series.name}:{point.percentage:.0f} % <br>',
            valueDecimals: 2
        },
        colors: [
            '#bdc3c7', //黑
            '#1abc9c', //红
            '#00FF00', //绿
            '#0000FF', //蓝
            '#FFFF00', //黄
            '#FF00FF', //紫
            '#FFFFFF', //紫
        ],
        
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        chart: {
            marginTop: 200,
        },
        credits: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        legend: {
            enabled: false
        },
        labels: {
            items: [{
                html: '汇总比例',
                style: {
                    left: '100px',
                    top: '-150px',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                }
            }]
        },
        series: chart
    });

}