var setting = {
    check: {
        enable: true,
        nocheckInherit: false
    },
    data: {
        simpleData: {
            enable: true
        }
    },
    callback: {

    }
};

var nodes = [{
    name: "条件类别",
    children: [
        { name: "条件1" },
        { name: "条件2" },
        { name: "条件3" },
        { name: "条件4" },
        { name: "条件5" },
        { name: "条件6" },
        { name: "条件7" },
        { name: "条件8" },
        { name: "条件9" },
        { name: "条件10" },
        { name: "条件11" },
        { name: "条件12" },
        { name: "条件13" },
        { name: "条件14" },
        { name: "条件15" },
        { name: "条件16" }
    ]
}];


$.fn.zTree.init($("#tree"), setting, nodes);

var text = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ';
// 注意：这里的代码只是对上面的句子进行分词并计算权重（重复次数）并构建词云图需要的数据，其中 arr.find 和 
//  reduce 函数可能在低版本 IE 中无法使用（属于ES6新增的函数），如果不能正常使用（对应的函数有报错），请自行加相应的 Polyfill
//  array.find 的 ployfill 参见：https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array/find#Polyfill
//  array.reduce 的 ployfill ：https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array/reduce#Polyfill
var data = text.split(/[,\. ]+/g)
    .reduce(function(arr, word) {
        var obj = arr.find(function(obj) {
            return obj.name === word;
        });
        if (obj) {
            obj.weight += 1;
        } else {
            obj = {
                name: word,
                weight: 1
            };
            arr.push(obj);
        }
        return arr;
    }, []);
Highcharts.chart('chartOne', {
    series: [{
        type: 'wordcloud',
        data: data
    }],
    exporting:{
        enabled:false
    },
    credits:{
        enabled:false
    },
    title: {
        text: ''
    }
});



Highcharts.addEvent(
    Highcharts.seriesTypes.networkgraph,
    'afterSetOptions',
    function(e) {
        var colors = Highcharts.getOptions().colors,
            i = 0,
            nodes = {};
        e.options.data.forEach(function(link) {
            if (link[0] === 'Proto Indo-European') {
                nodes['Proto Indo-European'] = {
                    id: 'Proto Indo-European',
                    marker: {
                        radius: 20
                    }
                };
                nodes[link[1]] = {
                    id: link[1],
                    marker: {
                        radius: 10
                    },
                    color: colors[i++]
                };
            } else if (nodes[link[0]] && nodes[link[0]].color) {
                nodes[link[1]] = {
                    id: link[1],
                    color: nodes[link[0]].color
                };
            }
        });
        e.options.nodes = Object.keys(nodes).map(function(id) {
            return nodes[id];
        });
    }
);
Highcharts.chart('chartTwo', {
    chart: {
        type: 'networkgraph',
    },
    title: {
        text: ''
    },
    plotOptions: {
        networkgraph: {
            keys: ['from', 'to'],
            layoutAlgorithm: {
                enableSimulation: true
            }
        }
    },
     exporting:{
        enabled:false
    },
    credits:{
        enabled:false
    },
    series: [{
        dataLabels: {
            enabled: true
        },
        data: [
            ['Proto Indo-European', 'Balto-Slavic'],
            ['Proto Indo-European', 'Germanic'],
            ['Proto Indo-European', 'Celtic'],
            ['Proto Indo-European', 'Italic'],
            ['Proto Indo-European', 'Hellenic'],
            ['Proto Indo-European', 'Anatolian'],
            ['Proto Indo-European', 'Indo-Iranian'],
            ['Proto Indo-European', 'Tocharian'],
        ]
    }]
});


table.render({
    elem: '#table',
    height: 312,
    url: '/demo/table/user/' //数据接口
        ,
    page: true //开启分页
        ,
    cols: [
        [ //表头
            { field: 'id', title: 'ID', width: 80, sort: true, fixed: 'left' }, { field: 'username', title: '用户名', width: 80 }, { field: 'sex', title: '性别', width: 80, sort: true }, { field: 'city', title: '城市', width: 80 }, { field: 'sign', title: '签名', width: 177 }, { field: 'experience', title: '积分', width: 80, sort: true }, { field: 'score', title: '评分', width: 80, sort: true }, { field: 'classify', title: '职业', width: 80 }, { field: 'wealth', title: '财富', width: 135, sort: true }
        ]
    ]
});