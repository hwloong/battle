var table = layui.table;

getTableData()
getTreeData()


table.on('row(table)', function(obj) {
    var flag = $("#edit").attr('disabled');
    if (flag) {
        layer.alert('请先取消上次操作')
    } else {
        var data = obj.data;
        var id = data.id_prikey

        openBtn([$('#edit')])
        getTreeDataById(id)

        $("#flag").val(id)
        obj.tr.addClass('layui-table-click').siblings().removeClass('layui-table-click');
    }


});

table.on('toolbar(table)', function(obj) {
    if (obj.event == 'edit') {
        var flag = $("#flag").val()
        if (flag == '') {
            layer.alert('请先选择一个角色');
        } else {

            openBtn([$('#save'), $('#cancel')])
            closeBtn([$('#edit')])
            $(".hb-mask").fadeOut()
        }

    } else if (obj.event == 'save') {
        var id = $("#flag").val()

        var treeObj = $.fn.zTree.getZTreeObj("tree");
        var nodes = treeObj.getCheckedNodes(true);

        var ids = []
        for (var i = 0; i < nodes.length; i++) {
            ids.push(nodes[i].id)
        }

        save(ids, id)

    } else if (obj.event == 'cancel') {
        closeBtn([$('#save'), $('#cancel')])
        openBtn([$('#edit')])

        getTableData()
        getTreeData()
        $(".hb-mask").fadeIn()
    }
});

function save(ids, id) {
    $.ajax({
        url: "setRoleMenu ",
        type: "POST",
        data: {
            ids: ids,
            id_prikey: id
        },
        success: function(res) {
            if (res.code == 0) {
                layer.alert(res.message, function() {
                    closeBtn([$('#save'), $('#cancel')])
                    openBtn([$('#edit')])
                    $(".hb-mask").fadeIn()
                    layer.closeAll();
                });
            } else {
                layer.msg(res.message);
            }
            console.log(res)
        }
    });
}

function closeBtn(dom) {
    for (var i = 0; i < dom.length; i++) {
        dom[i].addClass('layui-btn-disabled')
        dom[i].attr('disabled', true);
    }
}

function openBtn(dom) {
    for (var i = 0; i < dom.length; i++) {
        dom[i].removeClass('layui-btn-disabled')
        dom[i].attr('disabled', false);
    }
}

function getTreeDataById(id) {
    $.ajax({
        url: "getRoleMenu",
        type: "POST",
        data: {
            id_prikey: id
        },
        success: function(res) {
            if (res.code == 0) {
                var data = res.data
                var nodes = []
                var treeObj = $.fn.zTree.getZTreeObj("tree");
                for (var i = 0; i < data.length; i++) {
                    var node = treeObj.getNodesByParam("id", data[i], null);
                    nodes.push(node[0])
                }
                console.log(nodes)

                for (var i = 0; i < nodes.length; i++) {
                    treeObj.checkNode(nodes[i], true, true);
                }
            }
        }
    });
}

function getTreeData() {
    var setting = {
        check: {
            enable: true,
            nocheckInherit: false
        },
        data: {
            simpleData: {
                enable: true
            }
        },
        callback: {

        }
    };


    $.ajax({
        url: "getTreeMenu",
        type: "POST",
        data: {},
        success: function(res) {
            $.fn.zTree.init($("#tree"), setting, res.data);

            var treeObj = $.fn.zTree.getZTreeObj("tree");
            treeObj.expandAll(true);
        }
    });
}

function getTableData() {
    table.render({
        elem: '#table',
        method: 'post',
        url: 'getRolePagingData',
        height: $('.hb-page-left').height(),
        page: true,
        toolbar: '#toolbar',
        defaultToolbar: [],
        loading: true,
        size: 'sm',
        limit: 20,
        autoSort: false,
        cols: [
            [{
                    field: 'name',
                    title: '名称',
                    align: 'center',
                },
                {
                    field: 'showName',
                    title: '描述',
                    align: 'center',
                },

            ]
        ]
    })
}