var i = 0
$('#code').click(function() {
    i += 1
    $('#code').attr('src', 'getCode?' + i)
})

$(document).keydown(function(){
    if(event.keyCode==13){
        $("#login").trigger('click')
    }
});


$("#login").click(function() {
    var name = $("#name").val()
    var pass = $("#pass").val()
    var code = $("#code").val()

    if (name == '') {
        layer.alert('请输入姓名');
    } else if (pass == '') {
        layer.alert('请输入密码');
    } else if (code == '') {
        layer.alert('请输入验证码');
    } else {
        var data = {
            name: name,
            pass: $.md5(pass),
            code: code
        }
        save(data)
    }
});

function save(data) {
    $.ajax({
        url: "login",
        type: "POST",
        data: data,
        success: function(res) {
            if (res.code == 0) {
                window.location.href = "showProject";
            } else {
                layer.alert(res.msg);
            }
        }
    });
}

var i = 0
$('#codeImg').click(function() {
    i += 1
    $('#codeImg').attr('src', 'getCode?' + i)
})