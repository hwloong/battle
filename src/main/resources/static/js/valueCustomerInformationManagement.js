getTableData()

function getTableData() {
    table.render({
        elem: '#table',
        method: 'post',
        page: true,
        even: true,
        toolbar: '#toolbar',
        defaultToolbar: [],
        loading: true,
        // size: 'sm',
        autoSort: false,
        cols: [
            [
                { type: 'checkbox' },
                { type: 'numbers' },
                { field: 'name', title: '姓名', align: 'center' },
                { field: 'tel', title: '手机号码', },
                { field: 'start', title: '星级', align: 'center' },
                { field: 'sex', title: '性别', align: 'center' },
                { field: 'birthday', title: '出生日期', align: 'center' },
                { field: 'idCard', title: '身份证号', align: 'center' },
                { title: '操作', toolbar: '#bar' }
            ]
        ],
        data: [{
            name: '宋雨波',
            tel: '15702662889',
            start: '1星',
            sex: '男',
            navtion: '汉族',
            address: '山西省高平市',
            edu: '博士',
            birthday: '2018-12-27',
            marry: '未婚',
            idCard: '140581199407129859',
            homeTel: '',
            homeAddress: '大撒旦撒旦撒'
        }]

    });
}



table.on('tool(table)', function(obj) {
    if (obj.event == 'detail') {
        var dialogData = {
            type: 1,
            content: $('#detailOut'),
            area: ['1000px', '300px'],
            title: '详细信息'
        }
        openWindow(dialogData)
    } else {

    }
});


table.on('toolbar(table)', function(obj) {
    if (obj.event == 'add') {
        var dialogData = {
            type: 1,
            content: $('#addOut'),
            area: ['650px', '600px'],
            title: '价值客户信息管理档案',
            btn: ['保存', '取消'],
            yes: function(index, layero) {
                
            },
            btn2: function(index, layero) {
               
            }
        }
        openWindow(dialogData)
    }
});


$.ajax({
    url: "getProductType",
    type:"POST",
    data: {},
    success: function(res) {
    }
});

$.ajax({
    url: "getProductInfo",
    type:"POST",
    data: {},
    success: function(res) {
        var html = ''
        console.log(res.data)
        for(var i=0;i<(res.data.length/4);i++){
            html+='<tr>'
            html+='        <td><h1>存款余额</h1>'
            html+='        </td>'
            html+='        <td><h1></h1>'
            html+='        </td>'
            html+='        <td><h1>存款新增</h1>'
            html+='        </td>'
            html+='        <td><h1></h1>'
            html+='        </td>'
            html+='        <td><h1>三方存管</h1>'
            html+='        </td>'
            html+='        <td><h1></h1>'
            html+='        </td>'
            html+='        <td><h1>pos机</h1>'
            html+='        </td>'
            html+='        <td><h1></h1>'
            html+='        </td>'
            html+='    </tr>'
        }
        $('#myBank').html(html)
    }
});






function openWindow(data) {
    layer.open(data);
}