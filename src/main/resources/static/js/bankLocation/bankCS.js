$(function() {
    layui.use('table', function() {
        getData()
    })

    table.on('toolbar(table)', function(obj) {
        if (obj.event == 'search') {
            search()
        } else if (obj.event == 'add') {
            renderDialog('add')
        }else if(obj.event == 'del'){
            var checkStatus = table.checkStatus(obj.config.id);
            var data = checkStatus.data
            var ids = []
            for (var i = 0; i < data.length; i++) {
                ids.push(data[i].id_prikey)
            }
            if (ids.length == 0) {
                layer.alert('请先选择数据')
            } else {
                del(ids)
            }
        }
    });

    table.on('tool(table)', function(obj) {
        var data = obj.data;
        if (obj.event == 'update') {
            getDataById(data.id_prikey)
        } else if (obj.event == 'addDetail') {
            getDetailById(data.id_prikey, 0, 'addUserRole', '请选择要关联的角色')
        } else if (obj.event == 'removeDetail') {
            getDetailById(data.id_prikey, 1, 'removeUserRole', '请选择要取消关联的角色')
        }
    })
})

function del(ids) {
    $.ajax({
        url: "removeBankCS",
        type: "POST",
        data: {
            ids: ids
        },
        success: function(res) {
            if (res.code == 0) {
                layer.alert('删除成功', function() {
                    getData()
                    layer.closeAll();
                })
            } else {
                layer.alert(res.msg)
            }
        }
    });
}

function getDetailById(id, type, url, title) {
    $.ajax({
        url: "getUserRole",
        type: "POST",
        data: {
            id_prikey: id,
            flag: type
        },
        success: function(res) {
            if (res.code == 0) {
                var data = res.data
                renderDialogRole(data, id, url, title)
            }
        }
    });
}

function renderDialogRole(data, id, url, title) {
    var html = ''
    html += '<div class="layui-form">'
    html += '   <div class="layui-inline">'
    html += '       <label class="layui-form-label">搜索角色</label>'
    html += '       <div class="layui-input-inline">'
    html += '           <input type="text" id="value" placeholder="请输入角色名" autocomplete="off" class="layui-input">'
    html += '       </div>'
    html += '   </div>'
    html += '   <ul id="tree" class="ztree"></ul>'
    html += '</div>'
    layer.open({
        type: 1,
        area: ['400px', '500px'],
        content: html,
        title: title,
        btn: ['确定', '取消'],
        yes: function() {
            var treeObj = $.fn.zTree.getZTreeObj("tree");
            var nodes = treeObj.getCheckedNodes(true);
            var ids = []
            for (var i = 0; i < nodes.length; i++) {
                ids.push(nodes[i].id_prikey)
            }

            if (ids.length == 0) {
                layer.msg('请至少选择一个角色')
            } else {
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                        id_prikey:id,
                        ids:ids
                    },
                    success: function(res) {
                        if(res.code == 0){
                            layer.alert(res.msg,function(){
                                layer.closeAll();
                            })
                        }else{
                            layer.alert(res.msg)
                        }
                    }
                });
            }
        }
    });

    renderTree(data)

    $("#value").keyup(function(){
        var value = $(this).val()
        var dataArr = []
        for(var i=0;i<data.length;i++){
            var flag = data[i].name.indexOf(value)
            if(flag != -1){
                dataArr.push(data[i])
            }
        }
        renderTree(dataArr)
    })

   
}

function renderTree(data) {
    var setting = {
        check: {
            enable: true,
        },
        data: {
            simpleData: {
                enable: true
            },
            key: {
                name: "name"
            }
        },
        callback: {
            onClick: function() {
                var treeObj = $.fn.zTree.getZTreeObj("tree");
                var nodes = treeObj.getSelectedNodes();
                getTreeDetail(nodes[0].id)
                close()
            }
        }
    };

    $.fn.zTree.init($("#tree"), setting, data);
}


function getDataById(id) {
    $.ajax({
        url: "getBankCSById",
        type: "POST",
        data: {
            id_prikey: id
        },
        success: function(res) {
            if (res.code == 0) {
                var data = res.data
                renderDialog('edit', data.id_prikey)
                $("#csName").val(data.csName)
                $("#csCode").val(data.csCode)
                $("#csPhone").val(data.csPhone)
                $("#csStatus").val(data.csStatus)

            } else {
                layer.alert(res.msg)
            }
        }
    });
}


function renderDialog(type, id) {
    var html = ''
    html += '<div  class="layui-form" >'
    html += '    <div class="layui-form-item">'
    html += '        <label class="layui-form-label">客服名称</label>'
    html += '        <div class="layui-input-block">'
    html += '            <input type="text" id="csName" placeholder="请输入客服名称" autocomplete="off" class="layui-input">'
    html += '        </div>'
    html += '    </div>'
    html += '    <div class="layui-form-item">'
    html += '        <label class="layui-form-label">客服编号</label>'
    html += '        <div class="layui-input-block">'
    html += '            <input type="text" id="csCode" placeholder="请输入客服编号" autocomplete="off" class="layui-input">'
    html += '        </div>'
    html += '    </div>'    
    html += '    <div class="layui-form-item">'
    html += '        <label class="layui-form-label">客服手机号</label>'
    html += '        <div class="layui-input-block">'
    html += '            <input type="text" id="csPhone" placeholder="请输入客服编号" autocomplete="off" class="layui-input">'
    html += '        </div>'
    html += '    </div>'    	    	
    html += '    <div class="layui-form-item">'
    html += '        <label class="layui-form-label">状态</label>'
    html += '        <div class="layui-input-block">'
    html += '            <select id="csStatus">'
    html += '                <option value="0">启用</option>'
    html += '                <option value="1">停用</option>'
    html += '            </select>'
    html += '        </div>'
    html += '    </div>'
    html += '</div>'

    var dialogData = {
        type: 1,
        content: html,
        btn: ['保存', '取消'],
    }

    if (type == 'add') {
        dialogData.title = '添加用户'
        dialogData.yes = function() {
            add()
        }
    } else {
        dialogData.title = '修改用户'
        dialogData.yes = function() {
            edit(id)
        }
    }
    layer.open(dialogData)
    form.render()

    var num = 0
}

function edit(id) {
    var data = {
		csName: $('#csName').val(),
		csCode: $('#csCode').val(),
		csPhone: $('#csPhone').val(),
		csStatus: $('#csStatus').val(),
        id_prikey: id
    }
    var fun = 'setBankCS'
	 if ($('#csName').val() == '') {
	        layer.msg('名称不能为空')
    } else if ($('#csPhone').val() == '') {
        layer.msg('手机号不能为空')
    } else if ($('#csCode').val() == '') {
        layer.msg('编号不能为空')
    } else {
        save(data, fun)
    }
}

function add() {
	alert( $('#csStatus').val())
    var data = {
		csName: $('#csName').val(),
		csCode: $('#csCode').val(),
		csPhone: $('#csPhone').val(),
		csStatus: $('#csStatus').val()
    }
    var fun = 'addBankCS'
    if ($('#csName').val() == '') {
        layer.msg('名称不能为空')
    } else if ($('#csPhone').val() == '') {
        layer.msg('手机号不能为空')
    } else if ($('#csCode').val() == '') {
        layer.msg('编号不能为空')
    } else {
        save(data, fun)
    }
}

function save(data, fun) {
    $.ajax({
        url: fun,
        type: "POST",
        data: data,
        success: function(res) {
            if (res.code == 0) {
                layer.alert(res.msg, function() {
                    layer.closeAll();
                    getData()
                })
            } else {
                layer.alert(res.msg)
            }
        }
    });
}

function search() {
    var data = {
    	csName: $("#csNameSearch").val(),
    	csCode: $("#csCodeSearch").val(),
    	csStatus: $("#csStatusSearch").val()
    }
    table.reload("table", {
        where: data,
        done: function() {
        	 $("#csNameSearch").val(data.csName)
             $("#csCodeSearch").val(data.csCode)
             $("#csStatusSearch").val(data.csStatus)
        }
    });
}

function getData() {
    table.render({
        elem: '#table',
        method: 'post',
        url: 'getBankCSPagingData',
        height: $('.hb-page-center').height(),
        page: true,
        size: 'sm',
        defaultToolbar: [],
        loading: true,
        limit: 20,
        toolbar: '#toolbar',
        cols: [
            [{
                    checkbox: true
                }, 
                {
                    field: 'csName',
                    title: '客服姓名',
                    align: 'center',
                },
                {
                    field: 'csCode',
                    title: '客服编号',
                    align: 'center',
                },
                {
                    field: 'csPhone',
                    title: '客服手机号',
                    align: 'center',
                },
                {
                    field: 'bankLocationId',
                    title: '所属网点',
                    align: 'center',
                },
                {
                    field: 'csStatus',
                    title: '状态',
                    align: 'center',
                    templet: function(d) {
                        if (d.csStatus == 0 || d.csStatus == undefined) {
                            return "启用";
                        } else {
                            return "停用";
                        }
                    }
                },
                {
                    field: 'createTime',
                    title: '创建时间',
                    align: 'center',
                },
                {
                    field: 'right',
                    title: '工具条',
                    align: 'center',
                    toolbar: '#bar'
                }
            ]
        ]
    });
}