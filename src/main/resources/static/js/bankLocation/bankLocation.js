$(function() {
    layui.use('table', function() {
        getData()
    })

    table.on('toolbar(table)', function(obj) {
        if (obj.event == 'search') {
            search()
        } else if (obj.event == 'add') {
            renderDialog('add')
        }else if(obj.event == 'del'){
            var checkStatus = table.checkStatus(obj.config.id);
            var data = checkStatus.data
            var ids = []
            for (var i = 0; i < data.length; i++) {
                ids.push(data[i].id_prikey)
            }
            if (ids.length == 0) {
                layer.alert('请先选择一条数据')
            } else {
                del(ids)
            }
        }
    });

    table.on('tool(table)', function(obj) {
        var data = obj.data;
        if (obj.event == 'update') {
            getDataById(data.id_prikey)
        } else if (obj.event == 'addDetail') {
            getDetailById(data.bankCode, 0, 'updateBankCSBankLoaction', '请选择要关联的客服')
        } else if (obj.event == 'removeDetail') {
            getDetailById(data.bankCode, 1, 'updateBankCSBankLoaction', '请选择要取消关联的角色')
        }
    })
})

function del(ids) {
    $.ajax({
        url: "removeBankLocation",
        type: "POST",
        data: {
            ids: ids
        },
        success: function(res) {
            if (res.code == 0) {
                layer.alert('删除成功', function() {
                    getData()
                    layer.closeAll();
                })
            } else {
                layer.alert(res.msg)
            }
        }
    });
}

function getDetailById(bankCode, type, url, title) {
    $.ajax({
        url: "findAllBankCS",
        type: "POST",
        data: {
        	bankCode: bankCode,
            flag: type
        },
        success: function(res) {
            if (res.code == 0) {
                var data = res.data
                renderDialogRole(data, bankCode, url, title,type)
            }
        }
    });
}

function renderDialogRole(data, bankCode, url, title,type) {
    var html = ''
    html += '<div class="layui-form">'
    html += '   <div class="layui-inline">'
    html += '       <label class="layui-form-label">搜索客服</label>'
    html += '       <div class="layui-input-inline">'
    html += '           <input type="text" id="value" placeholder="请输入客服名称" autocomplete="off" class="layui-input">'
    html += '       </div>'
    html += '   </div>'
    html += '   <ul id="tree" class="ztree"></ul>'
    html += '</div>'
    layer.open({
        type: 1,
        area: ['400px', '500px'],
        content: html,
        title: title,
        btn: ['确定', '取消'],
        yes: function() {
            var treeObj = $.fn.zTree.getZTreeObj("tree");
            var nodes = treeObj.getCheckedNodes(true);
            var ids = []
            for (var i = 0; i < nodes.length; i++) {
                ids.push(nodes[i].id_prikey)
            }

            if (ids.length == 0) {
                layer.msg('请选择客服')
            } else {
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                    	bankCode:bankCode,
                        ids:ids,
                        flag:type
                    },
                    success: function(res) {
                        if(res.code == 0){
                            layer.alert(res.msg,function(){
                                layer.closeAll();
                            })
                        }else{
                            layer.alert(res.msg)
                        }
                    }
                });
            }
        }
    });

    renderTree(data)

    $("#value").keyup(function(){
        var value = $(this).val()
        var dataArr = []
        for(var i=0;i<data.length;i++){
            var flag = data[i].name.indexOf(value)
            if(flag != -1){
                dataArr.push(data[i])
            }
        }
        renderTree(dataArr)
    })

   
}

function renderTree(data) {
    var setting = {
        check: {
            enable: true,
        },
        data: {
            simpleData: {
                enable: true
            },
            key: {
                name: "name"
            }
        },
        callback: {
            onClick: function() {
                var treeObj = $.fn.zTree.getZTreeObj("tree");
                var nodes = treeObj.getSelectedNodes();
                getTreeDetail(nodes[0].id)
                close()
            }
        }
    };

    $.fn.zTree.init($("#tree"), setting, data);
}


function getDataById(id) {
    $.ajax({
        url: "getBankLocationById",
        type: "POST",
        data: {
            id_prikey: id
        },
        success: function(res) {
            if (res.code == 0) {
                var data = res.data
                renderDialog('edit', data.id_prikey)
                $("#bankName").val(data.bankName)
                $("#bankCode").val(data.bankCode)
                $("#bankAddr").val(data.bankAddr)
                $("#bankLocationStatus").val(data.bankLocationStatus)

            } else {
                layer.alert(res.msg)
            }
        }
    });
}


function renderDialog(type, id) {
    var html = ''
    html += '<div  class="layui-form" >'
    html += '    <div class="layui-form-item">'
    html += '        <label class="layui-form-label">网点名称</label>'
    html += '        <div class="layui-input-block">'
    html += '            <input type="text" id="bankName" placeholder="请输入网点名称" autocomplete="off" class="layui-input">'
    html += '        </div>'
    html += '    </div>'
    html += '    <div class="layui-form-item">'
    html += '        <label class="layui-form-label">网点编号</label>'
    html += '        <div class="layui-input-block">'
    html += '            <input type="text" id="bankCode" placeholder="请输入网点编号" autocomplete="off" class="layui-input">'
    html += '        </div>'
    html += '    </div>'    	
    html += '    <div class="layui-form-item">'
    html += '        <label class="layui-form-label">网点地址</label>'
    html += '        <div class="layui-input-block">'
    html += '            <input type="text" id="bankAddr" placeholder="请输入网点地址" autocomplete="off" class="layui-input">'
    html += '        </div>'
    html += '    </div>'
    html += '    <div class="layui-form-item">'
    html += '        <label class="layui-form-label">状态</label>'
    html += '        <div class="layui-input-block">'
    html += '            <select id="bankLocationStatus">'
    html += '                <option value=0>启用</option>'
    html += '                <option value=1>停用</option>'
    html += '            </select>'
    html += '        </div>'
    html += '    </div>'
    html += '</div>'

    var dialogData = {
        type: 1,
        content: html,
        btn: ['保存', '取消'],
    }

    if (type == 'add') {
        dialogData.title = '添加用户'
        dialogData.yes = function() {
            add()
        }
    } else {
        dialogData.title = '修改用户'
        dialogData.yes = function() {
            edit(id)
        }
    }

    layer.open(dialogData)

    form.render()

    var num = 0
}

function edit(id) {
    var data = {
		bankName: $('#bankName').val(),
    	bankAddr: $('#bankAddr').val(),
    	bankCode: $('#bankCode').val(),
    	bankLocationStatus: $('#bankLocationStatus').val(),
        id_prikey: id
    }
    var fun = 'setBankLocation'
    	if ($('#bankName').val() == '') {
            layer.msg('名称不能为空')
        } else if ($('#bankAddr').val() == '') {
            layer.msg('地址不能为空')
        } else if ($('#bankCode').val() == '') {
            layer.msg('编号不能为空')
        } else {
            save(data, fun)
        }
}

function add() {
    var data = {
    	bankName: $('#bankName').val(),
    	bankAddr: $('#bankAddr').val(),
    	bankCode: $('#bankCode').val(),
    	bankLocationStatus: $('#bankLocationStatus').val()
    }
    var fun = 'addBankLocal'
    if ($('#bankName').val() == '') {
        layer.msg('名称不能为空')
    } else if ($('#bankAddr').val() == '') {
        layer.msg('地址不能为空')
    } else if ($('#bankCode').val() == '') {
        layer.msg('编号不能为空')
    } else {
        save(data, fun)
    }
}

function save(data, fun) {
    $.ajax({
        url: fun,
        type: "POST",
        data: data,
        success: function(res) {
            if (res.code == 0) {
                layer.alert(res.msg, function() {
                    layer.closeAll();
                    getData()
                })
            } else {
                layer.alert(res.msg)
            }
        }
    });
}

function search() {
    var data = {
        bankName: $("#bankNameSearch").val(),
        bankCode: $("#bankCodeSearch").val(),
        bankLocationStatus: $("#bankLocationStatusSearch").val()
    }
    table.reload("table", {
        where: data,
        done: function() {
        	 $("#bankNameSearch").val(data.bankName)
             $("#bankCodeSearch").val(data.bankCode)
             $("#bankLocationStatusSearch").val(data.bankLocationStatus)
        }
    });
}

function getData() {
    table.render({
        elem: '#table',
        method: 'post',
        url: 'getBankLocationPagingData',
        height: $('.hb-page-center').height(),
        page: true,
        size: 'sm',
        defaultToolbar: [],
        loading: true,
        limit: 20,
        toolbar: '#toolbar',
        cols: [
            [{
                    checkbox: true
                }, 
                {
                    field: 'bankName',
                    title: '网点名称',
                    align: 'center',
                },
                {
                    field: 'bankCode',
                    title: '网点编号',
                    align: 'center',
                },
                {
                    field: 'bankAddr',
                    title: '网点地址',
                    align: 'center',
                },
                {
                    field: 'bankLocationStatus',
                    title: '停用',
                    align: 'center',
                    templet: function(d) {
                        if (d.bankLocationStatus == 0 || d.bankLocationStatus == undefined) {
                            return "启用";
                        } else {
                            return "停用";
                        }
                    }
                },
                {
                    field: 'createTime',
                    title: '创建时间',
                    align: 'center',
                },
                {
                    field: 'right',
                    title: '工具条',
                    align: 'center',
                    toolbar: '#bar'
                }
            ]
        ]
    });
}