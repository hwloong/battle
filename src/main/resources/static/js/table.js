var table = layui.table;
var height = $(window).outerHeight() - 70;

//第一个实例
table.render({
    elem: '#table',
    method: 'post',
    url: 'getRolePagingData',
    height: height,
    page: true,
    even: true,
    defaultToolbar: [],
    loading: true,
    limit: 20,
    autoSort: false,
    toolbar: '#toolbar',
    cols: [
        [{
                checkbox: true
            }, {
                field: 'name',
                title: '名称',
                align: 'center',
            },
            {
                field: 'showName',
                title: '描述',
                align: 'center',
            },
            {
                field: 'disabled',
                title: '停用',
                align: 'center',
                templet: function(d) {
                    if (d.disabled == 0 || d.disabled == undefined) {
                        return "启用";
                    } else {
                        return "停用";
                    }
                }
            },
            {
                field: 'right',
                title: '工具条',
                align: 'center',
                toolbar: '#bar'
            }
        ]
    ]
});


table.on('sort(table)', function(obj) {
    console.log(obj.field);
    console.log(obj.type);
    console.log(this);

});


table.on('toolbar(table)', function(obj) {
    var checkStatus = table.checkStatus(obj.config.id);
    switch (obj.event) {
        case "search":
            var data = {};
            var sendData = {};
            sendData.name = $("#selWhere input[name='name']").val();
            sendData.showName = $("#selWhere input[name='showName']").val();
            sendData.disabled = $("#selWhere select[name='disabled']").val();
            data.where = sendData;
            reload(data);
            break;
        case "add":
            layer.open({
                type: 1,
                title: '添加角色',
                content: $('#data'),
                area: ['500px', '400px'],
                btn: ['保存', '取消'],
                yes: function() {
                    $(".addDataButton").trigger('click')
                }
            })

            var data = {
                name: "",
                showName: "",
                city: "",
                id_prikey: ""
            };
            form.val('data', data);
            break;
        case "del":
            if (checkStatus.data.length == 0) {
                toast("必须选中才能删除");
            } else {
                var arr = [];
                for (var i = 0; i < checkStatus.data.length; i++) {
                    arr.push(checkStatus.data[i].id_prikey);
                }
                doAjax("removeRole", {
                    ids: arr
                }, function(data) {
                    toast(data.msg);
                    if (data.code == 0) {
                        reload();
                    }
                });
            }
            break;
    }
});

table.on('tool(table)', function(obj) {
    var data = obj.data;
    var layEvent = obj.event;
    switch (obj.event) {
        case "update":
            doAjax("getRoleById", {
                id_prikey: data.id_prikey
            }, function(data) {
                if (data.code == 0) {
                    form.val("data", data.data);
                    layer.open({
                        type: 1,
                        title: '修改角色',
                        content: $('#data'),
                        area: ['500px', '400px'],
                        btn: ['保存', '取消'],
                        yes: function() {
                            $(".addDataButton").trigger('click')
                        }
                    })
                } else {
                    toast(data.msg);
                    layer.alert(data.msg);
                }

            });
            break;
        case "detail":
            
            break
    }
})

form.on('submit()', function(data) {
    if (data.form == undefined) {
        layer.closeAll();
    } else {
        var filter = $(data.elem).attr("lay-filter");
        switch (filter) {
            case "addData":
                var senddata = data.field;
                var path = senddata.id_prikey == "" ? "addRole" : "setRole";
                doAjax(path, senddata, function(data) {
                    if (data.code == 0) {
                        layer.alert(data.msg, function(index) {
                            layer.closeAll();
                            reload();
                        });
                    } else {
                        toast(data.msg);
                    }
                });
                break;
        }
    }
    return false;
});


function reload(data) {
    table.reload("table", data);
}