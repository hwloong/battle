$('#menu h1').click(function() {
    $('#menu h1').removeClass('active')
    $(this).addClass('active')

    // $("#menu .detial").hide()
    // $(this).next().show();
})

$(".detial h2").click(function() {
    $(".detial h2").removeClass('active')
    $(this).addClass('active')
})

$("#retract").click(function() {})

$("#all").click(function() {
    $(".allMain").fadeToggle()
})

$('.model .title').click(function(){
    var flag = $(this).hasClass('active')

    if(flag == false){
        $('.model .title').removeClass('active')
        $(this).addClass('active')
    }else{
        $('.model .title').removeClass('active')
    }
})

$("#closeIndex").click(function() {
    $("#crumbs li").each(function() {
        var flag = $(this).hasClass('active')
        if (flag == true) {
            $(this).find('span').trigger('click')
            return false
        }
    })
    $("#menu li").removeClass('active')
    $(".liMain").removeClass('active')
    $(".liMain").fadeOut()
    $(".allMain").fadeOut()
})

$("#closeOther").click(function() {
    $("#crumbs li").each(function(index) {
        var flag = $(this).hasClass('active')
        if (flag == false) {
            if (index != 0) {
                $(this).remove()
            }
        }
    })
    $("iframe").each(function(index) {
        var flag = $(this).is(':hidden')
        if (flag == true) {
            if (index != 0) {
                $(this).remove()
            }
        }
    })
    $("#menu li").removeClass('active')
    $(".liMain").removeClass('active')
    $(".liMain").fadeOut()
    $(".allMain").fadeOut()
})

$("#closeAll").click(function() {
    $("#crumbs li").each(function(index) {
        if (index != 0) {
            $(this).remove()
        }
    })
    $("iframe").each(function(index) {
        if (index != 0) {
            $(this).remove()
        }
    })
    $("#menu li").removeClass('active')
    $("#crumbs li").eq(0).trigger('click')
    $(".liMain").removeClass('active')
    $(".liMain").fadeOut()
    $(".allMain").fadeOut()
})

$("#pre").click(function() {
    $("#crumbs li").each(function(index) {
        var flag = $(this).hasClass('active')
        if (flag == true) {
            $(this).prev().trigger('click')
            return false
        }
    })
    $("#menu li").removeClass('active')
    $(".liMain").removeClass('active')
    $(".liMain").fadeOut()
})

$("#next").click(function() {
    $("#crumbs li").each(function(index) {
        var flag = $(this).hasClass('active')
        if (flag == true) {
            $(this).next().trigger('click')
            return false
        }
    })
    $("#menu li").removeClass('active')
    $(".liMain").removeClass('active')
    $(".liMain").fadeOut()
})


$("#changePwd").click(function() {
    layer.open({
        type: 1,
        content: $('#changePwdDialog'),
        area: ['400px', '300px'],
        btn: ['确定', '取消'],
        title: '修改密码',
        yes: function(index, layero) {
            layer.closeAll()
        }
    });
})

function getActiveTabName(title) {
    return title
}