/**
 * 当前显示的选项卡id
 */
var defId = "consoleMain";
var showTabLayId = defId;
/**
 * 左侧菜单切换
 */
element.on('nav(menu)', function(elem) {

	var href = elem.attr("lay-href") == undefined ? "" : elem.attr("lay-href").trim();
	if(href != "") {
		var text = elem.text().trim();
		if($(".layui-tab-title li[lay-id='" + href + "']").length == 0) {
			if($(".layui-tab-title li").length > 6) {
				var layId = $(".layui-tab-title li:eq(1)").attr("lay-id");
				element.tabDelete("tabs", layId);
			}
			element.tabAdd('tabs', {
				title: text,
				id: href
			});
			$("#body-iframe").append('<div class="layadmin-tabsbody-item"><iframe src="' + href + '" frameborder="0" class="layadmin-iframe"></iframe></div>');
		}
		element.tabChange('tabs', href);
		$("#leftMenu").removeClass("layadmin-side-spread-sm");
	}
});

/**
 * 头部左侧按钮
 */
element.on('nav(top-left)', function(elem) {
	var title = elem.attr("title");
	switch(title) {
		case "侧边伸缩":
			var width = $(window).width();
			if($("#leftMenu").hasClass("layadmin-side-shrink") == false) {
				if(width > 992) {
					$("#leftMenu").addClass("layadmin-side-shrink")
				} else {
					$("#leftMenu").addClass("layadmin-side-spread-sm")
				}
			} else {
				$("#leftMenu").removeClass("layadmin-side-shrink");
				$("#leftMenu").removeClass("layadmin-side-spread-sm");
			}
			break;
		case "刷新":
			$(".layui-show iframe").attr("src", $(".layui-show iframe").attr("src"));
			break;
		case "分页条数设置":
			openPage("123","分页条数设置",1);
			console.log(123);
			break;
	}
});

/**
 * 头部右侧按钮
 */
element.on('nav(top-right)', function(elem) {
	var title = elem.attr("title");
	switch(title) {
		case "侧边伸缩":
			var obj = $("#leftMenu");
			obj.hasClass("layadmin-side-shrink") ? obj.removeClass("layadmin-side-shrink") : obj.addClass("layadmin-side-shrink");
			break;
		case "刷新":
			$(".layui-show iframe").attr("src", $(".layui-show iframe").attr("src"));
			break;
		default:
			break;
	}
});

/**
 * 选项卡按钮
 */
element.on('nav(pagetabs-nav)', function(elem) {
	var filter = elem.attr("lay-filter");
	switch(filter) {
		case "closeThisTabs":
			if(showTabLayId != defId)
				element.tabDelete("tabs", showTabLayId);
			break;
		case "closeOtherTabs":
			$(".layui-tab-title li[lay-id!='" + defId + "'][lay-id!='" + showTabLayId + "']").each(function() {
				element.tabDelete("tabs", $(this).attr("lay-id"));
			});
			break;
		case "closeAllTabs":
			$(".layui-tab-title li[lay-id!='" + defId + "']").each(function() {
				element.tabDelete("tabs", $(this).attr("lay-id"));
			});
			break;
	}
});

$(".layui-icon-prev").click(function() {
	calcTabWidth("left");
});

$(".layui-icon-next").click(function() {
	calcTabWidth();
});

/**
 * 选项卡切换
 */
element.on('tab(tabs)', function(data) {
	var id = $(this).attr("lay-id");
	$(".menu-list").removeClass("layui-this");
	$(".menu-list a[lay-href='" + id + "']").parent().addClass("layui-this");
	$(".layadmin-tabsbody-item").removeClass("layui-show");
	$(".layadmin-tabsbody-item iframe[src='" + id + "']").parent().addClass("layui-show");
	showTabLayId = id;
	calcTabWidth("auto", data.index);
});

/**
 * 选项卡删除
 */
element.on('tabDelete(tabs)', function(data) {
	var exisTabIds = [];
	$(".layui-tab-title li").each(function() {
		exisTabIds.push($(this).attr("lay-id"));
	})
	if(exisTabIds.length > 0) {
		var evalStr = '$(".layadmin-tabsbody-item iframe';
		for(var i = 0; i < exisTabIds.length; i++) {
			evalStr += "[src!='" + exisTabIds[i] + "']";
		}
		evalStr += '").parent().remove()';
		eval(evalStr);
	}
});

/**
 * 手机端按遮罩关闭左侧菜单
 */
$(".layadmin-body-shade").click(function() {
	$("#leftMenu").removeClass("layadmin-side-spread-sm");
});

/**
 * 
 * @param {String} type	类型
 * @param {int} index	下标
 */
function calcTabWidth(type, index) {
	var obj = $(".layui-tab-title"),
		objLi = obj.children("li"),
		len = (obj.prop("scrollWidth"), obj.outerWidth()),
		cssLeftNum = parseFloat(obj.css("left"));

	if("left" == type) {
		objLi.each(function(e, i) {
			var l = $(i).position().left;
			if(l + $(i).outerWidth() < len - cssLeftNum) {
				return obj.css("left", l), !1
			}
		});
	} else {
		"auto" === type ? ! function() {
			var liIndex = objLi.eq(index);
			if(liIndex[0]) {
				obj.css("left", 0);
				var liLeftNum = liIndex.position().left;
				if(liLeftNum + liIndex.outerWidth() >= len - cssLeftNum) {
					var o = liLeftNum + liIndex.outerWidth() - (len - cssLeftNum);
					objLi.each(function(e, i) {
						var l = $(i).position().left;
						if(l + cssLeftNum > 0 && l - cssLeftNum > o) {
							return obj.css("left", -l), !1
						}
					});
				}
			}
		}() : objLi.each(function(e, i) {
			var r = $(i).position().left;
			if(r + $(i).outerWidth() >= len - cssLeftNum) {
				return obj.css("left", -r), !1
			}
		});
	}

}