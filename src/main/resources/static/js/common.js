$(function() {
    layoutCenter()
    $("menuitem li h2").click(function() {
        $('menuitem li h2').removeClass('active')
        $(this).addClass('active')
    })


    renderCss()

})

function login() {
    var name = $("#name").val()
    var pass = $("#pass").val()
    var code = $("#code").val()

    if (name == '') {
        layer.alert('请输入姓名');
    } else if (pass == '') {
        layer.alert('请输入密码');
    } else if (code == '') {
        layer.alert('请输入验证码');
    } else {
        var data = {
            name: name,
            pass: $.md5(pass),
            code: code
        }

        $.ajax({
            url: "login",
            type: "POST",
            data: data,
            success: function(res) {
                if (res.code == 0) {
                    window.location.href = "showProject";
                } else {
                    layer.alert(res.msg);
                    changeCode($('#codeImg'))
                }
            }
        });
    }
}

function changeCode(dom) {
    $(dom).attr('src', 'getCode?' + Math.random())
}

function renderCss() {
    var jq = top.jQuery;
    var href = jq('#common').attr('href')
    var html = '<link rel="stylesheet" type="text/css" href="' + href + '" id="common">'
    var flag = $("#common")
    $('head').append(html)
}

function themeShow() {
    $('theme').fadeToggle()
}

function changeTheme(dom) {
    var type = $(dom).attr('data-type')
    $('#common').attr('href', 'css/common' + type + '.css');
    var str = $("#common").attr('href')
    var nameArr = []
    $('iframe').each(function() {
        var name = $(this).attr('name')
        nameArr.push(name)
    })

    for (var i = 0; i < nameArr.length; i++) {
        $(window.frames[nameArr[i]].document).find("#common").attr('href', str)
    }
    $("theme").fadeOut()



    $.ajax({
        url: "setSkin",
        type: "POST",
        data: {
            skinName: type
        },
        success: function(res) {
            console.log(res)
        }
    });

    renderCss()



}

function layoutCenter() {
    var top = $(".hb-page-top")
    var left = $(".hb-page-left")
    var right = $(".hb-page-right")
    var center = $(".hb-page-center")
    var bottom = $(".hb-page-bottom")
    var all = $('.hb-page')

    if (top) {
        var topHeight = top.outerHeight()
    } else {
        var topHeight = 0
    }

    if (left) {
        var leftWidth = left.outerWidth()
    } else {
        var leftWidth = 0
    }

    if (right) {
        var rightWidth = right.outerWidth()
    } else {
        var rightWidth = 0
    }

    if (bottom) {
        var bottomHeight = bottom.outerHeight()
    } else {
        var bottomHeight = 0
    }

    if (all) {
        var allHeight = all.outerHeight()
        var allWeight = all.outerWidth()
    } else {
        var allHeight = 0
        var allWeight = 0
    }
    console.log(all.html())
    center.css({
        'width': allWeight - leftWidth - rightWidth,
        'height': allHeight - topHeight - bottomHeight,
        'top': topHeight,
        'left': leftWidth
    })
    left.css({
        'height': allHeight - topHeight - bottomHeight,
        'top': topHeight
    })
}

function toggle(dom) {
    $(dom).parents('menuitem').find('ul').toggle()
}

function changePwd() {
    var html = ''
    html += '    <div class="layui-form" lay-filter="changePwd">'
    html += '        <div class="layui-form-item">'
    html += '            <label class="layui-form-label">原密码</label>'
    html += '            <div class="layui-input-block">'
    html += '                <input type="password"  id="oloPass" placeholder="请输入原密码" autocomplete="off" class="layui-input">'
    html += '            </div>'
    html += '        </div>'
    html += '        <div class="layui-form-item">'
    html += '            <label class="layui-form-label">新密码</label>'
    html += '            <div class="layui-input-block">'
    html += '                <input type="password"  id="newPass" placeholder="请输入新密码" autocomplete="off" class="layui-input">'
    html += '            </div>'
    html += '        </div>'
    html += '        <div class="layui-form-item">'
    html += '            <label class="layui-form-label">确认密码</label>'
    html += '            <div class="layui-input-block">'
    html += '                <input type="password"  id="newPass1" placeholder="请确认密码" autocomplete="off" class="layui-input">'
    html += '            </div>'
    html += '        </div>'
    html += '    </div>'


    layer.open({
        type: 1,
        content: html,
        area: ['400px', '300px'],
        btn: ['确定', '取消'],
        title: '修改密码',
        yes: function(index, layero) {
            var oloPass = $('#oloPass').val()
            var newPass = $('#newPass').val()
            var newPass1 = $('#newPass1').val()



            if (oloPass == '') {
                layer.msg('请输入原密码');
                return false
            } else if (newPass == '') {
                layer.msg('请输入新密码');
                return false
            } else if (newPass1 == '') {
                layer.msg('请确认密码');
                return false
            } else if (newPass != newPass1) {
                layer.msg('两次新密码输入不一致');
                return false
            }

            $.ajax({
                url: "setUserPass",
                type: "POST",
                data: {
                    oloPass: $.md5(oloPass),
                    newPass: $.base64.encode(newPass),
                    newPass1: $.base64.encode(newPass1)
                },
                success: function(res) {
                    if (res.code == 0) {
                        layer.alert(res.msg, function() {
                            layer.closeAll()
                        });
                    } else {
                        layer.msg(res.msg);
                    }
                }
            });

        }
    });

}

function next() {
    $("crumbsitem").each(function(index) {
        var flag = $(this).hasClass('active')
        if (flag == true) {
            $(this).next().trigger('click')
            return false
        }
    })
    $("crumbsAll ol").fadeOut()
}

function pre() {
    $("crumbsitem").each(function(index) {
        var flag = $(this).hasClass('active')
        if (flag == true) {
            $(this).prev().trigger('click')
            return false
        }
    })
    $("crumbsAll ol").fadeOut()
}

function showCrumbDetail() {
    $("crumbsAll ol").fadeToggle()
}

function closeAll() {
    $("crumbsitem").each(function(index) {
        if (index != 0) {
            $(this).find('span').trigger('click')
        }
    })
}

function closeIndex() {
    $("crumbsitem").each(function() {
        var flag = $(this).hasClass('active')
        if (flag == true) {
            $(this).find('span').trigger('click')
            return false
        }
    })
}

function closeOther() {
    $("crumbsitem").each(function(index) {
        var flag = $(this).hasClass('active')
        if (flag == false) {
            if (index != 0) {
                $(this).find('span').trigger('click')
            }
        }
    })
}

function inSystem(id) {
    $.ajax({
        url: "getLoginSystemInfo",
        type: "POST",
        data: {
            menuId: id
        },
        success: function(res) {
            if (res.code == 0) {
                var jumpMode = res.data.jumpMode
                if (jumpMode == 'frame') {
                    window.location.href = 'home'
                } else {
                    window.location.href = res.data.url
                }
            } else {
                layer.alert(res.message);
            }
        }
    });
}

function addTab(title, url) {
    var jq = top.jQuery;
    var crumbsArr = []
    jq("crumbsitem").each(function() {
        crumbsArr.push(jq(this).find('b').text())
    });
    for (var i = 0; i < crumbsArr.length; i++) {
        if (crumbsArr[i] == title) {
            jq("crumbsitem").eq(i).trigger('click')
            return false
        }
    }

    jq("crumbsitem").removeClass('active')

    var item = ''
    item += '<crumbsitem class="active">'
    item += '    <h1><b>' + title + '</b><span>✖</span></h1>'
    item += '</crumbsitem>'
    jq("crumbsList").append(item)

    jq("main iframe").hide()

    var page = ''
    page += '<iframe name="' + title + '" src="' + url + '"></iframe>'
    jq("main").append(page)

    jq("crumbsitem span").click(function() {
        var text = $(this).parents('crumbsitem').find('b').text()
        jq('main iframe').each(function() {
            var flag = jq(this).attr('name')
            if (flag == text) {
                jq(this).remove()
            }
        })
        jq(this).parents('crumbsitem').remove()

        jq("crumbsitem").last().trigger('click')
    });



    jq("crumbsitem").click(function() {
        jq("crumbsitem").removeClass('active')
        jq("main iframe").hide()
        jq(this).addClass('active')
        var text = jq(this).find('b').text()

        jq("main iframe").each(function() {
            var flag = jq(this).attr('name')
            if (flag == text) {
                jq(this).show()
                return false
            }
        });
    });



    var len = jq('crumbsitem').length
    if (len == 7) {
        jq("crumbsitem").eq(1).find('span').trigger('click')
    }



}