<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="css/layui.css" media="all">
    <link rel="stylesheet" type="text/css" href="css/common${hb_user.attr}.css" id="common">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/animate.min.css">
    <script src="js/layui.all.js"></script>
    <script src="js/layui.config.js"></script>
    <script src="js/jquery.md5.js"></script>
    <script src="js/jquery.base64.js"></script>
</head>

<body>
    <div class="hb-container">
        <nav>
            <ul>
                <li>
                    <h1><img src="icon/welcome.png" alt="">欢迎您：${hb_user.showName}</h1>
                </li>
                <li onclick="changePwd()">
                    <h1><img src="icon/editPwd.png" alt="" >修改密码</h1>
                </li>
                <li onclick="themeShow()">
                    <h1><img src="icon/theme.png" alt="" >切换主题</h1>
                </li>
                <a href="showProject">
                    <li>
                        <h1><img src="icon/changeSystem.png" alt="">切换系统</h1>
                    </li>
                </a>
                </li>
                <a href="closeUser">
                    <li>
                       <h1><img src="icon/signOut.png" alt="">退出登录</h1>
                    </li>
                </a>
            </ul>
        </nav>
        <crumbs>
            <prev onclick="pre()">
                <h1><span class="fa fa-angle-double-left fa-fw"></span></h1>
            </prev>
            <crumbsList>
                <crumbsitem class="active">
                    <h1><b>首页</b></h1>
                </crumbsitem>
            </crumbsList>
            <next onclick="next()">
                <h1><span class="fa fa-angle-double-right fa-fw"></span></h1>
            </next>
            <crumbsAll onclick="showCrumbDetail()">
                <h1 ><span class="fa fa-list fa-fw"></span></h1>
                <ol hidden="">
                    <li onclick="closeIndex()"><h2>关闭当前标签页</h2>
                    </li>
                    <li onclick="closeOther()"><h2>关闭其他标签页</h2>
                    </li>
                    <li onclick="closeAll()"><h2>关闭全部标签页</h2>
                    </li>
                </ol>
            </crumbsAll>
            <theme hidden>
                <themeItem onclick="changeTheme(this)" title="草地" data-type="-grass"></themeItem>
                <themeItem onclick="changeTheme(this)" title="木质" data-type="-wood"></themeItem>
                <themeItem onclick="changeTheme(this)" title="中秋节" data-type="-month"></themeItem>
                <themeItem onclick="changeTheme(this)" title="守护精灵" data-type="-spirit"></themeItem>
                <themeItem onclick="changeTheme(this)" title="闲云野鹤" data-type="-cloud"></themeItem>
                <themeItem onclick="changeTheme(this)" title="默认" data-type=""></themeItem>
            </theme>
        </crumbs>
        <menu>
            <div class="logo">
                <!-- <img src="img/logo.png" alt="" class="showBig"> -->
            </div>
                <#list currentOneMenu as oneMenu>
                    <menuitem>
                    <div class="title" onclick="toggle(this)">
                        <h1><span class="${oneMenu.treeIcon!''}"></span><b>${oneMenu.name}</b></h1>
                    </div>
                    <ul hidden="">
                        <#list currentTowMenu as towMenu>
                            <#if oneMenu.id==towMenu.id?substring(0,oneMenu.id?length)>
                                <li>
                                    <h2 onclick="addTab('${towMenu.name}','${currentProjectUrl}${currentProjectName}${towMenu.urlAddress}')"><span class="${towMenu.treeIcon!''}"></span>${towMenu.name}</h2>
                                </li>
                            </#if>
                        </#list>
                    </ul>
                    </menuitem>
                </#list>
        </menu>
        <main>
            <iframe src="consoleMain" name="首页"></iframe>
        </main>
    </div>
    <script src="js/common.js"></script>

</body>

</html>