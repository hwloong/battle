package com.hbsoft.wechat.controller;

import com.hb.controller.ABaseController;
import com.hbsoft.wechat.bean.OutbreakLog;
import com.hbsoft.wechat.bean.WXUser;
import com.hbsoft.wechat.service.OutbreakLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
public class OutbreakLogController extends ABaseController {
    private OutbreakLogService outbreakLogService;

    @Autowired
    public OutbreakLogController(OutbreakLogService outbreakLogService) {
        this.outbreakLogService = outbreakLogService;
    }

    /**
     * 添加疫情日报
     *
     * @param outbreakLog 日报信息
     * @return
     * @throws Exception
     */
    @RequestMapping("addOutbreakLog")
    public String addOutbreakLog(@Validated OutbreakLog outbreakLog) throws Exception {
        outbreakLog.setCreateDateTime(new Date());
        WXUser user = getWXUser();
        return gson.toJson(outbreakLogService.addOutbreakLog(outbreakLog, user));
    }

    /**
     * 获取我自己的申报记录
     *
     * @param outbreakLog 申报信息
     * @return
     * @throws Exception
     */
    @RequestMapping("getMyPagingOutbreakLog")
    public String getMyPagingOutbreakLog(OutbreakLog outbreakLog) throws Exception {
        Map<String, Object> result = new HashMap<>();
        if (outbreakLog.getDeptId() == null) {
            result.put("code", 400);
            result.put("msg", "单位id不能为空");
            result.put("message", "单位id不能为空");
        } else if (outbreakLog.getUserId() == null) {
            result.put("code", 400);
            result.put("msg", "人员id不能为空");
            result.put("message", "人员id不能为空");
        } else {
            WXUser user = getWXUser();
            result = outbreakLogService.getMyPagingOutbreakLog(outbreakLog, user);
        }
        return gson.toJson(result);
    }

    @RequestMapping("getMyPagingOutbreakLogAdmin")
    public String getMyPagingOutbreakLogAdmin(OutbreakLog outbreakLog) throws Exception{
        Map<String, Object> result = new HashMap<>();

        return gson.toJson(result);
    }

    /**
     * 获取微信用户
     *
     * @return
     */
    private WXUser getWXUser() {
        WXUser wxUser = (WXUser) session.getAttribute("wxUser");
        if (wxUser == null) {
            throw new RuntimeException("未注册不能添加部门信息");
        }
        if (isEmpty(wxUser.getPhone())) {
            throw new RuntimeException("注册信息手机号为空，不能添加部门信息");
        }
        return wxUser;
    }
}
