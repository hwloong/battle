package com.hbsoft.wechat.controller;

import com.hb.annotation.LoginNotRequired;
import com.hb.bean.CallResult;
import com.hb.controller.ABaseController;
import com.hbsoft.annotation.WeChatRequired;
import com.hbsoft.wechat.bean.Clock;
import com.hbsoft.wechat.bean.PerOrgan;
import com.hbsoft.wechat.dao.service.ClockDaoService;
import com.hbsoft.wechat.dao.service.PerOrganDaoService;
import com.hbsoft.wechat.dao.service.PersonDaoService;
import com.hbsoft.wechat.service.ClockService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(produces={"application/json;charset=UTF-8"})
public class ClockController extends ABaseController {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    ClockDaoService clockDaoService;

    @Autowired
    ClockService clockService;

    @Autowired
    PersonDaoService personDaoService;

    @Autowired
    PerOrganDaoService perOrganDaoService;
    /**
     * 打卡
     * @param clock
     * @return
     * @throws Exception
     */
    @LoginNotRequired
    @RequestMapping("clockIn")
    @WeChatRequired
    public String clockIn(Clock clock) throws Exception {
        CallResult<String> result = new CallResult<>();
        logger.info("打卡开始");
        try {
            if (isEmpty(clock.getClockLocal())) {
                result.setCode(400);
                result.setMessage("打卡地点不能为空");
                return gson.toJson(result);
            }
            if (null == clock.getLatitude()) {
                result.setCode(400);
                result.setMessage("纬度不能为空");
                return gson.toJson(result);
            }if (null == clock.getLongitude()) {
                result.setCode(400);
                result.setMessage("经度不能为空");
                return gson.toJson(result);
            }if (isEmpty(clock.getOpendId())) {
                result.setCode(400);
                result.setMessage("微信id不能为空");
                return gson.toJson(result);
            }
            if (null == clock.getTemperature()) {
                result.setCode(400);
                result.setMessage("体温不能为空");
                return gson.toJson(result);
            }

            PerOrgan perOrgan = new PerOrgan();
            perOrgan.setPersonId(clock.getOpendId());
            List<PerOrgan> list = perOrganDaoService.getAll(perOrgan);
            if (null != list && list.size() > 0) {
                clock.setClockTime(new Date());
                clockDaoService.add(clock);
                result.setCode(0);
                result.setMessage("打卡成功");
            }else {
                result.setCode(400);
                result.setMessage("请先加入组织");
            }
        }catch (Exception e) {
            result.setCode(400);
            result.setMessage("打卡失败");
            logger.info("打卡失败" + e.getMessage(),e);
            e.printStackTrace();
        }
        logger.info("打卡结束");
        return gson.toJson(result);
    }

    /**
     * 获取打卡记录
     * @param clock
     * @return
     */
    @LoginNotRequired
    @RequestMapping("findClockInfo")
    @WeChatRequired
    public String findClockInfo(Clock clock) {
        CallResult<Map<String,Object>> result = new CallResult<>();
        logger.info("查询打卡记录开始");
        try {
            if (isEmpty(clock.getOpendId())) {
                result.setCode(400);
                result.setMessage("微信id不能为空");
                return gson.toJson(result);
            }
            Map<String,Object> map = clockDaoService.getPagingData(clock);
            if (null != map && (Integer)map.get("count") > 0) {
                result.setCode(0);
                result.setMessage("获取成功");
                result.setData(map);
                return gson.toJson(result);
            }else {
                result.setCode(400);
                result.setMessage("暂无卡达记录");
                return gson.toJson(result);
            }
        }catch (Exception e) {
            result.setCode(400);
            result.setMessage("获取打卡记录失败");
            logger.error("获取打卡记录失败" + e.getMessage(),e);
            e.printStackTrace();
        }
        logger.info("获取打卡记录结束");
        return gson.toJson(result);
    }

}
