package com.hbsoft.wechat.controller;

import com.hb.bean.CallResult;
import com.hb.controller.ABaseController;
import com.hbsoft.wechat.bean.DeptReturnWork;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DeptReturnWorkController extends ABaseController {
    /**
     * 添加返工备案表
     *
     * @param deptReturnWork 返工备案表信息
     * @return
     * @throws Exception
     */
    @RequestMapping("addDeptReturnWork")
    public String addDeptReturnWork(DeptReturnWork deptReturnWork) throws Exception {
        CallResult<String> result = new CallResult<>();
        return gson.toJson(result);
    }
}
