package com.hbsoft.wechat.controller;

import com.hb.annotation.LoginNotRequired;
import com.hb.bean.CallResult;
import com.hb.controller.ABaseController;
import com.hbsoft.annotation.WeChatRequired;
import com.hbsoft.wechat.bean.Organize;
import com.hbsoft.wechat.bean.WXUser;
import com.hbsoft.wechat.dao.service.OrganizeDaoService;
import com.hbsoft.wechat.dao.service.WXUserDaoService;
import com.hbsoft.wechat.service.OrganizeService;
import com.hbsoft.wechat.service.PersonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping(produces={"application/json;charset=UTF-8"})
public class OrganizeController extends ABaseController {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    OrganizeDaoService organizeDaoService;

    @Autowired
    OrganizeService organizeService;

    @Autowired
    PersonService personService;

    @Autowired
    WXUserDaoService wXUserDaoService;

    /**
     * 创建组织
     * @param organize
     * @return
     */
    @LoginNotRequired
    @RequestMapping("createOrganize")
    @WeChatRequired
    public String createOrganize(Organize organize) throws Exception {
        CallResult<String> result = new CallResult<>();
        logger.info("创建组织开始");
        if (isEmpty(organize.getOpendId())) {
            result.setCode(400);
            result.setMessage("微信id不能为空");
            return gson.toJson(result);
        }
        if (isEmpty(organize.getUnitName())) {
            result.setCode(400);
            result.setMessage("组织名称不能为空");
            return gson.toJson(result);
        }
        List<Organize> lists = organizeDaoService.getAll(organize);
        if (null != lists && lists.size() > 0) {
            result.setCode(400);
            result.setMessage("已有该组织");
            return gson.toJson(result);
        }
        result = organizeService.addOrganize(organize);
        logger.info("创建组织结束");
        return gson.toJson(result);
    }

    /**
     * 获取组织列表
     * @param organize
     * @return
     */
    @LoginNotRequired
    @RequestMapping("findAllOrganize")
    @WeChatRequired
    public String findAllOrganize(Organize organize) {
        CallResult<List<Organize>> result = new CallResult<>();
        logger.info("获取组织列表开始");
        try {
            List<Organize> list = organizeDaoService.getAll(organize);
            if (null != list && list.size() > 0) {
                result.setCode(0);
                result.setMessage("获取成功");
                result.setData(list);
            }
        }catch (Exception e) {
            result.setCode(400);
            result.setMessage("获取组织列表失败");
            logger.info("获取组织列表失败" + e.getMessage(),e);
            e.printStackTrace();
        }
        logger.info("获取组织列表结束");
        return gson.toJson(result);
    }


    /**
     * 获取个人加入的组织
     * @param organize
     * @return
     */
    @LoginNotRequired
    @RequestMapping("findPersonOrganize")
    @WeChatRequired
    public String findPersonOrganize(Organize organize) {
        CallResult<List<Organize>> result = new CallResult<>();
        logger.info("获取加入的组织开始");
        try {
            if (isEmpty(organize.getOpendId())) {
                result.setCode(400);
                result.setMessage("微信id不能为空");
                return gson.toJson(result);
            }
            List<Organize> list = organizeService.findOrganByOpendId(organize);
            if (null != list && list.size() > 0) {
                result.setCode(0);
                result.setMessage("获取成功");
                result.setData(list);
            }else {
                result.setCode(400);
                result.setMessage("暂无加入组织");
                return gson.toJson(result);
            }
        }catch (Exception e) {
            result.setCode(400);
            result.setMessage("获取加入的组织失败");
            logger.error("获取加入的组织失败" + e.getMessage(),e);
            e.printStackTrace();
        }
        logger.info("获取加入的组织结束");
        return gson.toJson(result);
    }


    /**
     * 获取组织下人员信息
     * @param organize
     * @return
     */
    @LoginNotRequired
    @RequestMapping("findOrganizePerson")
    @WeChatRequired
    public String findOrganizePerson(Organize organize) {
        CallResult<List<WXUser>> result = new CallResult<>();
        logger.info("获取组织下人员信息开始");
        try {
            if (null == organize.getId_prikey()) {
                result.setCode(400);
                result.setMessage("主键不能为空");
                return gson.toJson(result);
            }

            List<WXUser> list = wXUserDaoService.getOrganPerson(organize);
            if (null != list && list.size() > 0) {
                result.setCode(0);
                result.setMessage("获取成功");
                result.setData(list);
            }else {
                result.setCode(400);
                result.setMessage("今日人员暂无打卡记录");
            }
        }catch (Exception e) {
            result.setCode(400);
            result.setMessage("获取组织人员信息失败");
            logger.error("获取组织人员信息失败" + e.getMessage(),e);
            e.printStackTrace();
        }
        logger.info("获取组织人员信息结束");
        return gson.toJson(result);
    }

    /**
     * 加入组织
     * @param organize
     * @return
     */
    @LoginNotRequired
    @PostMapping("joinOrganize")
    @WeChatRequired
    public String joinOrganize(Organize organize) {
        CallResult<String> result = new CallResult<>();
        logger.info("加入组织开始");
        if (isEmpty(organize.getOpendId())) {
            result.setCode(400);
            result.setMessage("微信id不能为空");
            return gson.toJson(result);
        }
        if (null == organize.getId_prikey()) {
            result.setCode(400);
            result.setMessage("主键不能为空");
            return gson.toJson(result);
        }
        result = personService.joinOrganize(organize);
        logger.info("加入组织结束");
        return gson.toJson(result);
    }

   /* public String getOrganizeByDeptId(Organize organize) {
        CallResult<String> result = new CallResult<>();
        logger.info("获取上级部门开始");
        try {
            organizeDaoService.getByField();
        }catch (Exception e) {
            result.setCode(400);
            result.setMessage("获取失败");
            logger.error("获取失败" + e.getMessage(),e);
            e.printStackTrace();
        }
        logger.info("获取上级部门结束");
        return gson.toJson(result);
    }*/
}
