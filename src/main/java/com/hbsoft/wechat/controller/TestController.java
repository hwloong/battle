package com.hbsoft.wechat.controller;

import com.hb.annotation.LoginNotRequired;
import com.hbsoft.util.WechatUtil;
import com.hbsoft.wechat.bean.WXUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

@Controller
public class TestController {


	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	WechatUtil wechatUtil;
	
	@ModelAttribute
	public void getClient(HttpSession session){
		session.setAttribute("wxAppOpenid", "ow3dt5ZfK6rO4EJ28cAoC7NgdUPk");
		WXUser wxUser = new WXUser();
		wxUser.setOpenid("ow3dt5ZfK6rO4EJ28cAoC7NgdUPk");
		wxUser.setUserName("test");
		wxUser.setOrgCode("1");
		wxUser.setPhone("18677777777");
		wxUser.setId_prikey(1);
		session.setAttribute("wxUser",wxUser);

	}
	
	@LoginNotRequired
	@RequestMapping("test")
	@ResponseBody
	public String test() {
		return "SUCCESS";
	}

}
