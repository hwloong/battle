package com.hbsoft.wechat.controller;

import com.hb.annotation.LoginNotRequired;
import com.hb.bean.CallResult;
import com.hb.controller.ABaseController;
import com.hbsoft.wechat.bean.Swan;
import com.hbsoft.wechat.bean.SwanAdmin;
import com.hbsoft.wechat.bean.SwanLog;
import com.hbsoft.wechat.bean.WXUser;
import com.hbsoft.wechat.service.SwanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 卡口设置
 */
@RestController
public class SwanController extends ABaseController {
    private SwanService swanService;

    @Autowired
    public SwanController(SwanService swanService) {
        this.swanService = swanService;
    }

    /**
     * 添加卡口
     *
     * @param swan
     * @return
     * @throws Exception
     */
    @RequestMapping("addSwan")
    public String addSwan(@Validated Swan swan) throws Exception {
        WXUser wxUser = getWXUser();
        swan.setOpenId(wxUser.getOpenid());
//        swan.setName(wxUser.getUserName());
        swan.setCreateDate(new Date());
        swan.setPhone(wxUser.getPhone());
        swan.setUserName(wxUser.getUserName());
        return gson.toJson(swanService.addSwan(swan));
    }

    /**
     * 添加卡口门卫信息
     *
     * @param HealthCode 门卫邀请码
     * @return
     * @throws Exception
     */
    @RequestMapping("addSwanAdmin")
    public String addSwanAdmin(String HealthCode) throws Exception {
        CallResult<String> result = new CallResult<>();
        WXUser wxUser = getWXUser();
        if (isEmpty(HealthCode)) {
            result.setCode(400);
            result.setMsg("邀请码不能为空");
        } else {
            result = swanService.addSwanAdmin(HealthCode, wxUser);
        }
        return gson.toJson(result);
    }

    /**
     * 添加卡口流水信息
     *
     * @param swanLog 卡口流水信息
     * @return
     * @throws Exception
     */
    @LoginNotRequired
    @RequestMapping("addSwanLog")
    public String addSwanLog(@Validated SwanLog swanLog) throws Exception {
        WXUser wxUser = getWXUser();
        swanLog.setName(wxUser.getUserName());
        swanLog.setMobile(wxUser.getPhone());
        swanLog.setOpenId(wxUser.getOpenid());
        swanLog.setCreateDate(new Date());
        return gson.toJson(swanService.addSwanLog(swanLog));
    }

    /**
     * 分页获取卡口流水信息（创建人使用）
     *
     * @param swanLog 卡口流水信息
     * @return
     * @throws Exception
     */
    @RequestMapping("getPagingSwanLog")
    public String getPagingSwanLog(SwanLog swanLog) throws Exception {
        WXUser wxUser = getWXUser();
//        swanLog.setOpenId(wxUser.getOpenid());
        return gson.toJson(swanService.getPagingSwanLog(swanLog));
    }

    /**
     * 根据卡口二维码获取卡口信息和用户最后一次再该卡口的填报记录
     *
     * @param qrCode 卡口二维码
     * @return
     */
    @RequestMapping("getSwanName")
    public String getSwanName(String qrCode) throws Exception {
        CallResult<Map<String, Object>> result = new CallResult<>();
        WXUser wxUser;
        try {
            wxUser = getWXUser();
        } catch (Exception e) {
            wxUser = null;
        }
        if (isEmpty(qrCode)) {
            result.setCode(400);
            result.setMsg("卡口二维码不能为空");
        } else {
            result = swanService.getSwanName(qrCode, wxUser);
        }
        return gson.toJson(result);
    }

    /**
     * 获取我的卡口二维码
     *
     * @param id_prikey 卡口id
     * @return
     */
    @RequestMapping("getSwanCode")
    public String getSwanCode(Integer id_prikey) throws Exception {
        CallResult<Swan> result = new CallResult<>();
        if (id_prikey == null) {
            result.setCode(400);
            result.setMsg("卡口id不能为空");
        } else {
            result = swanService.getSwanCode(id_prikey);
        }
        return gson.toJson(result);
    }

    /**
     * 获取我的卡口门卫二维码
     *
     * @param id_prikey 卡口id
     * @return
     */
    @RequestMapping("getSwanAdminCode")
    public String getSwanAdminCode(Integer id_prikey) throws Exception {
        CallResult<Swan> result = new CallResult<>();
        if (id_prikey == null) {
            result.setCode(400);
            result.setMsg("卡口id不能为空");
        } else {
            result = swanService.getSwanAdminCode(id_prikey);
        }
        return gson.toJson(result);
    }

    /**
     * 根据卡口填写部门信息
     *
     * @param id_prikey 卡口id
     * @param deptName  部门名称
     * @return
     * @throws Exception
     */
    @RequestMapping("getSwanDeptName")
    public String getSwanDeptName(Integer id_prikey, String deptName) throws Exception {
        CallResult<List<Map<String, Object>>> result = new CallResult<>();
        if (id_prikey == null) {
            result.setCode(400);
            result.setMsg("卡口id不能为空");
        } else {
            result = swanService.getSwanDeptName(id_prikey, deptName);
        }
        return gson.toJson(result);
    }

    /**
     * 获取我的卡口列表
     *
     * @return
     * @throws Exception
     */
    @RequestMapping("getMySwanAll")
    public String getMySwanAll() throws Exception {
        WXUser wxUser = getWXUser();
        return gson.toJson(swanService.getMySwanAll(wxUser.getOpenid()));
    }

    /**
     * 修改卡口信息
     *
     * @param swan
     * @return
     * @throws Exception
     */
    @RequestMapping("setSwan")
    public String setSwan(@Validated Swan swan) throws Exception {
        CallResult<String> result = new CallResult<>();
        WXUser wxUser = getWXUser();
        if (swan.getId_prikey() == null) {
            result.setCode(400);
            result.setMsg("主键不能为空");
        } else {
            swan.setOpenId(wxUser.getOpenid());
            swan.setQrCode(null);
            swan.setQrCodeUrl(null);
            swan.setHealthCode(null);
            swan.setHealthCodeUrl(null);
            swan.setCreateDate(null);
            result = swanService.setSwan(swan);
        }
        return gson.toJson(result);
    }

    /**
     * 获取卡口信息
     *
     * @param id_prikey
     * @return
     * @throws Exception
     */
    @RequestMapping("getSwanById")
    public String getSwanById(Integer id_prikey) throws Exception {
        CallResult<Map<String, Object>> result = new CallResult<>();
        if (id_prikey == null) {
            result.setCode(400);
            result.setMsg("主键不能为空");
        } else {
            result = swanService.getSwanById(id_prikey);
        }
        return gson.toJson(result);
    }

    /**
     * 移除卡口门卫
     *
     * @param id_prikey 主键
     * @return
     * @throws Exception
     */
    @RequestMapping("removeSwanAdmin")
    public String removeSwanAdmin(Integer id_prikey) throws Exception {
        CallResult<String> result = new CallResult<>();
        if (id_prikey == null) {
            result.setCode(400);
            result.setMsg("主键不能为空");
        } else {
            result = swanService.removeSwanAdmin(id_prikey);
        }
        return gson.toJson(result);
    }

    /**
     * 获取微信用户
     *
     * @return
     */
    private WXUser getWXUser() {
        WXUser wxUser = (WXUser) session.getAttribute("wxUser");
        if (wxUser == null) {
            throw new RuntimeException("未注册不能添加部门信息");
        }
        if (isEmpty(wxUser.getPhone())) {
            throw new RuntimeException("注册信息手机号为空，不能添加部门信息");
        }
        return wxUser;
    }

}
