package com.hbsoft.wechat.controller;

import com.hb.bean.CallResult;
import com.hb.controller.ABaseController;
import com.hbsoft.util.WechatUtil;
import com.hbsoft.wechat.bean.DeptYQ;
import com.hbsoft.wechat.bean.WXUser;
import com.hbsoft.wechat.dao.service.DeptYQDaoService;
import com.hbsoft.wechat.dao.service.WXUserDaoService;
import com.hbsoft.wechat.service.StatisticalYYPService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class StatisticalYYPController extends ABaseController {
    private StatisticalYYPService statisticalYYPService;
    private WXUserDaoService wxUserDaoService;
    private DeptYQDaoService deptYQDaoService;

    @Autowired
    public StatisticalYYPController(StatisticalYYPService statisticalYYPService, WXUserDaoService wxUserDaoService, DeptYQDaoService deptYQDaoService) {
        this.statisticalYYPService = statisticalYYPService;
        this.wxUserDaoService = wxUserDaoService;
        this.deptYQDaoService = deptYQDaoService;
    }

    @RequestMapping("getOrganizeDaily")
    public String getOrganizeDaily(Integer organizeId, Date date) {
        CallResult<List<Map<String, Integer>>> result = new CallResult<>();
        if (organizeId == null) {
            result.setCode(400);
            result.setMessage("组织id不能为空");
        } else if (date == null) {
            result.setCode(400);
            result.setMessage("查询日期不能为空");
        } else {
            List<Map<String, Integer>> list = statisticalYYPService.getOrganizeDaily(organizeId, date);
            result.setCode(0);
            result.setMessage("获取成功");
            result.setData(list);
        }
        return gson.toJson(result);
    }

    /**
     * 获取openid
     *
     * @param openId openId
     * @return
     * @throws Exception
     */
    @RequestMapping("getYYPOpenId")
    public String getYYPOpenId(String openId) throws Exception {
        CallResult<String> result = new CallResult<>();
        WXUser user = new WXUser();
        user.setOpenid(openId);
        WXUser byField = wxUserDaoService.getByField(user);
        if (byField != null) {
            result.setCode(0);
            result.setMessage("获取成功");
            session.setAttribute("wxUser", byField);
        } else {
            result.setCode(400);
            result.setMessage("获取失败");
        }
        return gson.toJson(result);
    }

    /**
     * 贾志用部门邀请二维码返现部门名称
     *
     * @param id 二维码code
     * @return
     * @throws Exception
     */
    @RequestMapping("getDeptCodeToDeptNameJZ")
    public String getDeptCodeToDeptNameJZ(String id,String type) throws Exception {
        CallResult<Map<String, String>> result = new CallResult<>();
        if (isEmpty(id)) {
            result.setCode(400);
            result.setMessage("id不能为空");
        }else if (isEmpty(type)){
            result.setCode(400);
            result.setMessage("type不能为空");
        }else {
            DeptYQ deptYQ = new DeptYQ();
            if("1".equals(type)){
                deptYQ.setDeptParentCode(id);
            }else{
                deptYQ.setDeptCode(id);
            }
            DeptYQ byField = deptYQDaoService.getByField(deptYQ);
            if (byField != null) {
                Map<String, String> data = new HashMap<>();
                data.put("name", byField.getDpetName());
                data.put("icon", byField.getIcon());
                result.setCode(0);
                result.setMessage("获取成功");
                result.setData(data);
            } else {
                result.setCode(400);
                result.setMessage("未找到信息");
            }
        }
        return gson.toJson(result);
    }
}
