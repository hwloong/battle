package com.hbsoft.wechat.controller;

import com.hb.annotation.LoginNotRequired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WeChatMapViewController {


    @GetMapping("addCase")
    public String addCase() throws Exception {
        return "addCase";
    }

    @GetMapping("map")
    public String map() throws Exception {
        return "map";
    }

    @GetMapping("province")
    public String province() throws Exception {
        return "province";
    }

    @GetMapping("clock")
    public String clock() throws Exception {
        return "clock";
    }

}
