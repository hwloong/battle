package com.hbsoft.wechat.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.hb.bean.CallResult;
import com.hb.controller.ABaseController;
import com.hb.util.ExcelUtil;
import com.hbsoft.wechat.service.BusPunchCardService;
import com.hbsoft.wechat.vo.BusVo;

@Controller
public class BusPunchCarController extends ABaseController {

	@Autowired
	BusPunchCardService busPunchCardService;

	/**
	 * 公交打卡明细
	 * 
	 * @param vo
	 * @return
	 */
	@ResponseBody
	@PostMapping("getBusPunchCardInfo")
	public String getBusPunchCardInfo(BusVo vo) {
		CallResult<List<Map<String, Object>>> result = new CallResult<>();
		try {
			List<Map<String, Object>> list = busPunchCardService.getBusPunchCardInfo(vo);
			Integer i = busPunchCardService.getBusPunchCardPageNum(vo);
			result.setCount(i);
			result.setData(list);
			result.setCode(0);
			result.setMessage("获取成功");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			result.setCode(400);
			result.setMessage("获取失败");
			e.printStackTrace();
		}
		return gson1.toJson(result);
	}

	/**
	 * 公交汇总打卡数
	 * 
	 * @param vo
	 * @return
	 */
	@ResponseBody
	@PostMapping("busPunchCardCount")
	public String busPunchCardCount(BusVo vo) {
		CallResult<List<Map<String, Object>>> result = new CallResult<>();
		try {
			List<Map<String, Object>> list = busPunchCardService.busPunchCardCount(vo);
			Integer i = busPunchCardService.busPunchCardPageNum(vo);
			result.setCount(i);
			result.setData(list);
			result.setCode(0);
			result.setMessage("获取成功");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			result.setCode(400);
			result.setMessage("获取失败");
			e.printStackTrace();
		}
		return gson1.toJson(result);
	}

	@GetMapping("excelBusExport")
	public void excelBusExport(HttpServletResponse response, BusVo vo, String flag) {
		List<String> title = new ArrayList<>();
		List<String> dataOrder = new ArrayList<>();
		if ("1".equals(flag)) {
			String fileName = "公交打卡总明细.xls";
			title.add("打卡时间");
			title.add("车牌号");
			title.add("客户姓名");
			title.add("客户手机号");
			title.add("公交线路");
			dataOrder.add("creatTime");
			dataOrder.add("busName");
			dataOrder.add("name");
			dataOrder.add("phone");
			dataOrder.add("busType");
			List<Map<String, Object>> list = busPunchCardService.getBusPunchCardInfo2(vo);
			try {
				ExcelUtil.exportExcel(response, fileName, title, dataOrder, list);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if("2".equals(flag)){
			String fileName = "公交打卡明细.xls";
			title.add("公交线路");
			title.add("车牌号");
			title.add("打卡数");
			dataOrder.add("busType");
			dataOrder.add("busName");
			dataOrder.add("num");
			List<Map<String, Object>> list = busPunchCardService.busPunchCardCount2(vo);
			try {
				ExcelUtil.exportExcel(response, fileName, title, dataOrder, list);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
