package com.hbsoft.wechat.controller;

import com.hb.bean.CallResult;
import com.hb.controller.ABaseController;
import com.hbsoft.wechat.bean.DeptOutbreakLog;
import com.hbsoft.wechat.bean.WXUser;
import com.hbsoft.wechat.service.DeptOutbreakLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DeptOutbreakLogController extends ABaseController {
    private DeptOutbreakLogService deptOutbreakLogService;

    @Autowired
    public DeptOutbreakLogController(DeptOutbreakLogService deptOutbreakLogService) {
        this.deptOutbreakLogService = deptOutbreakLogService;
    }

    /**
     * 添加部门日志
     *
     * @param deptOutbreakLog 部门日志信息
     * @return
     * @throws Exception
     */
    @RequestMapping("addDeptOutbreakLog")
    public String addDeptOutbreakLog(@Validated DeptOutbreakLog deptOutbreakLog) throws Exception {
        WXUser user = getWXUser();
        CallResult<String> result = deptOutbreakLogService.addDeptOutbreakLog(deptOutbreakLog, user);
        return gson.toJson(result);
    }

    /**
     * 获取部门日志
     *
     * @param deptOutbreakLog 部门日志信息
     * @return
     * @throws Exception
     */
    @RequestMapping("getDeptOutbreakLog")
    public String getDeptOutbreakLog(DeptOutbreakLog deptOutbreakLog) throws Exception {
        CallResult<DeptOutbreakLog> result = new CallResult<>();
        WXUser user = getWXUser();
        if (deptOutbreakLog.getDeptId() == null) {
            result.setCode(400);
            result.setMessage("部门id不能为空");
        } else if (deptOutbreakLog.getDate() == null) {
            result.setCode(400);
            result.setMessage("日期不能为空");
        } else {
            result = deptOutbreakLogService.getDeptOutbreakLog(deptOutbreakLog, user);
        }
        return gson.toJson(result);
    }

    /**
     * 获取微信用户
     *
     * @return
     */
    private WXUser getWXUser() {
        WXUser wxUser = (WXUser) session.getAttribute("wxUser");
        if (wxUser == null) {
            throw new RuntimeException("未注册不能添加部门信息");
        }
        if (isEmpty(wxUser.getPhone())) {
            throw new RuntimeException("注册信息手机号为空，不能添加部门信息");
        }
        return wxUser;
    }
}
