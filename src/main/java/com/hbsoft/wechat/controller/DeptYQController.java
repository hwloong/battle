package com.hbsoft.wechat.controller;

import com.hb.bean.CallResult;
import com.hb.controller.ABaseController;
import com.hb.util.StringUtil;
import com.hbsoft.wechat.bean.DeptBindUser;
import com.hbsoft.wechat.bean.DeptYQ;
import com.hbsoft.wechat.bean.WXUser;
import com.hbsoft.wechat.service.DeptYQService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
public class DeptYQController extends ABaseController {
    private DeptYQService deptYQService;

    @Autowired
    public DeptYQController(DeptYQService deptYQService) {
        this.deptYQService = deptYQService;
    }

    /**
     * 添加部门或集团信息
     *
     * @return
     */
    @RequestMapping("addDeptYQ")
    public String addDeptYQ(@Validated DeptYQ deptYQ) throws Exception {
        CallResult<String> result = new CallResult<>();
        WXUser user = getWXUser();
        if (result.isExec()) {
            deptYQ.setDeptNo(StringUtil.randomId());
            deptYQ.setMobile(user.getPhone());
            deptYQ.setCreateDate(new Date());
            deptYQ.setDisable(0);
        }
        if (result.isExec()) {
            DeptBindUser bindUser = new DeptBindUser();
            bindUser.setMobile(user.getPhone());
            bindUser.setName(user.getUserName());
            bindUser.setOpenId(user.getOpenid());
            bindUser.setCreateDate(new Date());
            bindUser.setDisable(0);
            result = deptYQService.addDeptYQ(deptYQ, bindUser);
        }
        return gson.toJson(result);
    }

    /**
     * 删除部门信息
     *
     * @return
     */
    @RequestMapping("removeDeptYQ")
    public String removeDeptYQ(String deptNo) {
        CallResult<String> result = new CallResult<>();
        WXUser user = getWXUser();
        if (isEmpty(deptNo)) {
            result.setCode(400);
            result.setMessage("部门id不能为空");
        }
        return gson.toJson(result);
    }

    /**
     * 修改部门信息
     *
     * @return
     */
    @RequestMapping("setDeptYQ")
    public String setDeptYQ(@Validated DeptYQ deptYQ) throws Exception {
        CallResult<String> result = new CallResult<>();
        WXUser user = getWXUser();
        if (deptYQ.getId_prikey() == null) {
            result.setCode(400);
            result.setMessage("主键不能为空");
        }
        if (result.isExec()) {
            result = deptYQService.setDeptYQ(deptYQ);
        }
        return gson.toJson(result);
    }

    /**
     * 获取我的部门信息
     *
     * @param deptNo 部门id
     * @return
     */
    @RequestMapping("getMyDeptYQById")
    public String getMyDeptYQById(String deptNo) throws Exception {
        CallResult<Map<String, Object>> result = new CallResult<>();
        WXUser user = getWXUser();
        if (isEmpty(deptNo)) {
            result.setCode(400);
            result.setMessage("部门id不能为空");
        } else {
            result = deptYQService.getMyDeptYQById(deptNo, user);
        }
        return gson.toJson(result);
    }

    /**
     * 获取我的部门统计信息
     *
     * @param date   日期
     * @param deptNo 部门id
     * @return
     * @throws Exception
     */
    @RequestMapping("getMyDeptYQStatistical")
    public String getMyDeptYQStatistical(Date date, String deptNo) throws Exception {
        CallResult<Map<String, Object>> result = new CallResult<>();
        WXUser user = getWXUser();
        if (isEmpty(deptNo)) {
            result.setCode(400);
            result.setMessage("部门id不能为空");
        } else {
            result = deptYQService.getMyDeptYQStatistical(date, deptNo, user);
        }
        return gson.toJson(result);
    }

    /**
     * 获取我的下级部门信息
     *
     * @param deptNo 部门id
     * @return
     * @throws Exception
     */
    @RequestMapping("getMyNextDeptYQ")
    public String getMyNextDeptYQ(String deptNo) throws Exception {
        CallResult<List<Map<String, Object>>> result = new CallResult<>();
        getWXUser();
        if (isEmpty(deptNo)) {
            result.setCode(400);
            result.setMessage("部门id不能为空");
        } else {
            result = deptYQService.getMyNextDeptYQ(deptNo);
        }
        return gson.toJson(result);
    }

    /**
     * 获取我的下级部门统计信息
     *
     * @param deptNo 部门id
     * @return
     * @throws Exception
     */
    @RequestMapping("getMyNextDeptYQStatistical")
    public String getMyNextDeptYQStatistical(Date date, String deptNo) throws Exception {
        CallResult<Map<String, Object>> result = new CallResult<>();
        getWXUser();
        if (isEmpty(deptNo)) {
            result.setCode(400);
            result.setMessage("部门id不能为空");
        } else {
            result = deptYQService.getMyNextDeptYQStatistical(date, deptNo);
        }
        return gson.toJson(result);
    }

    /**
     * 获取我加入的部门信息
     *
     * @return
     */
    @RequestMapping("getMyDeptYQAll")
    public String getMyDeptYQAll() throws Exception {
        WXUser user = getWXUser();
        return gson.toJson(deptYQService.getMyDeptYQAll(user));
    }

    /**
     * 获取我管理的部门信息
     *
     * @return
     */
    @RequestMapping("getMyDeptYQAllAdmin")
    public String getMyDeptYQAllAdmin() throws Exception {
        CallResult<List<DeptYQ>> result = new CallResult<>();
        WXUser user = getWXUser();
        if (result.isExec()) {
            result = deptYQService.getMyDeptYQAllAdmin(user.getPhone());
        }
        return gson.toJson(result);
    }

    /**
     * 获取集团邀请二维码
     *
     * @param deptNo 部门id
     * @return
     * @throws Exception
     */
    @RequestMapping("getDeptParentCode")
    public String getDeptParentCode(String deptNo) throws Exception {
        CallResult<DeptYQ> result = new CallResult<>();
        WXUser user = getWXUser();
        if (isEmpty(deptNo)) {
            result.setCode(400);
            result.setMessage("部门id不能为空");
        } else {
            result = deptYQService.getDeptParentCode(deptNo, user.getPhone());
        }
        return gson.toJson(result);
    }

    /**
     * 获取部门邀请二维码
     *
     * @param deptNo 部门id
     * @return
     * @throws Exception
     */
    @RequestMapping("getDeptCode")
    public String getDeptCode(String deptNo) throws Exception {
        CallResult<DeptYQ> result = new CallResult<>();
        WXUser user = getWXUser();
        if (isEmpty(deptNo)) {
            result.setCode(400);
            result.setMessage("部门id不能为空");
        } else {
            result = deptYQService.getDeptCode(deptNo, user.getPhone());
        }
        return gson.toJson(result);
    }

    /**
     * 根据部门邀请码获取部门名称
     *
     * @param deptCode 部门邀请码
     * @return
     */
    @RequestMapping("getDeptCodeToDeptName")
    public String getDeptCodeToDeptName(String deptCode) throws Exception {
        CallResult<Map<String, String>> result = new CallResult<>();
        WXUser user = getWXUser();
        if (isEmpty(deptCode)) {
            result.setCode(400);
            result.setMessage("部门邀请码不能为空");
        } else {
            result = deptYQService.getDeptCodeToDeptName(deptCode, user.getPhone());
        }
        return gson.toJson(result);
    }

    /**
     * 根据集团邀请码获取部门名称
     *
     * @param deptCode 集团邀请码
     * @return
     */
    @RequestMapping("getDeptParentCodeToDeptName")
    public String getDeptParentCodeToDeptName(String deptCode) throws Exception {
        CallResult<Map<String, String>> result = new CallResult<>();
        getWXUser();
        if (isEmpty(deptCode)) {
            result.setCode(400);
            result.setMessage("部门邀请码不能为空");
        } else {
            result = deptYQService.getDeptParentCodeToDeptName(deptCode);
        }
        return gson.toJson(result);
    }

    /**
     * 根据姓名查询是否可以绑定到部门
     *
     * @param name     姓名
     * @param deptCode 部门邀请码
     * @return
     * @throws Exception
     */
    @RequestMapping("getNameToDept")
    public String getNameToDept(String deptCode, String name) throws Exception {
        CallResult<String> result = new CallResult<>();
        WXUser user = getWXUser();
        if (isEmpty(name)) {
            result.setCode(400);
            result.setMessage("姓名不能为空");
        } else if (isEmpty(deptCode)) {
            result.setCode(400);
            result.setMessage("部门邀请码不能为空");
        } else {
            result = deptYQService.getNameToDept(deptCode, name, user);
        }
        return gson.toJson(result);
    }

    /**
     * 删除绑定人员
     *
     * @param id_prikey 绑定主键
     * @return
     */
    @RequestMapping("removeDeptYQUser")
    public String removeDeptYQUser(Integer id_prikey) throws Exception {
        CallResult<String> result = new CallResult<>();
        WXUser user = getWXUser();
        if (id_prikey == null) {
            result.setCode(400);
            result.setMessage("主键不能为空");
        } else {
            result = deptYQService.removeDeptYQUser(id_prikey, user);
        }
        return gson.toJson(result);
    }

    /**
     * 获取首页数据
     *
     * @return
     */
    @RequestMapping("getIndexDataXJ")
    public String getIndexDataXJ() throws Exception {
        return gson.toJson(deptYQService.getIndexDataXJ());
    }

    /**
     * 获取下级疫情日报汇总
     *
     * @param deptNo 部门编号
     * @param date   日期
     * @return
     * @throws Exception
     */
    @RequestMapping("getNextDeptYQSummary")
    public String getNextDeptYQSummary(String deptNo, Date date) throws Exception {
        CallResult<List<Map<String, Object>>> result = new CallResult<>();
        getWXUser();
        if(isEmpty(deptNo)){
            result.setCode(400);
            result.setMessage("部门编号不能为空");
        }else if(date==null){
            result.setCode(400);
            result.setMessage("日期不能为空");
        }else{
            result = deptYQService.getNextDeptYQSummary(deptNo,date);
        }
        return gson.toJson(result);
    }

    /**
     * 获取下级疫情概览查询
     *
     * @param deptNo 部门编号
     * @return
     * @throws Exception
     */
    @RequestMapping("getNextDeptYQSituation")
    public String getNextDeptYQSituation(String deptNo) throws Exception {
        CallResult<List<Map<String, Object>>> result = new CallResult<>();
        getWXUser();
        if(isEmpty(deptNo)){
            result.setCode(400);
            result.setMessage("部门编号不能为空");
        }else{
            result = deptYQService.getNextDeptYQSituation(deptNo);
        }
        return gson.toJson(result);
    }

    /**
     * 获取微信用户
     *
     * @return
     */
    private WXUser getWXUser() {
        WXUser wxUser = (WXUser) session.getAttribute("wxUser");
        if (wxUser == null) {
            throw new RuntimeException("未注册不能添加部门信息");
        }
        if (isEmpty(wxUser.getPhone())) {
            throw new RuntimeException("注册信息手机号为空，不能添加部门信息");
    }
        return wxUser;
    }
}
