package com.hbsoft.wechat.controller;

import com.hb.annotation.LoginNotRequired;
import com.hb.bean.CallResult;
import com.hb.controller.ABaseController;
import com.hb.util.HttpUtil;
import com.hbsoft.annotation.WeChatRequired;
import com.hbsoft.util.DateUtil;
import com.hbsoft.util.GsonUtil;
import com.hbsoft.wechat.bean.*;
import com.hbsoft.wechat.dao.service.WxActivePathDaoService;
import com.hbsoft.wechat.dao.service.WxBattleRealTimeDataDaoService;
import com.hbsoft.wechat.dao.service.WxMapClockDaoService;
import com.hbsoft.wechat.service.WxCaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(produces = {"application/json;charset=UTF-8"})
public class WechatMapController extends ABaseController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    WxCaseService wxCaseService;
    @Autowired
    WxActivePathDaoService wxActivePathDaoService;
    @Autowired
    WxBattleRealTimeDataDaoService wxBattleRealTimeDataDaoService;
    @Autowired
    WxMapClockDaoService wxMapClockDaoService;

    /**
     * 获取人员以及活动路径信息
     *
     * @param wxCase
     * @return
     */
    @LoginNotRequired
    @RequestMapping("getCaseAndActivePathInfo")
    public String getCaseAndActivePathInfo(WxCase wxCase) {
        CallResult<List<WxCase>> result = new CallResult<>();
        logger.info("查询人员以及活动路径信息开始");
        try {
            List<WxCase> wxcaseList = wxCaseService.getAlls(wxCase);

            if (wxcaseList == null) {
                result.setCode(0);
                result.setMessage("暂无记录");
                return gson.toJson(result);
            } else {
                result.setData(wxcaseList);
                result.setCode(0);
                return gson.toJson(result);
            }
        } catch (Exception e) {
            result.setCode(400);
            result.setMessage("查询失败");
            logger.error("查询失败" + e.getMessage(), e);
            e.printStackTrace();
        }
        logger.info("查询人员以及活动信息结束");
        return gson.toJson(result);
    }


    /**
     * 纠错
     *
     * @param wxActivePath
     * @return
     * @throws Exception
     */
    @LoginNotRequired
    @RequestMapping("updateAndAddWxActivePath")
    public String updateAndAddWxActivePath(WxActivePath wxActivePath) throws Exception {
        CallResult<String> result = new CallResult<>();
        logger.info("纠错开始");
        try {
            if (wxActivePath.getId_prikey() == null) {
                result.setCode(400);
                result.setMessage("主键不能为空");
                return gson.toJson(result);
            }
            if (isEmpty(wxActivePath.getExamineCoordinateAddress())) {
                result.setCode(400);
                result.setMessage("坐标经纬度不能为空");
                return gson.toJson(result);
            }
            if (isEmpty(wxActivePath.getExamineCoordinateName())) {
                result.setCode(400);
                result.setMessage("坐标名称不能为空");
                return gson.toJson(result);
            }
            if (isEmpty(wxActivePath.getUpdateBy())) {
                result.setCode(400);
                result.setMessage("更新人不能为空");
                return gson.toJson(result);
            }
            if (isEmpty(wxActivePath.getExamineCounties())) {
                result.setCode(400);
                result.setMessage("所属区县不能为空");
                return gson.toJson(result);
            }
            wxActivePath.setUpdateDate(new Date());
            wxActivePath.setIsExamine(0);
            wxActivePathDaoService.set(wxActivePath);
            result.setCode(0);
            result.setMessage("纠错成功，等待审核");
        } catch (Exception e) {
            result.setCode(400);
            result.setMessage("纠错失败");
            logger.info("纠错失败" + e.getMessage(), e);
            e.printStackTrace();
        }
        logger.info("纠错结束");
        return gson.toJson(result);
    }

    /**
     * 获取未审核信息
     *
     * @return
     */
    @LoginNotRequired
    @RequestMapping("findNotExamineList")
    public String findNotExamineList(WxActivePath wxActivePath) {
        CallResult<List<WxActivePath>> result = new CallResult<>();
        logger.info("获取未审核活动路径数据开始");
        try {
            //获取未审核活动路径数据
            wxActivePath.setIsExamine(0);
            List<WxActivePath> list = wxActivePathDaoService.getAll(wxActivePath);
            if (null != list && list.size() > 0) {
                result.setCode(0);
                result.setMessage("获取成功");
                result.setData(list);
            }
        } catch (Exception e) {
            result.setCode(400);
            result.setMessage("获取未审核活动路径数据失败");
            logger.error("获取未审核活动路径数据失败" + e.getMessage(), e);
            e.printStackTrace();
        }
        logger.info("获取未审核活动路径数据结束");
        return gson.toJson(result);
    }

    /**
     * 审核
     *
     * @param id
     * @return
     * @throws Exception
     */
    @LoginNotRequired
    @RequestMapping("updateWxActivePathExamine")
    public String updateWxActivePathExamine(Integer id, String examineBy) throws Exception {
        CallResult<String> result = new CallResult<>();
        logger.info("审核开始");
        try {
            if (id == null) {
                result.setCode(400);
                result.setMessage("主键不能为空");
                return gson.toJson(result);
            }
            if (isEmpty(examineBy)) {
                result.setCode(400);
                result.setMessage("审核人不能为空");
                return gson.toJson(result);
            }
            WxActivePath wxActivePath = wxActivePathDaoService.getById(id);
            wxActivePath.setCoordinateAddress(wxActivePath.getExamineCoordinateAddress());
            wxActivePath.setCoordinateName(wxActivePath.getExamineCoordinateName());
            wxActivePath.setCounties(wxActivePath.getExamineCounties());
            wxActivePath.setIsExamine(1);
            wxActivePath.setExamineBy(examineBy);
            wxActivePath.setExamineDate(new Date());
            wxActivePathDaoService.set(wxActivePath);
            result.setCode(0);
            result.setMessage("审核成功");
        } catch (Exception e) {
            result.setCode(400);
            result.setMessage("审核失败");
            logger.info("审核失败" + e.getMessage(), e);
            e.printStackTrace();
        }
        logger.info("审核结束");
        return gson.toJson(result);
    }

    /**
     * 获取实时病例信息
     *
     * @param area
     * @return
     */
    @LoginNotRequired
    @RequestMapping("getBattleRealTimeData")
    public String getBattleRealTimeData(String area) {
        CallResult<List<WxBattleRealTimeData>> result = new CallResult<>();
        logger.info("获取实时病例信息开始");
        try {
            WxBattleRealTimeData wxbtd = new WxBattleRealTimeData();
            if (isEmpty(area)) {
                wxbtd.setArea("长治市");
            } else {
                wxbtd.setArea(area);
            }

            List<WxBattleRealTimeData> list = wxBattleRealTimeDataDaoService.getAll(wxbtd);
            if (null != list && list.size() > 0) {
                result.setCode(0);
                result.setMessage("获取成功");
                result.setData(list);
            }
        } catch (Exception e) {
            result.setCode(400);
            result.setMessage("获取实时病例信息失败");
            logger.error("获取实时病例信息失败" + e.getMessage(), e);
            e.printStackTrace();
        }
        logger.info("获取实时病例信息结束");
        return gson.toJson(result);
    }

    /**
     * 微信地图打卡
     *
     * @param wxMapClock
     * @return
     * @throws Exception
     */
    @LoginNotRequired
    @RequestMapping("startAddWxMapClock")
    public String startAddWxMapClock(WxMapClock wxMapClock) throws Exception {
        CallResult<String> result = new CallResult<>();
        logger.info("微信地图打卡开始");
        try {
            WxMapClock wxMapClock_1 = new WxMapClock();
            if (wxMapClock.getOpendId() != null) {
                wxMapClock_1.setOpendId(wxMapClock.getOpendId());
                Integer i = wxCaseService.findIsEndTimeClock(wxMapClock_1);
                if (i > 0) {
                    result.setCode(400);
                    result.setMessage("还有未结束打卡");
                    return gson.toJson(result);
                }
            }
            if (isEmpty(wxMapClock.getOpendId())) {
                result.setCode(400);
                result.setMessage("打卡人信息不能为空");
                return gson.toJson(result);
            }
            if (isEmpty(wxMapClock.getColockCoordinate())) {
                result.setCode(400);
                result.setMessage("打卡坐标不能为空");
                return gson.toJson(result);
            }
            if (isEmpty(wxMapClock.getClockLocal())) {
                result.setCode(400);
                result.setMessage("打卡地点不能为空");
                return gson.toJson(result);
            }
            if (isEmpty(wxMapClock.getRemarks())) {
                result.setCode(400);
                result.setMessage("打卡备注不能为空");
                return gson.toJson(result);
            }
            wxMapClock.setStartDate(new Date());
            wxMapClockDaoService.add(wxMapClock);
            result.setCode(0);
            result.setMessage("打卡成功");
        } catch (Exception e) {
            result.setCode(400);
            result.setMessage("打卡失败");
            logger.info("打卡失败" + e.getMessage(), e);
            e.printStackTrace();
        }
        logger.info("微信地图打卡结束");
        return gson.toJson(result);
    }

    /**
     * 微信地图结束打卡
     *
     * @param wxMapClock
     * @return
     * @throws Exception
     */
    @LoginNotRequired
    @RequestMapping("endUpdateWxMapClock")
    public String endUpdateWxMapClock(WxMapClock wxMapClock) throws Exception {
        CallResult<List<WxMapClock>> result = new CallResult<>();
        logger.info("微信地图结束打卡开始");
        try {
            WxMapClock wxMapClock_1 = new WxMapClock();
            if (wxMapClock.getOpendId() != null) {
                wxMapClock_1.setOpendId(wxMapClock.getOpendId());
                Integer i = wxCaseService.findEndClock(wxMapClock_1);
                if (i == 0) {
                    result.setCode(400);
                    result.setMessage("您还未打卡");
                    return gson.toJson(result);
                }
                Integer ii = wxCaseService.findIsEndTimeClock(wxMapClock_1);
                if (ii == 0) {
                    result.setCode(400);
                    result.setMessage("打卡失败");
                    return gson.toJson(result);
                }

                WxMapClock wxMapClock_i = wxCaseService.getWxMapClockByOpenid(wxMapClock_1);
                if (wxMapClock_i.getEndDate() == null) {
                    wxMapClock_i.setEndDate(new Date());
                    Boolean iii = wxMapClockDaoService.set(wxMapClock_i);
                    if (iii) {
                        List<WxMapClock> wxMapClockList = wxCaseService.getAllsForWxMapClock(wxMapClock_1);
                        result.setCode(0);
                        result.setMessage("打卡成功");
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        for (WxMapClock mm : wxMapClockList) {
                            if (mm.getStartDate() != null && mm.getEndDate() != null) {
                                String startDate = df.format(mm.getStartDate());
                                String endDate = df.format(mm.getEndDate());
                                mm.setStartToEndTime(DateUtil.getTimeDifference(endDate, startDate));
                            }
                        }
                        result.setData(wxMapClockList);
                    }
                }
            }
        } catch (Exception e) {
            result.setCode(400);
            result.setMessage("打卡失败");
            logger.info("打卡失败" + e.getMessage(), e);
            e.printStackTrace();
        }
        logger.info("微信地图打卡结束");
        return gson.toJson(result);
    }

    /**
     * 获取微信地图打卡list
     *
     * @return
     */
    @LoginNotRequired
    @RequestMapping("findWxMapClockList")
    public String findWxMapClockList(WxMapClock wxMapClock) {
        CallResult<List<WxMapClock>> result = new CallResult<>();
        logger.info("获取获取微信地图打卡list数据开始");
        try {
            //获取未审核活动路径数据
            List<WxMapClock> list = wxMapClockDaoService.getAll(wxMapClock);
            if (null != list && list.size() > 0) {
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                for (WxMapClock mm : list) {
                    if (mm.getStartDate() != null && mm.getEndDate() != null) {
                        String startDate = df.format(mm.getStartDate());
                        String endDate = df.format(mm.getEndDate());
                        mm.setStartToEndTime(DateUtil.getTimeDifference(endDate, startDate));
                    } else {
                        mm.setStartToEndTime("至今");
                    }
                }
                result.setCode(0);
                result.setMessage("获取成功");
                result.setData(list);
            }
        } catch (Exception e) {
            result.setCode(400);
            result.setMessage("获取微信地图打卡list失败");
            logger.error("获取微信地图打卡list失败" + e.getMessage(), e);
            e.printStackTrace();
        }
        logger.info("获取微信地图打卡list结束");
        return gson.toJson(result);
    }

    @LoginNotRequired
    @RequestMapping(value = "getBaiduMapApiXy")
    @ResponseBody
    public String getBaiduMapApiXy(String url) {
        CallResult<String> result = new CallResult<>();
        // url = "http://api.map.baidu.com/ag/coord/convert?from=0&to=4&x=113.10369873046875&y=36.226566314697266";
        String result1 = HttpUtil.sendGet(url);
        BaiDuMapInfo baiDuMapInfo = GsonUtil.fromJson(result1, BaiDuMapInfo.class);
        if (!"0".equals(baiDuMapInfo.getError())) {
            result.setCode(400);
            result.setMessage("获取数据异常");
            return gson.toJson(result);
        }
        return GsonUtil.toJson(baiDuMapInfo);
    }

}
