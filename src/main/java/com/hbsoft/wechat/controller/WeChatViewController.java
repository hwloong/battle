package com.hbsoft.wechat.controller;

import com.hb.annotation.LoginNotRequired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WeChatViewController {

    @LoginNotRequired
    @RequestMapping("{page}")
    public String page(@PathVariable("page") String page) {
        return page;
    }

    @LoginNotRequired
    @RequestMapping("battleHome")
    public String battleHome() {
        return "home";
    }

    @LoginNotRequired
    @RequestMapping("deptClockInInfo")
    public String deptClockInInfo() {
        return "deptClockInInfo";
    }

    @LoginNotRequired
    @RequestMapping("allClockInInfo")
    public String allClockInInfo() {
        return "allClockInInfo";
    }


    @LoginNotRequired
    @RequestMapping("swanClockInInfo")
    public String swanClockInInfo() {
        return "swanClockInInfo";
    }
    
    
    @LoginNotRequired
    @RequestMapping("busPunchCarInfo")
    public String busPunchCarInfo() {
    	return "busPunchCarInfo";
    }
    
    @LoginNotRequired
    @RequestMapping("busPunchCarStatistics")
    public String busPunchCarStatistics() {
    	return "busPunchCarStatistics";
    }
    
    
}
