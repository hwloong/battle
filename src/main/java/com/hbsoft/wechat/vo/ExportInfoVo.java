package com.hbsoft.wechat.vo;

import lombok.Data;

@Data
public class ExportInfoVo {

    private String exportUrl;

    private String userName;

    private String password;
}
