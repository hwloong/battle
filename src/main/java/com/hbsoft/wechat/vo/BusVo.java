package com.hbsoft.wechat.vo;

import com.hb.bean.APojo;

import lombok.Data;

@Data
public class BusVo extends APojo {

	// 路号
	private String busType;
	// 开始时间
	private String startTime;
	// 结束时间
	private String endTime;
	// 车牌号
	private String busName;
	// 客户姓名
	private String name;
}
