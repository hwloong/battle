package com.hbsoft.wechat.vo;

import lombok.Data;

@Data
public class WxField {

    private String field1;

    private String field2;

    private String field3;
}
