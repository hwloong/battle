package com.hbsoft.wechat.vo;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import lombok.Data;

import java.util.Date;

@Data
public class UserVo  {

    private String openid;

    private String phone;

    private String userName;

    private String orgCode;

    private String flag;

    private String address;

    private String gender;

    private Integer age;

    private String job;


    private String idCard;


    private Date workDate;


    private String trainNum;


    private String health;

    private String remark;

    private String field1;

    private String field2;

    private String deptName;

}
