package com.hbsoft.wechat.bean;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.annotation.ver.NotNull_hb;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import com.hb.hbenum.OrderMode;
import lombok.Data;

import java.util.Date;

@Data
@TableName("k_swan_log")
public class SwanLog extends APojo {
    @NotNull_hb(message = "卡口id不能为空")
    @FieldParam("卡口id")
    private Integer swanId;
    @NotNull_hb(message = "类型不能为空")
    @FieldParam("类型")
    private String type;
    @FieldParam("体温")
    private Double temperature;
    @FieldParam("姓名")
    private String name;
    @FieldParam("手机号")
    private String mobile;
    @FieldParam("微信id")
    private String openId;
    @FieldParam("单位名称")
    private String dept;
    @FieldParam("家庭住址")
    private String address;
    @FieldParam(value = "创建时间", order = OrderMode.desc)
    private Date createDate;
    @FieldParam("备注")
    private String note;
    @FieldParam(fieldType = FieldType.prikeyIncrement)
    private Integer id_prikey;

    @FieldParam(association = true)
    private String startDate;

    @FieldParam(association = true)
    private String endDate;
}
