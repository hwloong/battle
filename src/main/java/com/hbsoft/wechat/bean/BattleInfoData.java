package com.hbsoft.wechat.bean;

import lombok.Data;

import java.util.List;

@Data
public class BattleInfoData {

    private String date;

    private String diagnosed;

    private String suspect;

    private String death;

    private String cured;

    public List<Area> area;

}
