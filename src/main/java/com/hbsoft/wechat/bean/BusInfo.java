package com.hbsoft.wechat.bean;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import lombok.Data;

@Data
@TableName("k_bus_info")
public class BusInfo extends APojo {

    @FieldParam(value="ID_PRIKEY", fieldType= FieldType.prikeyIncrement)
    private Integer id_prikey;

    @FieldParam("路别")
    private String busType;

    @FieldParam("车牌号码")
    private String busName;

    @FieldParam("标识")
    private String flag;
}
