package com.hbsoft.wechat.bean;

import lombok.Data;

@Data
public class BattleInfo {

    private String errcode;

    private BattleInfoData data;

}
