package com.hbsoft.wechat.bean;

import com.hb.bean.APojo;
import lombok.Data;

import java.util.Date;

@Data
public class WxAppClockInfo extends APojo {

    private Date clockInTime;

    private String clockInStr;

    private String type;

    private String temperature;

    private String clockName;

    private String openid;
}
