package com.hbsoft.wechat.bean;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.annotation.ver.NotNull_hb;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import lombok.Data;

import java.util.Date;

@Data
@TableName("k_swan")
public class Swan extends APojo {
    @NotNull_hb(message = "卡口名称不能为空")
    @FieldParam("名称")
    private String name;
    @FieldParam("密码")
    private String pass;
    @NotNull_hb(message = "卡口类型不能为空")
    @FieldParam("类型")
    private String type;
    @FieldParam("二维码值")
    private String qrCode;
    @FieldParam("二维码链接")
    private String qrCodeUrl;
    @FieldParam("门卫二维码")
    private String healthCode;
    @FieldParam("门卫二维码链接")
    private String healthCodeUrl;
    @FieldParam("标志")
    private String logo;
    @FieldParam("微信id")
    private String openId;
    @FieldParam("创建时间")
    private Date createDate;
    @FieldParam("部门id")
    private Integer deptId;
    @FieldParam(fieldType = FieldType.prikeyIncrement)
    private Integer id_prikey;
    @FieldParam(association = true)
    private String phone;
    @FieldParam(association = true)
    private String userName;
    @FieldParam("处理标识")
    private String flag;
}
