package com.hbsoft.wechat.bean;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import java.util.Date;
import lombok.Data;

@Data
@TableName("d_organize")
public class Organize extends APojo {

    private static final long serialVersionUID = 1L;

    @FieldParam(value = "ID_PRIKEY", fieldType = FieldType.prikeyIncrement)
    private Integer id_prikey;

    @FieldParam("组织名称")
    private String unitName;

    @FieldParam(value="opendId",association = true)
    private String opendId;

    @FieldParam("部门id")
    private String deptId;

    @FieldParam("创建时间")
    private Date createTime;

    @FieldParam("创建人")
    private String createPerson;
}
