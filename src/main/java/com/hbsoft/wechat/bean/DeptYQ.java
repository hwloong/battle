package com.hbsoft.wechat.bean;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.annotation.ver.NotNull_hb;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import lombok.Data;
import java.util.Date;
import java.util.List;

/**
 * 单位信息表（yyp）
 *
 */
@Data
@TableName("k_dept")
public class DeptYQ extends APojo {
    @FieldParam("单位编号")
    private String deptNo;
    @FieldParam("父级编号")
    private String deptParentNo;
    @FieldParam("集团邀请码")
    private String deptParentCode;
    @FieldParam("单位邀请码")
    private String deptCode;
    @FieldParam("集团邀请链接")
    private String deptParentUrl;
    @FieldParam("单位邀请链接")
    private String deptUrl;
    @NotNull_hb(message = "单位名称不能为空")
    @FieldParam("单位名称")
    private String dpetName;
    @NotNull_hb(message = "登记类型不能为空")
    @FieldParam("登记类型")
    private String type;
    @NotNull_hb(message = "是否登记体温不能为空")
    @FieldParam("登记体温")
    private String temperatureType;
    @FieldParam("创建人手机号")
    private String mobile;
    @FieldParam(association = true, forkeyId = "创建人手机号", forkeyName = "姓名", forkeyRelationId = "手机号码", forkeyTableName = "k_wx_user_info")
    private String mobileName;
    @FieldParam("创建时间")
    private Date createDate;
    @FieldParam("停用")
    private Integer disable;
    @FieldParam(fieldType = FieldType.prikeyIncrement)
    private Integer id_prikey;
    @FieldParam("图标")
    private String icon;
    @FieldParam("常驻人口")
    private Integer residentNum;
    @FieldParam("流动人口")
    private Integer flowNum;
    @FieldParam("疫区归来")
    private Integer epidemicReturnNum;
    @FieldParam(association = true)
    private List<DeptBindUser> bindUsers;
    //是否有下级部门
    @FieldParam(association = true)
    private Integer nextNum;
}
