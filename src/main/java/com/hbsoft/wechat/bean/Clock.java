package com.hbsoft.wechat.bean;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import lombok.Data;
import org.jboss.logging.Field;

import java.util.Date;

@Data
@TableName("k_clock")
public class Clock extends APojo {

    private static final long serialVersionUID = 1L;

    @FieldParam(value="ID_PRIKEY", fieldType= FieldType.prikeyIncrement)
    private Integer id_prikey;

    @FieldParam(value = "人员姓名",association = true)
    private String name;

    @FieldParam("opendId")
    private String opendId;

    @FieldParam("体温")
    private Double temperature;

    @FieldParam("经度")
    private Double longitude;

    @FieldParam("纬度")
    private Double latitude;

    @FieldParam("打卡地点")
    private String clockLocal;

    @FieldParam("打卡时间")
    private Date clockTime;

    @FieldParam(value="开始时间", association = true)
    private String startDate;

    @FieldParam(value = "结束时间",association = true)
    private String endDate;
}
