package com.hbsoft.wechat.bean;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.annotation.ver.NotNull_hb;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import lombok.Data;
import java.util.Date;

@Data
@TableName("k_dept_outbreak_log")
public class DeptOutbreakLog extends APojo {
    @NotNull_hb(message = "日期不能为空")
    @FieldParam("日期")
    private Date date;
    @NotNull_hb(message = "部门id不能为空")
    @FieldParam(value = "部门id", fieldType = FieldType.forkey)
    private Integer deptId;
    @FieldParam("创建日期")
    private Date createDate;
    @FieldParam("修改日期")
    private Date updateDate;
    @NotNull_hb(message = "常驻人口不能为空")
    @FieldParam("常驻人口")
    private Integer residentNum;
//    @NotNull_hb(message = "流动人口不能为空")
    @FieldParam("流动人口")
    private Integer flowNum;
//    @NotNull_hb(message = "疫区归来不能为空")
    @FieldParam("疫区归来")
    private Integer epidemicReturnNum;
//    @NotNull_hb(message = "职工总数不能为空")
    @FieldParam("职工总数")
    private Integer countNum;
//    @NotNull_hb(message = "疫情排查情况不能为空")
    @FieldParam("疫情排查情况")
    private String outbreakSituation;
//    @NotNull_hb(message = "返工复产情况不能为空")
    @FieldParam("返工复产情况")
    private String reworkSituation;
    @FieldParam("返工复产日期")
    private Date reworkDate;
//    @NotNull_hb(message = "外来务工不能为空")
    @FieldParam("外来务工")
    private Integer migrantNum;
//    @NotNull_hb(message = "防疫措施不能为空")
    @FieldParam("防疫措施")
    private String preventionMeasures;
    @FieldParam(fieldType = FieldType.prikeyIncrement)
    private Integer id_prikey;
}
