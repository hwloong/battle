package com.hbsoft.wechat.bean;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.annotation.ver.NotNull_hb;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import lombok.Data;
import java.util.Date;

@Data
@TableName("k_dept_return_work")
public class DeptReturnWork extends APojo {
    private String d001;
    private String d002;
    private String d003;
    private String d004;
    private String d005;
    private String d006;
    private String d007;
    private String d008;
    private String d009;
    private String d010;
    private String d011;
    private String d012;
    private String d013;
    private String d014;
    private String d015;
    private String d016;
    private String d017;
    private String d018;
    private String d019;
    private String d020;
    private String d021;
    private String d022;
    private String d023;
    private String d024;
    private String d025;
    private String d026;
    private String d027;
    @FieldParam("部门id")
    @NotNull_hb(message = "部门id不能为空")
    private Integer deptId;
    @FieldParam("类型")
    @NotNull_hb(message = "企业类型不能为空")
    private String type;
    @FieldParam("创建日期")
    private Date createDate;
    @FieldParam(fieldType = FieldType.prikeyIncrement)
    private Integer id_prikey;
}
