package com.hbsoft.wechat.bean;

import lombok.Data;

@Data
public class BaiDuMapInfo {

    private String error;

    private String x;

    private String y;
}
