package com.hbsoft.wechat.bean;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import lombok.Data;

import java.util.Date;

@Data
@TableName("k_wx_user_info")
public class WXUser extends APojo {

    @FieldParam(value = "ID_PRIKEY", fieldType = FieldType.prikeyIncrement)
    private Integer id_prikey;

    @FieldParam(value = "openid")
    private String openid;

    @FieldParam(value = "手机号码")
    private String phone;

    @FieldParam(value = "密码")
    private String password;

    @FieldParam(value = "姓名")
    private String userName;

    @FieldParam(value = "组织编号")
    private String orgCode;

    @FieldParam(value = "创建时间")
    private Date createOn;

    @FieldParam(value = "更新时间")
    private Date updateOn;

    @FieldParam(value = "打卡时间",association = true)
    private Date clockTime;

    @FieldParam(value = "体温",association = true)
    private Double temperature;

    @FieldParam("家庭地址")
    private String address;

    @FieldParam("性别")
    private String gender;

    @FieldParam("年龄")
    private Integer age;

    @FieldParam("职业")
    private String job;


    @FieldParam("身份证号码")
    private String idCard;


    @FieldParam("复工时间")
    private Date workDate;


    @FieldParam("车次")
    private String trainNum;


    @FieldParam("目前身体状况")
    private String health;

    @FieldParam("备注")
    private String remark;


    @FieldParam("是否去疫区")
    private String field1;

    @FieldParam("是否密切接触")
    private String field2;

    @FieldParam(value = "单位名称")
    private String deptName;

}
