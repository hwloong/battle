package com.hbsoft.wechat.bean;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@TableName("k_case")
public class WxCase extends APojo {

    @FieldParam(value = "ID_PRIKEY", fieldType = FieldType.prikeyIncrement)
    private Integer id_prikey;

    @FieldParam(value = "病例姓名")
    private String caseName;

    @FieldParam(value = "家庭坐标")
    private String homeCoordinate;

    @FieldParam(value = "所在区县")
    private String counties;

    @FieldParam(value = "性别")
    private String sex;

    @FieldParam(value = "年龄")
    private String age;

    @FieldParam(value = "籍贯")
    private String place;

    @FieldParam(value = "备注")
    private String remarks;


    @FieldParam(value = "创建时间")
    private Date createDate;

    @FieldParam(value = "创建人")
    private String createBy;

    @FieldParam(value = "更新时间")
    private Date updateDate;

    @FieldParam(value = "更新人")
    private String updateBy;

    private List<WxActivePath> wxActivePath;

    @Override
    public String toString() {
        return "WxCase{" +
                "id_prikey=" + id_prikey +
                ", caseName='" + caseName + '\'' +
                ", homeCoordinate='" + homeCoordinate + '\'' +
                ", counties='" + counties + '\'' +
                ", sex='" + sex + '\'' +
                ", age='" + age + '\'' +
                ", place='" + place + '\'' +
                ", remarks='" + remarks + '\'' +
                ", createDate=" + createDate +
                ", createBy='" + createBy + '\'' +
                ", updateDate=" + updateDate +
                ", updateBy='" + updateBy + '\'' +
                '}';
    }
}
