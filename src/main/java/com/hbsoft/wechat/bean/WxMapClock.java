package com.hbsoft.wechat.bean;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import lombok.Data;

import java.util.Date;

@Data
@TableName("k_wx_map_clock")
public class WxMapClock extends APojo {

    private static final long serialVersionUID = 1L;

    @FieldParam(value="ID_PRIKEY", fieldType= FieldType.prikeyIncrement)
    private Integer id_prikey;

    @FieldParam("opendId")
    private String opendId;

    @FieldParam("打卡坐标")
    private String colockCoordinate;

    @FieldParam("打卡地点")
    private String clockLocal;

    @FieldParam("备注")
    private String remarks;

    @FieldParam(value="开始时间")
    private Date startDate;

    @FieldParam(value = "结束时间")
    private Date endDate;

    private String startToEndTime;
}
