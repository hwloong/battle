package com.hbsoft.wechat.bean;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import lombok.Data;
import java.util.Date;

@Data
@TableName("k_dept_bind_user")
public class DeptBindUser extends APojo {
    @FieldParam("手机号")
    private String mobile;
    @FieldParam("姓名")
    private String name;
    @FieldParam("单位id")
    private Integer deptId;
    @FieldParam("微信id")
    private String openId;
    @FieldParam("绑定时间")
    private Date createDate;
    @FieldParam("停用")
    private Integer disable;
    @FieldParam(fieldType = FieldType.prikeyIncrement)
    private Integer id_prikey;
}
