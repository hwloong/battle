package com.hbsoft.wechat.bean;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import lombok.Data;

import java.util.Date;


@Data
@TableName("k_person_register_from")
public class PersonnelRegisterFrom extends APojo {

    @FieldParam(value = "ID_PRIKEY", fieldType = FieldType.prikeyIncrement)
    private Integer id_prikey;

    @FieldParam(value = "人员编号")
    private Integer personId;

    @FieldParam(value = "企业编号")
    private Integer enterpriseId;

    @FieldParam(value = "姓名")
    private String name;

    @FieldParam(value = "性别")
    private String sex;

    @FieldParam(value = "年龄")
    private String age;

    @FieldParam(value = "职业")
    private String occupation;

    @FieldParam(value = "身份证号码")
    private String idCard;

    @FieldParam(value = "联系电话")
    private String number;

    @FieldParam(value = "家庭住址")
    private String homeAddress;

    @FieldParam(value = "返回时间")
    private Date backDate;

    @FieldParam(value = "所乘车次")
    private String trainRide;

    @FieldParam(value = "目前身体状况")
    private String ownPhysicalCondition;

    @FieldParam(value = "接触人员身体状况")
    private String othersPhysicalCondition;

    @FieldParam(value = "是否去过重点疫情区")
    private Integer isGo;

    @FieldParam(value = "具体情况")
    private String remarks;


}
