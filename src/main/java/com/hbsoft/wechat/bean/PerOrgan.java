package com.hbsoft.wechat.bean;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import lombok.Data;

import java.util.Date;

@Data
@TableName("k_person_organize")
public class PerOrgan extends APojo {

    private static final long serialVersionUID = 1L;

    @FieldParam(value = "ID_PRIKEY", fieldType = FieldType.prikeyIncrement)
    private Integer id_prikey;

    @FieldParam("人员编号")
    private String personId;

    @FieldParam("组织编号")
    private Integer organizeId;

    @FieldParam("绑定时间")
    private Date bindTime;

    @FieldParam("停用")
    private Integer stop;
}
