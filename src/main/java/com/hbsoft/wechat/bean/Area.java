package com.hbsoft.wechat.bean;

import lombok.Data;

import java.util.List;

@Data
public class Area {

    public String provinceName ;

    public String provinceShortName ;

    public int confirmedCount ;

    public int suspectedCount ;

    public int curedCount ;

    public int deadCount ;

    public String comment ;

    public int locationId ;

    public List<Cities> cities ;

    public String preProvinceName ;
}
