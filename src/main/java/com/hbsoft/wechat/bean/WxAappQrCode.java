package com.hbsoft.wechat.bean;

import lombok.Data;

@Data
public class WxAappQrCode {

    private String path;

    private String width;
}
