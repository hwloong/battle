package com.hbsoft.wechat.bean;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import lombok.Data;

import java.util.Date;
import java.util.List;


@Data
@TableName("k_active_path")
public class WxActivePath extends APojo {

    @FieldParam(value = "ID_PRIKEY", fieldType = FieldType.prikeyIncrement)
    private Integer id_prikey;

    @FieldParam(value = "病例Id")
    private Integer caseId;

    @FieldParam(value = "坐标名称")
    private String coordinateName;

    @FieldParam(value = "审核坐标名称")
    private String examineCoordinateName;

    @FieldParam(value = "坐标地址")
    private String coordinateAddress;

    @FieldParam(value = "审核坐标地址")
    private String examineCoordinateAddress;

    @FieldParam(value = "是否审核")
    private Integer isExamine;//1已经审核 2 未审核

    @FieldParam(value = "修改人")
    private String updateBy;//记录修改人的openId

    @FieldParam(value = "修改时间")
    private Date updateDate;

    @FieldParam(value = "审核人")
    private String examineBy;//记录审核的openId


    @FieldParam(value = "审核时间")
    private Date examineDate;

    @FieldParam(value = "所属区县")
    private String counties;

    @FieldParam(value = "审核所属区县")
    private String examineCounties;

    @FieldParam(value = "开始时间")
    private Date startTime;

    private Integer days;


}
