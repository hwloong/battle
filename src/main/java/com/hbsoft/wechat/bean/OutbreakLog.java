package com.hbsoft.wechat.bean;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.annotation.ver.NotNull_hb;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import com.hb.hbenum.OrderMode;
import lombok.Data;

import java.util.Date;

/**
 * 疫情日志（yyp）
 */
@Data
@TableName("k_outbreak_log")
public class OutbreakLog extends APojo {
    @NotNull_hb(message = "单位id不能为空")
    @FieldParam("单位id")
    private Integer deptId;
    @NotNull_hb(message = "人员id不能为空")
    @FieldParam("人员id")
    private Integer userId;
    @FieldParam(association = true, forkeyId = "人员id", forkeyName = "姓名", forkeyRelationId = "id_prikey", forkeyTableName = "k_dept_bind_user")
    private String userIdName;
    @NotNull_hb(message = "是否正常不能为空")
    @FieldParam("是否正常")
    private String normal;
    @FieldParam("体温")
    private Double temperature;
    @FieldParam("备注")
    private String note;
    @FieldParam("登记时间")
    private Date createDateTime;
    @NotNull_hb(message = "日期不能为空")
    @FieldParam(value = "日期", order = OrderMode.desc)
    private Date createDate;
    @FieldParam(fieldType = FieldType.prikeyIncrement)
    private Integer id_prikey;
}
