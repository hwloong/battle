package com.hbsoft.wechat.bean;

import com.hb.bean.APojo;
import lombok.Data;

@Data
public class ImportUserClockInInfo extends APojo {

    private String dpetName;

    private String name;

    private String mobile;

    private String temperature;

    private String normal;

    private String createDateTime;

    private String deptId;

    private String startDate;

    private String endDate;

    private String flag;

}
