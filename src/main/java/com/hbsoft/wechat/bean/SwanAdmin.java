package com.hbsoft.wechat.bean;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import lombok.Data;

@Data
@TableName("k_swan_admin")
public class SwanAdmin extends APojo {
    @FieldParam("卡口id")
    private Integer swanId;
    @FieldParam("微信id")
    private String openId;
    @FieldParam(association = true, forkeyId = "微信id", forkeyName = "姓名", forkeyRelationId = "openid", forkeyTableName = "k_wx_user_info")
    private String name;
    @FieldParam(association = true, forkeyId = "微信id", forkeyName = "手机号码", forkeyRelationId = "openid", forkeyTableName = "k_wx_user_info")
    private String mobile;
    @FieldParam(fieldType = FieldType.prikeyIncrement)
    private Integer id_prikey;
}
