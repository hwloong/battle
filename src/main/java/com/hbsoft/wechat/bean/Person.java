package com.hbsoft.wechat.bean;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import lombok.Data;

import java.util.Date;

@Data
@TableName(("k_wx_user_info"))
public class Person extends APojo {

    private static final long serialVersionUID = 1L;

    @FieldParam(value = "ID_PRIKEY", fieldType = FieldType.prikeyIncrement)
    private Integer id_prikey;

    @FieldParam("opendId")
    private String opendId;

    @FieldParam("手机号码")
    private String phone;

    @FieldParam(value="组织名称",association = true)
    private String organName;

    @FieldParam("密码")
    private String password;

    @FieldParam("姓名")
    private String name;

    @FieldParam("组织编号")
    private Integer organizeId;

    @FieldParam("更新时间")
    private Date updateTime;

    @FieldParam("创建时间")
    private Date createTime;
}
