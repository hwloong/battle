package com.hbsoft.wechat.bean;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@TableName("k_battle_data")
public class WxBattleRealTimeData extends APojo {

    @FieldParam(value = "ID_PRIKEY", fieldType = FieldType.prikeyIncrement)
    private Integer id_prikey;

    @FieldParam(value = "区域")
    private String area;

    @FieldParam(value = "确诊病例")
    private Integer confirmedCount;

    @FieldParam(value = "疑似病例")
    private Integer suspectedCount;

    @FieldParam(value = "治愈人数")
    private Integer curedCount;

    @FieldParam(value = "死亡人数")
    private Integer deadCount;

    @FieldParam(value = "备用字段1")
    private String bak1;

    @FieldParam(value = "备用字段2")
    private String bak2;

}
