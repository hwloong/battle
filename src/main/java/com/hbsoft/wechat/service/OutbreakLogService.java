package com.hbsoft.wechat.service;

import com.hb.bean.CallResult;
import com.hbsoft.wechat.bean.OutbreakLog;
import com.hbsoft.wechat.bean.WXUser;

import java.util.List;
import java.util.Map;

public interface OutbreakLogService {
    /**
     * 添加疫情日报
     *
     * @param outbreakLog 日报信息
     * @param wxUser      用户信息
     * @return
     * @throws Exception
     */
    CallResult<String> addOutbreakLog(OutbreakLog outbreakLog, WXUser wxUser) throws Exception;

    /**
     *
     * @param outbreakLog
     * @param wxUser
     * @return
     * @throws Exception
     */
    Map<String, Object> getMyPagingOutbreakLog(OutbreakLog outbreakLog, WXUser wxUser) throws Exception;
}
