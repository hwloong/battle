package com.hbsoft.wechat.service.impl;

import com.hb.bean.CallResult;
import com.hbsoft.wechat.bean.DeptOutbreakLog;
import com.hbsoft.wechat.bean.DeptYQ;
import com.hbsoft.wechat.bean.WXUser;
import com.hbsoft.wechat.dao.service.DeptOutbreakLogDaoService;
import com.hbsoft.wechat.dao.service.DeptYQDaoService;
import com.hbsoft.wechat.service.DeptOutbreakLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service("deptOutbreakLogService")
public class DeptOutbreakLogServiceImpl implements DeptOutbreakLogService {
    private DeptYQDaoService deptYQDaoService;
    private DeptOutbreakLogDaoService deptOutbreakLogDaoService;

    @Autowired
    public DeptOutbreakLogServiceImpl(DeptYQDaoService deptYQDaoService, DeptOutbreakLogDaoService deptOutbreakLogDaoService) {
        this.deptYQDaoService = deptYQDaoService;
        this.deptOutbreakLogDaoService = deptOutbreakLogDaoService;
    }

    @Transactional
    @Override
    public CallResult<String> addDeptOutbreakLog(DeptOutbreakLog deptOutbreakLog, WXUser wxUser) throws Exception {
        CallResult<String> result = new CallResult<>();
        DeptYQ byId = deptYQDaoService.getById(deptOutbreakLog.getDeptId());
        if (byId == null) {
            result.setCode(400);
            result.setMessage("未找到部门信息");
        } else {
            if (!wxUser.getPhone().equals(byId.getMobile())) {
                result.setCode(400);
                result.setMessage("您不是部门管理员");
            } else {
                DeptOutbreakLog log = new DeptOutbreakLog();
                log.setDeptId(deptOutbreakLog.getDeptId());
                log.setDate(deptOutbreakLog.getDate());
                DeptOutbreakLog byField = deptOutbreakLogDaoService.getByField(log);
                if (byField == null) {
                    deptOutbreakLog.setCreateDate(new Date());
                    deptOutbreakLogDaoService.add(deptOutbreakLog);
                } else {
                    deptOutbreakLog.setUpdateDate(new Date());
                    deptOutbreakLog.setId_prikey(byField.getId_prikey());
                    deptOutbreakLogDaoService.set(deptOutbreakLog);
                }
                result.setCode(0);
                result.setMessage("申报成功");
                DeptYQ deptYQ = new DeptYQ();
                deptYQ.setResidentNum(deptOutbreakLog.getResidentNum());
                deptYQ.setFlowNum(deptOutbreakLog.getFlowNum());
                deptYQ.setEpidemicReturnNum(deptOutbreakLog.getEpidemicReturnNum());
                deptYQ.setId_prikey(byId.getId_prikey());
                deptYQDaoService.set(deptYQ);
            }
        }
        return result;
    }

    @Override
    public CallResult<DeptOutbreakLog> getDeptOutbreakLog(DeptOutbreakLog deptOutbreakLog, WXUser wxUser) throws Exception {
        CallResult<DeptOutbreakLog> result = new CallResult<>();
        DeptOutbreakLog log = new DeptOutbreakLog();
        log.setDeptId(deptOutbreakLog.getDeptId());
        log.setDate(deptOutbreakLog.getDate());
        DeptOutbreakLog byField = deptOutbreakLogDaoService.getByField(log);
        if(byField!=null){
            result.setCode(0);
            result.setMessage("获取成功");
            result.setData(byField);
        }else{
            result.setCode(500);
            result.setMessage("暂无数据");
        }
        return result;
    }
}
