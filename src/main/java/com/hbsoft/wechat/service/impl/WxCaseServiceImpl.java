package com.hbsoft.wechat.service.impl;

import com.hb.bean.CallResult;
import com.hb.util.DateUtil;
import com.hb.util.StringUtil;
import com.hbsoft.wechat.bean.WxCase;
import com.hbsoft.wechat.bean.WxMapClock;
import com.hbsoft.wechat.dao.WxCaseDao;
import com.hbsoft.wechat.dao.WxMapClockDao;
import com.hbsoft.wechat.service.WxCaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

@Service("wxCaseService")
public class WxCaseServiceImpl implements WxCaseService {

    @Autowired
    private WxCaseDao wxCaseDao;

    @Autowired
    private WxMapClockDao wxMapClockDao;
    @Override
    public List<WxCase> getAlls(WxCase wxCase) throws Exception {
        return wxCaseDao.findAlls(wxCase);
    }

    @Override
    public Integer findIsEndTimeClock(WxMapClock wxMapClock) throws Exception {
        return wxCaseDao.findIsEndTimeClock(wxMapClock);
    }

    @Override
    public Integer findEndClock(WxMapClock wxMapClock) throws Exception {
        return wxCaseDao.findEndClock(wxMapClock);
    }

    @Override
    public List<WxMapClock> getAllsForWxMapClock(WxMapClock wxMapClock) throws Exception {
        return wxMapClockDao.getAllsForWxMapClock(wxMapClock);
    }

    @Override
    public WxMapClock getWxMapClockByOpenid(WxMapClock wxMapClock) throws Exception {
        return wxMapClockDao.getWxMapClockByOpenid(wxMapClock);
    }

}
