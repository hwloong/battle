package com.hbsoft.wechat.service.impl;

import com.hb.bean.CallResult;
import com.hbsoft.wechat.bean.DeptBindUser;
import com.hbsoft.wechat.bean.DeptYQ;
import com.hbsoft.wechat.bean.OutbreakLog;
import com.hbsoft.wechat.bean.WXUser;
import com.hbsoft.wechat.dao.service.DeptBindUserDaoService;
import com.hbsoft.wechat.dao.service.DeptYQDaoService;
import com.hbsoft.wechat.dao.service.OutbreakLogDaoService;
import com.hbsoft.wechat.service.OutbreakLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("outbreakLogService")
public class OutbreakLogServiceImpl implements OutbreakLogService {
    private OutbreakLogDaoService outbreakLogDaoService;
    private DeptYQDaoService deptYQDaoService;
    private DeptBindUserDaoService deptBindUserDaoService;

    @Autowired
    public OutbreakLogServiceImpl(OutbreakLogDaoService outbreakLogDaoService, DeptYQDaoService deptYQDaoService, DeptBindUserDaoService deptBindUserDaoService) {
        this.outbreakLogDaoService = outbreakLogDaoService;
        this.deptYQDaoService = deptYQDaoService;
        this.deptBindUserDaoService = deptBindUserDaoService;
    }

    @Override
    public CallResult<String> addOutbreakLog(OutbreakLog outbreakLog, WXUser wxUser) throws Exception {
        CallResult<String> result = new CallResult<>();
        DeptYQ byId = deptYQDaoService.getById(outbreakLog.getDeptId());
//        int num = 1;
        if (byId == null) {
            result.setCode(400);
            result.setMessage("未找到部门信息");
        }
//        if (result.isExec()) {
//            String type = byId.getType();
//            if ("每日二次".equals(type)) {
//                num = 2;
//            }
//        }
        if (result.isExec()) {
            String openId = wxUser.getOpenid();
            DeptBindUser bindUser = new DeptBindUser();
            bindUser.setDeptId(outbreakLog.getDeptId());
            bindUser.setId_prikey(outbreakLog.getId_prikey());
            bindUser.setOpenId(openId);
            DeptBindUser byField = deptBindUserDaoService.getByField(bindUser);
            if (byField == null) {
                result.setCode(400);
                result.setMessage("信息数据不正确");
            }
        }
//        if (result.isExec()) {
//            OutbreakLog log = new OutbreakLog();
//            log.setCreateDate(outbreakLog.getCreateDate());
//            log.setDeptId(outbreakLog.getDeptId());
//            log.setUserId(outbreakLog.getUserId());
//            List<OutbreakLog> all = outbreakLogDaoService.getAll(log);
//            if (all != null && all.size() >= num) {
//                result.setCode(400);
//                result.setMessage("已申报，不能重复申报");
//            }
//        }
        if (result.isExec()) {
            outbreakLogDaoService.add(outbreakLog);
            result.setCode(0);
            result.setMessage("申报成功");
        }
        return result;
    }

    @Override
    public Map<String, Object> getMyPagingOutbreakLog(OutbreakLog outbreakLog, WXUser wxUser) throws Exception {
        Map<String, Object> data = outbreakLogDaoService.getPagingData(outbreakLog);
        return data;
    }
}
