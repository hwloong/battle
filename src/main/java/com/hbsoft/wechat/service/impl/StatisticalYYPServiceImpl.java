package com.hbsoft.wechat.service.impl;

import com.hbsoft.wechat.dao.StatisticalYYPDao;
import com.hbsoft.wechat.service.StatisticalYYPService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service("statisticalYYPService")
public class StatisticalYYPServiceImpl implements StatisticalYYPService {
    private StatisticalYYPDao statisticalYYPDao;

    @Autowired
    public StatisticalYYPServiceImpl(StatisticalYYPDao statisticalYYPDao) {
        this.statisticalYYPDao = statisticalYYPDao;
    }

    @Override
    public List<Map<String, Integer>> getOrganizeDaily(Integer organizeId, Date date) {
        return statisticalYYPDao.findOrganizeDaily(organizeId, date);
    }
}
