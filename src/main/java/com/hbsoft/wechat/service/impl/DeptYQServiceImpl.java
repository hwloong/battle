package com.hbsoft.wechat.service.impl;

import com.hb.bean.CallResult;
import com.hb.util.BasePassUtil;
import com.hb.util.DateUtil;
import com.hb.util.StringUtil;
import com.hbsoft.util.WechatUtil;
import com.hbsoft.wechat.bean.DeptBindUser;
import com.hbsoft.wechat.bean.DeptYQ;
import com.hbsoft.wechat.bean.OutbreakLog;
import com.hbsoft.wechat.bean.WXUser;
import com.hbsoft.wechat.dao.OtherYYPDao;
import com.hbsoft.wechat.dao.service.DeptBindUserDaoService;
import com.hbsoft.wechat.dao.service.DeptYQDaoService;
import com.hbsoft.wechat.dao.service.OutbreakLogDaoService;
import com.hbsoft.wechat.dao.service.WXUserDaoService;
import com.hbsoft.wechat.service.DeptYQService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("deptYQService")
public class DeptYQServiceImpl implements DeptYQService {
    private final static SimpleDateFormat FORMAT = DateUtil.DATE_YMD_ONE_FORMAT;
    private DeptYQDaoService deptYQDaoService;
    private DeptBindUserDaoService deptBindUserDaoService;
    private WechatUtil wechatUtil;
    private OtherYYPDao otherYYPDao;
    private OutbreakLogDaoService outbreakLogDaoService;

    @Autowired
    public DeptYQServiceImpl(DeptYQDaoService deptYQDaoService, DeptBindUserDaoService deptBindUserDaoService, WechatUtil wechatUtil, OtherYYPDao otherYYPDao, OutbreakLogDaoService outbreakLogDaoService) {
        this.deptYQDaoService = deptYQDaoService;
        this.deptBindUserDaoService = deptBindUserDaoService;
        this.wechatUtil = wechatUtil;
        this.otherYYPDao = otherYYPDao;
        this.outbreakLogDaoService = outbreakLogDaoService;
    }

    @Override
    @Transactional
    public CallResult<String> addDeptYQ(DeptYQ deptYQ, DeptBindUser bindUser) throws Exception {
        CallResult<String> result = new CallResult<>();
        if (!StringUtil.isEmpty(deptYQ.getDeptParentNo())) {
            DeptYQ parentDeptYQ = new DeptYQ();
            parentDeptYQ.setDeptParentCode(deptYQ.getDeptParentNo());
            DeptYQ byField = deptYQDaoService.getByField(parentDeptYQ);
            if (byField != null) {
                deptYQ.setDeptParentNo(byField.getDeptNo());
            }
        }
        DeptYQ deptYQ1 = new DeptYQ();
        deptYQ1.setDpetName(deptYQ.getDpetName());
        List<DeptYQ> all = deptYQDaoService.getAll(deptYQ1);
        if (all != null && all.size() > 0) {
            DeptYQ deptYQ2 = all.get(0);
            DeptBindUser deptBindUser = new DeptBindUser();
            deptBindUser.setDeptId(deptYQ2.getId_prikey());
            deptBindUser.setName(bindUser.getName());
            DeptBindUser byField = deptBindUserDaoService.getByField(deptBindUser);
            if (byField == null) {
                bindUser.setDeptId(deptYQ2.getId_prikey());
                deptBindUserDaoService.add(bindUser);
            }
            result.setCode(400);
            result.setMessage("该企业名称已存在(创建人：*" + deptYQ2.getMobileName().substring(1) + ")，已添加您到该企业中");
        } else {
            Integer id_prikey = deptYQDaoService.addPrikey(deptYQ);
            if (id_prikey != null && id_prikey > 0) {
                bindUser.setDeptId(id_prikey);
                deptBindUserDaoService.add(bindUser);
                Integer count = otherYYPDao.findUserNameCount(bindUser.getMobile());
                if (!(count != null && count > 0)) {
                    Integer userId = null;
                    Map<String, Object> param = new HashMap<>();
                    String pass = BasePassUtil.passTransformation(bindUser.getMobile(), BasePassUtil.base64_encode("123456"));
                    param.put("name", bindUser.getMobile());
                    param.put("pass", pass);
                    param.put("describe", bindUser.getName());
                    Integer i = otherYYPDao.insertUser(param);
                    if (i == 1) {
                        userId = (Integer) param.get("id_prikey");
                        if (userId != null) {
                            otherYYPDao.insertUserRole(118, userId);
                        }
                    }
                }
            }
            result.setCode(0);
            result.setMessage("添加部门成功");
        }
        return result;
    }

    @Override
    public CallResult<List<DeptYQ>> getMyDeptYQAllAdmin(String mobile) throws Exception {
        CallResult<List<DeptYQ>> result = new CallResult<>();
        DeptYQ deptYQ = new DeptYQ();
        deptYQ.setMobile(mobile);
        deptYQ.setDisable(0);
        List<DeptYQ> all = deptYQDaoService.getAll(deptYQ);
        result.setCode(0);
        result.setMessage("获取成功");
        result.setData(all);
        return result;
    }

    @Override
    public CallResult<DeptYQ> getDeptParentCode(String deptNo, String mobile) throws Exception {
        CallResult<DeptYQ> result = new CallResult<>();
        DeptYQ deptYQ = new DeptYQ();
        deptYQ.setDeptNo(deptNo);
        deptYQ.setMobile(mobile);
        DeptYQ byField = deptYQDaoService.getByField(deptYQ);
        if (byField != null) {
            String deptParentCode = byField.getDeptParentCode();
            if (StringUtil.isEmpty(deptParentCode)) {
                while (true) {
                    String s = StringUtil.randomNumId(6);
                    String code = s.substring(s.length() - 6);
                    code = code.replace("4", "0");
                    DeptYQ deptYQ1 = new DeptYQ();
                    deptYQ1.setDeptParentCode(code);
                    List<DeptYQ> all = deptYQDaoService.getAll(deptYQ1);
                    if (all.size() == 0) {
                        deptParentCode = code;
                        break;
                    }
                }
                String deptParentUrl = wechatUtil.getWxAappQrCode("jt_" + deptParentCode);
                DeptYQ deptYQCode = new DeptYQ();
                deptYQCode.setId_prikey(byField.getId_prikey());
                deptYQCode.setDeptParentCode(deptParentCode);
                deptYQCode.setDeptParentUrl(deptParentUrl);
                deptYQDaoService.set(deptYQCode);
                result.setCode(0);
                result.setMessage("获取成功");
                deptYQCode.setId_prikey(null);
                result.setData(deptYQCode);
            } else {
                DeptYQ deptYQCode = new DeptYQ();
                deptYQCode.setDeptParentCode(byField.getDeptParentCode());
                deptYQCode.setDeptParentUrl(byField.getDeptParentUrl());
                result.setCode(0);
                result.setMessage("获取成功");
                result.setData(deptYQCode);
            }
        } else {
            result.setCode(400);
            result.setMessage("未找到部门信息");
        }
        return result;
    }

    @Override
    public CallResult<DeptYQ> getDeptCode(String deptNo, String mobile) throws Exception {
        CallResult<DeptYQ> result = new CallResult<>();
        DeptYQ deptYQ = new DeptYQ();
        deptYQ.setDeptNo(deptNo);
        deptYQ.setMobile(mobile);
        DeptYQ byField = deptYQDaoService.getByField(deptYQ);
        if (byField != null) {
            String deptCode = byField.getDeptCode();
            if (StringUtil.isEmpty(deptCode)) {
                while (true) {
                    String s = StringUtil.randomNumId(6);
                    String code = s.substring(s.length() - 6);
                    code = code.replace("4", "0");
                    DeptYQ deptYQ1 = new DeptYQ();
                    deptYQ1.setDeptCode(code);
                    List<DeptYQ> all = deptYQDaoService.getAll(deptYQ1);
                    if (all.size() == 0) {
                        deptCode = code;
                        break;
                    }
                }
                String deptUrl = wechatUtil.getWxAappQrCode("bm_" + deptCode);
                DeptYQ deptYQCode = new DeptYQ();
                deptYQCode.setId_prikey(byField.getId_prikey());
                deptYQCode.setDeptCode(deptCode);
                deptYQCode.setDeptUrl(deptUrl);
                deptYQDaoService.set(deptYQCode);
                result.setCode(0);
                result.setMessage("获取成功");
                deptYQCode.setId_prikey(null);
                result.setData(deptYQCode);
            } else {
                DeptYQ deptYQCode = new DeptYQ();
                deptYQCode.setDeptCode(byField.getDeptCode());
                deptYQCode.setDeptUrl(byField.getDeptUrl());
                result.setCode(0);
                result.setMessage("获取成功");
                result.setData(deptYQCode);
            }
        } else {
            result.setCode(400);
            result.setMessage("未找到部门信息");
        }
        return result;
    }

    @Override
    public CallResult<Map<String, String>> getDeptCodeToDeptName(String deptCode, String mobile) throws Exception {
        CallResult<Map<String, String>> result = new CallResult<>();
        DeptYQ deptYQ = new DeptYQ();
        deptYQ.setDeptCode(deptCode);
        DeptYQ byField = deptYQDaoService.getByField(deptYQ);
        if (byField != null) {
            Map<String, String> data = new HashMap<>();
//            DeptBindUser deptBindUser = new DeptBindUser();
////            deptBindUser.setDeptId(byField.getId_prikey());
////            deptBindUser.setMobile(mobile);
////            List<DeptBindUser> all = deptBindUserDaoService.getAll(deptBindUser);
////            if (all != null && all.size() > 0) {
////                data.put("name", all.get(0).getName());
////            }
            data.put("deptName", byField.getDpetName());
            result.setCode(0);
            result.setMessage("获取成功");
            result.setData(data);
        } else {
            result.setCode(400);
            result.setMessage("未找到部门信息");
        }
        return result;
    }

    @Override
    public CallResult<Map<String, String>> getDeptParentCodeToDeptName(String deptCode) throws Exception {
        CallResult<Map<String, String>> result = new CallResult<>();
        DeptYQ deptYQ = new DeptYQ();
        deptYQ.setDeptParentCode(deptCode);
        DeptYQ byField = deptYQDaoService.getByField(deptYQ);
        if (byField != null) {
            Map<String, String> data = new HashMap<>();
            data.put("deptName", byField.getDpetName());
            result.setCode(0);
            result.setMessage("获取成功");
            result.setData(data);
        } else {
            result.setCode(400);
            result.setMessage("未找到部门信息");
        }
        return result;
    }

    @Override
    public CallResult<String> getNameToDept(String deptCode, String name, WXUser user) throws Exception {
        CallResult<String> result = new CallResult<>();
        DeptYQ deptYQ = new DeptYQ();
        deptYQ.setDeptCode(deptCode);
        DeptYQ byField = deptYQDaoService.getByField(deptYQ);
        if (byField != null) {
            DeptBindUser deptBindUser = new DeptBindUser();
            deptBindUser.setDeptId(byField.getId_prikey());
            deptBindUser.setName(name);
            DeptBindUser byField1 = deptBindUserDaoService.getByField(deptBindUser);
            if (byField1 == null) {
                String mobile = user.getPhone();
                DeptBindUser deptBindUser1 = new DeptBindUser();
                deptBindUser1.setDeptId(byField.getId_prikey());
                deptBindUser1.setMobile(mobile);
                DeptBindUser byField2 = deptBindUserDaoService.getByField(deptBindUser1);
                if (byField2 == null) {
                    DeptBindUser bindUser = new DeptBindUser();
                    bindUser.setMobile(mobile);
                    bindUser.setName(name);
                    bindUser.setDeptId(byField.getId_prikey());
                    bindUser.setOpenId(user.getOpenid());
                    bindUser.setCreateDate(new Date());
                    bindUser.setDisable(0);
                    deptBindUserDaoService.add(bindUser);
                    result.setCode(0);
                    result.setMessage("绑定成功");
                } else {
                    result.setCode(400);
                    result.setMessage("该手机号重复，不能再次加入");
                }
            } else {
                result.setCode(400);
                result.setMessage("该名字重复，请修改姓名");
            }
        } else {
            result.setCode(400);
            result.setMessage("未找到部门信息");
        }
        return result;
    }

    @Override
    public CallResult<List<Map<String, String>>> getMyDeptYQAll(WXUser user) throws Exception {
        CallResult<List<Map<String, String>>> result = new CallResult<>();
        String openId = user.getOpenid();
        List<Map<String, String>> list = otherYYPDao.findMyDeptYQAll(openId);
        result.setCode(0);
        result.setMessage("获取成功");
        result.setData(list);
        return result;
    }

    @Override
    public CallResult<Map<String, Object>> getMyDeptYQById(String deptNo, WXUser wxUser) throws Exception {
        CallResult<Map<String, Object>> result = new CallResult<>();
        DeptYQ deptYQ = new DeptYQ();
        deptYQ.setDeptNo(deptNo);
        deptYQ.setMobile(wxUser.getPhone());
        DeptYQ byField = deptYQDaoService.getByField(deptYQ);
        if (byField != null) {
            String deptParentNo = byField.getDeptParentNo();
            if (!StringUtil.isEmpty(deptParentNo)) {
                DeptYQ deptParentYQ = new DeptYQ();
                deptParentYQ.setDeptNo(deptParentNo);
                DeptYQ byField1 = deptYQDaoService.getByField(deptParentYQ);
                if (byField1 != null) {
                    byField.setDeptParentNo(byField1.getDeptParentCode());
                }
            }
            DeptBindUser bindUser = new DeptBindUser();
            bindUser.setDeptId(byField.getId_prikey());
            List<DeptBindUser> all = deptBindUserDaoService.getAll(bindUser);
            Map<String, Object> data = new HashMap<>();
            data.put("deptYQ", byField);
            data.put("bindUsers", all);
            result.setCode(0);
            result.setMessage("获取成功");
            result.setData(data);
        } else {
            result.setCode(400);
            result.setMessage("未找到部门信息");
        }
        return result;
    }

    @Override
    public CallResult<Map<String, Object>> getMyDeptYQStatistical(Date date, String deptNo, WXUser wxUser) throws Exception {
        CallResult<Map<String, Object>> result = new CallResult<>();
        DeptYQ deptYQ = new DeptYQ();
        deptYQ.setDeptNo(deptNo);
        deptYQ.setMobile(wxUser.getPhone());
        DeptYQ byField = deptYQDaoService.getByField(deptYQ);
        if (byField != null) {
            Map<String, Object> data = new HashMap<>();
            OutbreakLog log = new OutbreakLog();
            log.setDeptId(byField.getId_prikey());
            log.setCreateDate(date);
            List<OutbreakLog> all = outbreakLogDaoService.getAll(log);
            data.put("outbreakLogs", all);
            List<Map<String, Object>> myDeptYQStatistical = otherYYPDao.findMyDeptYQStatistical(byField.getId_prikey(), date);
            data.put("outbreakLogsStatistical", myDeptYQStatistical);
            List<Map<String, Object>> myDeptYQStatistical1 = otherYYPDao.findMyDeptYQStatistical1(byField.getId_prikey(), date);
            data.put("outbreakLogsStatistical7", myDeptYQStatistical1);
            result.setCode(0);
            result.setMessage("获取成功");
            result.setData(data);
        } else {
            result.setCode(400);
            result.setMessage("未找到部门信息");
        }
        return result;
    }

    @Override
    public CallResult<String> setDeptYQ(DeptYQ deptYQ) throws Exception {
        CallResult<String> result = new CallResult<>();
        if (!StringUtil.isEmpty(deptYQ.getDeptParentNo())) {
            DeptYQ parentDeptYQ = new DeptYQ();
            parentDeptYQ.setDeptParentCode(deptYQ.getDeptParentNo());
            DeptYQ byField = deptYQDaoService.getByField(parentDeptYQ);
            if (byField != null) {
                deptYQ.setDeptParentNo(byField.getDeptNo());
            }
        }
        deptYQDaoService.set(deptYQ);
        result.setCode(0);
        result.setMessage("修改部门成功");
        return result;
    }

    @Override
    public CallResult<List<Map<String, Object>>> getMyNextDeptYQ(String deptNo) throws Exception {
        CallResult<List<Map<String, Object>>> result = new CallResult<>();
        List<Map<String, Object>> nextDeptYQ = otherYYPDao.findNextDeptYQ(deptNo);
        result.setCode(0);
        result.setMessage("获取成功");
        result.setData(nextDeptYQ);
        return result;
    }

    @Override
    public CallResult<Map<String, Object>> getMyNextDeptYQStatistical(Date date, String deptNo) throws Exception {
        CallResult<Map<String, Object>> result = new CallResult<>();
        DeptYQ deptYQ = new DeptYQ();
        deptYQ.setDeptNo(deptNo);
        DeptYQ byField = deptYQDaoService.getByField(deptYQ);
        if (byField != null) {
            Map<String, Object> data = new HashMap<>();
            OutbreakLog log = new OutbreakLog();
            log.setDeptId(byField.getId_prikey());
            log.setCreateDate(date);
            List<OutbreakLog> all = outbreakLogDaoService.getAll(log);
            data.put("outbreakLogs", all);
            List<Map<String, Object>> myDeptYQStatistical = otherYYPDao.findMyDeptYQStatistical(byField.getId_prikey(), date);
            data.put("outbreakLogsStatistical", myDeptYQStatistical);
            List<Map<String, Object>> myDeptYQStatistical1 = otherYYPDao.findMyDeptYQStatistical1(byField.getId_prikey(), date);
            data.put("outbreakLogsStatistical7", myDeptYQStatistical1);
            result.setCode(0);
            result.setMessage("获取成功");
            result.setData(data);
        } else {
            result.setCode(400);
            result.setMessage("未找到部门信息");
        }
        return result;
    }

    @Override
    public CallResult<String> removeDeptYQUser(Integer id_prikey, WXUser wxUser) throws Exception {
        CallResult<String> result = new CallResult<>();
        DeptBindUser byId = deptBindUserDaoService.getById(id_prikey);
        if (byId == null) {
            result.setCode(400);
            result.setMessage("暂无信息不能删除");
        } else {
            String mobile = byId.getMobile();
            if (mobile.equals(wxUser.getPhone())) {
                result.setCode(400);
                result.setMessage("不能删除自己");
            } else {
                Integer deptId = byId.getDeptId();
                DeptYQ byId1 = deptYQDaoService.getById(deptId);
                if (byId1 == null) {
                    result.setCode(400);
                    result.setMessage("未找到部门信息");
                } else {
                    if (!byId1.getMobile().equals(wxUser.getPhone())) {
                        result.setCode(400);
                        result.setMessage("不是您管理的人员信息，不能删除");
                    } else {
                        deptBindUserDaoService.removeOne(id_prikey);
                        result.setCode(0);
                        result.setMessage("删除成功");
                    }
                }
            }
        }
        return result;
    }

    @Override
    public CallResult<Map<String, Object>> getIndexDataXJ() throws Exception {
        CallResult<Map<String, Object>> result = new CallResult<>();
        Map<String, Object> indexData = otherYYPDao.findIndexData();
        result.setCode(0);
        result.setMessage("获取成功");
        result.setData(indexData);
        return result;
    }

    @Override
    public CallResult<List<Map<String, Object>>> getNextDeptYQSummary(String deptNo, Date date) throws Exception {
        CallResult<List<Map<String, Object>>> result = new CallResult<>();
        String dateStr = FORMAT.format(date);
        List<Map<String, Object>> list = otherYYPDao.findNextDeptYQSummary(dateStr, date, deptNo);
        result.setCode(0);
        result.setMessage("获取成功");
        result.setData(list);
        return result;
    }

    @Override
    public CallResult<List<Map<String, Object>>> getNextDeptYQSituation(String deptNo) throws Exception {
        CallResult<List<Map<String, Object>>> result = new CallResult<>();
        List<Map<String, Object>> list = otherYYPDao.findNextDeptYQSituation(deptNo);
        result.setCode(0);
        result.setMessage("获取成功");
        result.setData(list);
        return result;
    }
}
