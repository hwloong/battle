package com.hbsoft.wechat.service;

import com.hb.bean.CallResult;
import com.hbsoft.wechat.bean.DeptBindUser;
import com.hbsoft.wechat.bean.DeptYQ;
import com.hbsoft.wechat.bean.WXUser;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface DeptYQService {
    /**
     * 添加部门信息
     *
     * @param deptYQ   部门信息
     * @param bindUser 自己
     * @return
     * @throws Exception
     */
    CallResult<String> addDeptYQ(DeptYQ deptYQ, DeptBindUser bindUser) throws Exception;

    /**
     * 获取我管理的部门信息
     *
     * @param mobile
     * @return
     * @throws Exception
     */
    CallResult<List<DeptYQ>> getMyDeptYQAllAdmin(String mobile) throws Exception;

    /**
     * 生成集团邀请二维码
     *
     * @param deptNo 部门编号
     * @param mobile 创建人手机号
     * @return
     * @throws Exception
     */
    CallResult<DeptYQ> getDeptParentCode(String deptNo, String mobile) throws Exception;

    /**
     * 生成部门邀请二维码
     *
     * @param deptNo 部门编号
     * @param mobile 创建人手机号
     * @return
     * @throws Exception
     */
    CallResult<DeptYQ> getDeptCode(String deptNo, String mobile) throws Exception;

    /**
     * 根据部门邀请码获取部门名称
     *
     * @param deptCode 部门邀请码
     * @param mobile   用户手机号
     * @return
     * @throws Exception
     */
    CallResult<Map<String, String>> getDeptCodeToDeptName(String deptCode, String mobile) throws Exception;

    /**
     * 根据集团邀请码获取部门名称
     *
     * @param deptCode 集团邀请码
     * @return
     * @throws Exception
     */
    CallResult<Map<String, String>> getDeptParentCodeToDeptName(String deptCode) throws Exception;

    /**
     * 根据姓名查询是否可以绑定到部门
     *
     * @param deptCode 部门邀请码
     * @param name     姓名
     * @param user     用户信息
     * @return
     * @throws Exception
     */
    CallResult<String> getNameToDept(String deptCode, String name, WXUser user) throws Exception;

    /**
     * @param user
     * @return
     * @throws Exception
     */
    CallResult<List<Map<String, String>>> getMyDeptYQAll(WXUser user) throws Exception;

    /**
     * 获取我的部门信息
     *
     * @param deptNo
     * @param wxUser
     * @return
     * @throws Exception
     */
    CallResult<Map<String, Object>> getMyDeptYQById(String deptNo, WXUser wxUser) throws Exception;

    /**
     * 获取我的部门统计信息
     *
     * @param date   日期
     * @param deptNo 部门id
     * @param wxUser 用户信息
     * @return
     * @throws Exception
     */
    CallResult<Map<String, Object>> getMyDeptYQStatistical(Date date, String deptNo, WXUser wxUser) throws Exception;

    /**
     * 修改部门信息
     *
     * @param deptYQ 部门信息
     * @return
     * @throws Exception
     */
    CallResult<String> setDeptYQ(DeptYQ deptYQ) throws Exception;

    /**
     * 获取我的下级部门信息
     *
     * @param deptNo 部门id
     * @return
     * @throws Exception
     */
    CallResult<List<Map<String, Object>>> getMyNextDeptYQ(String deptNo) throws Exception;

    /**
     * 获取我的部门统计信息
     *
     * @param date   日期
     * @param deptNo 部门id
     * @return
     * @throws Exception
     */
    CallResult<Map<String, Object>> getMyNextDeptYQStatistical(Date date, String deptNo) throws Exception;

    /**
     * 删除绑定人员
     *
     * @param id_prikey 绑定主键
     * @param wxUser    用户信息
     * @return
     * @throws Exception
     */
    CallResult<String> removeDeptYQUser(Integer id_prikey, WXUser wxUser) throws Exception;

    /**
     * 获取首页数据
     *
     * @return
     * @throws Exception
     */
    CallResult<Map<String, Object>> getIndexDataXJ() throws Exception;

    /**
     * 获取下级疫情日报汇总
     *
     * @param deptNo 部门编号
     * @param date   日期
     * @return
     * @throws Exception
     */
    CallResult<List<Map<String, Object>>> getNextDeptYQSummary(String deptNo, Date date) throws Exception;

    /**
     * 获取下级疫情概览查询
     *
     * @param deptNo 部门编号
     * @return
     * @throws Exception
     */
    CallResult<List<Map<String, Object>>> getNextDeptYQSituation(String deptNo) throws Exception;

}
