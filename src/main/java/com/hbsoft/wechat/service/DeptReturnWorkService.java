package com.hbsoft.wechat.service;

import com.hb.bean.CallResult;
import com.hbsoft.wechat.bean.DeptReturnWork;
import com.hbsoft.wechat.bean.WXUser;

import java.util.List;
import java.util.Map;

public interface DeptReturnWorkService {
    /**
     * 添加返工备案信息
     *
     * @param deptReturnWork 返工备案信息
     * @param wxUser         用户信息
     * @return
     * @throws Exception
     */
    CallResult<DeptReturnWork> addDeptReturnWork(DeptReturnWork deptReturnWork, WXUser wxUser) throws Exception;

    /**
     * 获取
     * @param type
     * @return
     * @throws Exception
     */
    CallResult<List<Map<String,Object>>> getDeptReturnWorkDic(String type) throws Exception;
}
