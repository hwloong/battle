package com.hbsoft.wechat.service;

import com.hb.util.BasePassUtil;
import com.hbsoft.util.StringUtil;
import com.hbsoft.wechat.dao.OtherYYPDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class HBUserService {

    @Autowired
    private OtherYYPDao otherYYPDao;

    public void addHBUser(String mobile,int roleId,String name) throws  Exception{
        Integer count = otherYYPDao.findUserNameCount(mobile);
        if (!(count != null && count > 0)) {
            Integer userId = null;
            Map<String, Object> param = new HashMap<>();
            String pass = BasePassUtil.passTransformation(mobile, BasePassUtil.base64_encode(StringUtil.getCode(6)));
            param.put("name", mobile);
            param.put("pass", pass);
            param.put("describe", name);
            Integer i = otherYYPDao.insertUser(param);
            if (i == 1) {
                userId = (Integer) param.get("id_prikey");
                if (userId != null) {
                    otherYYPDao.insertUserRole(roleId, userId);
                }
            }
        }
    }
}
