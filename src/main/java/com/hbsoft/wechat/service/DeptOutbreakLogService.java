package com.hbsoft.wechat.service;

import com.hb.bean.CallResult;
import com.hbsoft.wechat.bean.DeptOutbreakLog;
import com.hbsoft.wechat.bean.WXUser;

public interface DeptOutbreakLogService {
    /**
     * 添加部门日志
     *
     * @param deptOutbreakLog 部门日志信息
     * @param wxUser 用户信息
     * @return
     * @throws Exception
     */
    CallResult<String> addDeptOutbreakLog(DeptOutbreakLog deptOutbreakLog, WXUser wxUser) throws Exception;

    /**
     * 获取部门日志
     * @param deptOutbreakLog 部门日志信息
     * @param wxUser 用户信息
     * @return
     * @throws Exception
     */
    CallResult<DeptOutbreakLog> getDeptOutbreakLog(DeptOutbreakLog deptOutbreakLog, WXUser wxUser) throws Exception;
}
