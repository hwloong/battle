package com.hbsoft.wechat.service;

import com.hb.bean.CallResult;
import com.hbsoft.wechat.bean.Swan;
import com.hbsoft.wechat.bean.SwanLog;
import com.hbsoft.wechat.bean.WXUser;

import java.util.List;
import java.util.Map;

public interface SwanService {
    /**
     * 添加卡口信息
     *
     * @param swan 卡口信息
     * @return
     * @throws Exception
     */
    CallResult<String> addSwan(Swan swan) throws Exception;

    /**
     * 添加卡口门卫信息
     *
     * @param HealthCode 门卫邀请码
     * @param wxUser     用户信息
     * @return
     * @throws Exception
     */
    CallResult<String> addSwanAdmin(String HealthCode, WXUser wxUser) throws Exception;

    /**
     * 添加卡口流水信息
     *
     * @param swanLog 卡口流水信息
     * @return
     * @throws Exception
     */
    CallResult<String> addSwanLog(SwanLog swanLog) throws Exception;

    /**
     * 分页获取卡口流水信息
     *
     * @param swanLog 卡口流水信息
     * @return
     * @throws Exception
     */
    Map<String, Object> getPagingSwanLog(SwanLog swanLog) throws Exception;

    /**
     * 根据卡口二维码获取卡口信息和用户最后一次再该卡口的填报记录
     *
     * @param qrCode 卡口二维码
     * @param wxUser 用户信息
     * @return
     * @throws Exception
     */
    CallResult<Map<String, Object>> getSwanName(String qrCode, WXUser wxUser) throws Exception;

    /**
     * 获取我的卡口二维码
     *
     * @param id_prikey 卡口id
     * @return
     * @throws Exception
     */
    CallResult<Swan> getSwanCode(Integer id_prikey) throws Exception;

    /**
     * 获取我的卡口门卫二维码
     *
     * @param id_prikey 卡口id
     * @return
     * @throws Exception
     */
    CallResult<Swan> getSwanAdminCode(Integer id_prikey) throws Exception;

    /**
     * 根据卡口填写部门信息
     * @param id_prikey 卡口id
     * @return
     * @throws Exception
     */
    CallResult<List<Map<String,Object>>> getSwanDeptName(Integer id_prikey,String deptName) throws Exception;

    /**
     * 获取我的卡口列表
     *
     * @param openId 微信id
     * @return
     * @throws Exception
     */
    CallResult<List<Swan>> getMySwanAll(String openId) throws Exception;

    /**
     * 修改部门
     * @param swan
     * @return
     * @throws Exception
     */
    CallResult<String> setSwan(Swan swan)throws Exception;

    /**
     *
     * @param id_prikey
     * @return
     * @throws Exception
     */
    CallResult<Map<String, Object>> getSwanById(Integer id_prikey) throws Exception;

    /**
     * 移除卡口门卫
     * @param id_prikey
     * @return
     * @throws Exception
     */
    CallResult<String> removeSwanAdmin(Integer id_prikey) throws Exception;
}
