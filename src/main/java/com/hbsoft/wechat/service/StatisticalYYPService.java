package com.hbsoft.wechat.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface StatisticalYYPService {
    /**
     * 组织日报
     *
     * @param organizeId 组织id
     * @param date       年月日
     * @return
     */
    List<Map<String, Integer>> getOrganizeDaily(Integer organizeId, Date date);
}
