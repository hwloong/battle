package com.hbsoft.wechat.service;

import com.hbsoft.wechat.bean.WxCase;
import com.hbsoft.wechat.bean.WxMapClock;

import java.util.List;

public interface WxCaseService {
    List<WxCase> getAlls(WxCase wxCase) throws Exception;
    Integer findIsEndTimeClock(WxMapClock wxMapClock) throws Exception;
    Integer findEndClock(WxMapClock wxMapClock) throws Exception;
    List<WxMapClock> getAllsForWxMapClock(WxMapClock wxMapClock) throws Exception;
    WxMapClock getWxMapClockByOpenid(WxMapClock wxMapClock) throws Exception;
}
