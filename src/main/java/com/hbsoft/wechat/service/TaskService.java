package com.hbsoft.wechat.service;

import com.hbsoft.util.WechatUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration      //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling   // 2.开启定时任务
public class TaskService {

    @Autowired
    WechatUtil wechatUtil;

    @Autowired
    GetBattleService getBattleService;

    @Scheduled(cron = "0 0 0/2 * * ?")
    private void GetWxAppAccessToken() {
        wechatUtil.getWxAppAccessTokenTime();
    }

    @Scheduled(cron = "0 0 0/1 * * ?")
    private void GetBattle() {
        getBattleService.getBattleInfoTime();
    }
}
