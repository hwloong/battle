package com.hbsoft.wechat.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hbsoft.wechat.dao.BusPunchCardInfo;
import com.hbsoft.wechat.vo.BusVo;

@Service
public class BusPunchCardService {

	@Autowired
	BusPunchCardInfo busPunchCardInfo;
	
	public List<Map<String,Object>> getBusPunchCardInfo(BusVo vo){
		List<Map<String,Object>> list = busPunchCardInfo.getBusPunchCardInfo(vo);
		return list;
	}
	
	public Integer getBusPunchCardPageNum(BusVo vo){
		Integer i = busPunchCardInfo.getBusPunchCardPageNum(vo);
		return i;
	}
	
	public List<Map<String,Object>> busPunchCardCount(BusVo vo){
		List<Map<String,Object>> list = busPunchCardInfo.busPunchCardCount(vo);
		return list;
	}
	
	public Integer busPunchCardPageNum(BusVo vo){
		Integer i = busPunchCardInfo.busPunchCardPageNum(vo);
		return i;
	}
	
	public List<Map<String,Object>> getBusPunchCardInfo2(BusVo vo){
		List<Map<String,Object>> list = busPunchCardInfo.getBusPunchCardInfo2(vo);
		return list;
	}
	
	public List<Map<String,Object>> busPunchCardCount2(BusVo vo){
		List<Map<String,Object>> list = busPunchCardInfo.busPunchCardCount2(vo);
		return list;
	}
	
}
