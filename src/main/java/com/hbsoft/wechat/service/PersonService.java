package com.hbsoft.wechat.service;

import com.hb.bean.CallResult;
import com.hbsoft.wechat.bean.Organize;
import com.hbsoft.wechat.bean.PerOrgan;
import com.hbsoft.wechat.bean.Person;
import com.hbsoft.wechat.bean.WXUser;
import com.hbsoft.wechat.dao.service.PerOrganDaoService;
import com.hbsoft.wechat.dao.service.PersonDaoService;
import com.hbsoft.wechat.dao.service.WXUserDaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class PersonService {

    @Autowired
    WXUserDaoService wXUserDaoService;

    @Autowired
    PerOrganDaoService perOrganDaoService;

    public CallResult<String> joinOrganize(Organize organize) {
        CallResult<String> result = new CallResult<>();
        try{
            WXUser wxUser = new WXUser();
            wxUser.setOpenid(organize.getOpendId());
            WXUser wxUser1 = wXUserDaoService.getByField(wxUser);
            if (null != wxUser1) {
                PerOrgan po = new PerOrgan();
                po.setOrganizeId(organize.getId_prikey());
                po.setPersonId(organize.getOpendId());
                PerOrgan perOrgan = perOrganDaoService.getByField(po);
                if (null == perOrgan) {
                    po.setStop(0);
                    po.setBindTime(new Date());
                    perOrganDaoService.add(po);
                    result.setCode(0);
                    result.setMessage("加入成功");
                }else {
                    result.setCode(400);
                    result.setMessage("已加入该组织");
                }
            }else {
                result.setCode(400);
                result.setMessage("请注册");
            }
        }catch (Exception e) {
            result.setCode(400);
            result.setMessage("加入组织失败");
        }
        return result;
    }
}
