package com.hbsoft.wechat.dao;

import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.wechat.bean.WXUserLoginInfo;

@Mapper
public interface WXUserLoginInfoDao extends INewDao<WXUserLoginInfo,Integer> {
}
