package com.hbsoft.wechat.dao;

import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.wechat.bean.Clock;

@Mapper
public interface ClockDao extends INewDao<Clock,Integer> {
}
