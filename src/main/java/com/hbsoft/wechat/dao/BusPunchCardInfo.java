package com.hbsoft.wechat.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.hbsoft.wechat.vo.BusVo;

@Mapper
public interface BusPunchCardInfo {

	List<Map<String, Object>> getBusPunchCardInfo(BusVo vo);
	
	Integer getBusPunchCardPageNum(BusVo vo);

	List<Map<String, Object>> busPunchCardCount(BusVo vo);
	
	Integer busPunchCardPageNum(BusVo vo);

	List<Map<String, Object>> getBusPunchCardInfo2(BusVo vo);

	List<Map<String, Object>> busPunchCardCount2(BusVo vo);

}
