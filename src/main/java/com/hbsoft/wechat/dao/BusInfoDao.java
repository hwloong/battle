package com.hbsoft.wechat.dao;

import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.wechat.bean.BusInfo;

@Mapper
public interface BusInfoDao extends INewDao<BusInfo,Integer> {
}
