package com.hbsoft.wechat.dao;

import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.wechat.bean.DeptBindUser;

@Mapper
public interface DeptBindUserDao extends INewDao<DeptBindUser,Integer> {
}
