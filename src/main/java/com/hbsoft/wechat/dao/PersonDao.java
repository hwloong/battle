package com.hbsoft.wechat.dao;

import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.wechat.bean.Person;

@Mapper
public interface PersonDao extends INewDao<Person,Integer> {
}
