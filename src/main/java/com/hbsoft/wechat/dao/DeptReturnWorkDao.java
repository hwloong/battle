package com.hbsoft.wechat.dao;

import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.wechat.bean.DeptReturnWork;

@Mapper
public interface DeptReturnWorkDao extends INewDao<DeptReturnWork,Integer> {
}
