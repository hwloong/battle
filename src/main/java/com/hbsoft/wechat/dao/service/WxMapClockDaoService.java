package com.hbsoft.wechat.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.wechat.dao.WxMapClockDao;
import com.hbsoft.wechat.bean.WxMapClock;

@Service
public class WxMapClockDaoService implements INewService<WxMapClock,Integer> {
	@Autowired
	private WxMapClockDao wxMapClockDao;

	@Override
	public Boolean add(WxMapClock wxMapClock) throws Exception {
		Integer i = wxMapClockDao.insert(wxMapClock);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(WxMapClock wxMapClock) throws Exception {
		Integer prikey = null;
		Integer i = wxMapClockDao.insertPrikey(wxMapClock);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = wxMapClock.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<WxMapClock> list) throws Exception {
		Integer i = wxMapClockDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(WxMapClock wxMapClock) throws Exception {
		Integer i = wxMapClockDao.delete(wxMapClock);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = wxMapClockDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = wxMapClockDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(WxMapClock wxMapClock) throws Exception {
		Integer i = wxMapClockDao.update(wxMapClock);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(WxMapClock wxMapClock) throws Exception {
		Integer i = wxMapClockDao.updateEmpty(wxMapClock);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public WxMapClock getById(Integer id) throws Exception {
		return wxMapClockDao.findById(id);
	}

	@Override
	public WxMapClock getByField(WxMapClock wxMapClock) throws Exception {
		return wxMapClockDao.findByField(wxMapClock);
	}

	@Override
	public List<WxMapClock> getAll(WxMapClock wxMapClock) throws Exception {
		return wxMapClockDao.findAll(wxMapClock);
	}

	@Override
	public Map<String, Object> getPagingData(WxMapClock wxMapClock) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = wxMapClockDao.findPagingCount(wxMapClock);
		List<WxMapClock> list = wxMapClockDao.findPagingData(wxMapClock);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<WxMapClock> getTreeAll(WxMapClock wxMapClock) throws Exception {
		return wxMapClockDao.findTreeData(wxMapClock);
	}
}
