package com.hbsoft.wechat.dao.service;

import com.hbsoft.wechat.bean.Organize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.wechat.dao.WXUserDao;
import com.hbsoft.wechat.bean.WXUser;

@Service
public class WXUserDaoService implements INewService<WXUser,Integer> {
	@Autowired
	private WXUserDao wXUserDao;

	@Override
	public Boolean add(WXUser wXUser) throws Exception {
		Integer i = wXUserDao.insert(wXUser);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(WXUser wXUser) throws Exception {
		Integer prikey = null;
		Integer i = wXUserDao.insertPrikey(wXUser);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = wXUser.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<WXUser> list) throws Exception {
		Integer i = wXUserDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(WXUser wXUser) throws Exception {
		Integer i = wXUserDao.delete(wXUser);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = wXUserDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = wXUserDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(WXUser wXUser) throws Exception {
		Integer i = wXUserDao.update(wXUser);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(WXUser wXUser) throws Exception {
		Integer i = wXUserDao.updateEmpty(wXUser);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public WXUser getById(Integer id) throws Exception {
		return wXUserDao.findById(id);
	}

	@Override
	public WXUser getByField(WXUser wXUser) throws Exception {
		return wXUserDao.findByField(wXUser);
	}

	@Override
	public List<WXUser> getAll(WXUser wXUser) throws Exception {
		return wXUserDao.findAll(wXUser);
	}

	@Override
	public Map<String, Object> getPagingData(WXUser wXUser) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = wXUserDao.findPagingCount(wXUser);
		List<WXUser> list = wXUserDao.findPagingData(wXUser);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<WXUser> getTreeAll(WXUser wXUser) throws Exception {
		return wXUserDao.findTreeData(wXUser);
	}

	public List<WXUser> getOrganPerson(Organize organize) {
		return wXUserDao.getOrganPerson(organize);
	}
}
