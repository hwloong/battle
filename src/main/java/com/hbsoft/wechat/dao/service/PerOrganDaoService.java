package com.hbsoft.wechat.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.wechat.dao.PerOrganDao;
import com.hbsoft.wechat.bean.PerOrgan;

@Service
public class PerOrganDaoService implements INewService<PerOrgan,Integer> {
	@Autowired
	private PerOrganDao perOrganDao;

	@Override
	public Boolean add(PerOrgan perOrgan) throws Exception {
		Integer i = perOrganDao.insert(perOrgan);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(PerOrgan perOrgan) throws Exception {
		Integer prikey = null;
		Integer i = perOrganDao.insertPrikey(perOrgan);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = perOrgan.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<PerOrgan> list) throws Exception {
		Integer i = perOrganDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(PerOrgan perOrgan) throws Exception {
		Integer i = perOrganDao.delete(perOrgan);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = perOrganDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = perOrganDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(PerOrgan perOrgan) throws Exception {
		Integer i = perOrganDao.update(perOrgan);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(PerOrgan perOrgan) throws Exception {
		Integer i = perOrganDao.updateEmpty(perOrgan);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public PerOrgan getById(Integer id) throws Exception {
		return perOrganDao.findById(id);
	}

	@Override
	public PerOrgan getByField(PerOrgan perOrgan) throws Exception {
		return perOrganDao.findByField(perOrgan);
	}

	@Override
	public List<PerOrgan> getAll(PerOrgan perOrgan) throws Exception {
		return perOrganDao.findAll(perOrgan);
	}

	@Override
	public Map<String, Object> getPagingData(PerOrgan perOrgan) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = perOrganDao.findPagingCount(perOrgan);
		List<PerOrgan> list = perOrganDao.findPagingData(perOrgan);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<PerOrgan> getTreeAll(PerOrgan perOrgan) throws Exception {
		return perOrganDao.findTreeData(perOrgan);
	}
}
