package com.hbsoft.wechat.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.wechat.dao.WxBattleRealTimeDataDao;
import com.hbsoft.wechat.bean.WxBattleRealTimeData;

@Service
public class WxBattleRealTimeDataDaoService implements INewService<WxBattleRealTimeData,Integer> {
	@Autowired
	private WxBattleRealTimeDataDao wxBattleRealTimeDataDao;

	@Override
	public Boolean add(WxBattleRealTimeData wxBattleRealTimeData) throws Exception {
		Integer i = wxBattleRealTimeDataDao.insert(wxBattleRealTimeData);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(WxBattleRealTimeData wxBattleRealTimeData) throws Exception {
		Integer prikey = null;
		Integer i = wxBattleRealTimeDataDao.insertPrikey(wxBattleRealTimeData);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = wxBattleRealTimeData.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<WxBattleRealTimeData> list) throws Exception {
		Integer i = wxBattleRealTimeDataDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(WxBattleRealTimeData wxBattleRealTimeData) throws Exception {
		Integer i = wxBattleRealTimeDataDao.delete(wxBattleRealTimeData);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = wxBattleRealTimeDataDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = wxBattleRealTimeDataDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(WxBattleRealTimeData wxBattleRealTimeData) throws Exception {
		Integer i = wxBattleRealTimeDataDao.update(wxBattleRealTimeData);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(WxBattleRealTimeData wxBattleRealTimeData) throws Exception {
		Integer i = wxBattleRealTimeDataDao.updateEmpty(wxBattleRealTimeData);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public WxBattleRealTimeData getById(Integer id) throws Exception {
		return wxBattleRealTimeDataDao.findById(id);
	}

	@Override
	public WxBattleRealTimeData getByField(WxBattleRealTimeData wxBattleRealTimeData) throws Exception {
		return wxBattleRealTimeDataDao.findByField(wxBattleRealTimeData);
	}

	@Override
	public List<WxBattleRealTimeData> getAll(WxBattleRealTimeData wxBattleRealTimeData) throws Exception {
		return wxBattleRealTimeDataDao.findAll(wxBattleRealTimeData);
	}

	@Override
	public Map<String, Object> getPagingData(WxBattleRealTimeData wxBattleRealTimeData) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = wxBattleRealTimeDataDao.findPagingCount(wxBattleRealTimeData);
		List<WxBattleRealTimeData> list = wxBattleRealTimeDataDao.findPagingData(wxBattleRealTimeData);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<WxBattleRealTimeData> getTreeAll(WxBattleRealTimeData wxBattleRealTimeData) throws Exception {
		return wxBattleRealTimeDataDao.findTreeData(wxBattleRealTimeData);
	}
}
