package com.hbsoft.wechat.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.wechat.dao.SwanDao;
import com.hbsoft.wechat.bean.Swan;

@Service
public class SwanDaoService implements INewService<Swan,Integer> {
	@Autowired
	private SwanDao swanDao;

	@Override
	public Boolean add(Swan swan) throws Exception {
		Integer i = swanDao.insert(swan);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(Swan swan) throws Exception {
		Integer prikey = null;
		Integer i = swanDao.insertPrikey(swan);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = swan.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<Swan> list) throws Exception {
		Integer i = swanDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(Swan swan) throws Exception {
		Integer i = swanDao.delete(swan);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = swanDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = swanDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(Swan swan) throws Exception {
		Integer i = swanDao.update(swan);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(Swan swan) throws Exception {
		Integer i = swanDao.updateEmpty(swan);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Swan getById(Integer id) throws Exception {
		return swanDao.findById(id);
	}

	@Override
	public Swan getByField(Swan swan) throws Exception {
		return swanDao.findByField(swan);
	}

	@Override
	public List<Swan> getAll(Swan swan) throws Exception {
		return swanDao.findAll(swan);
	}

	@Override
	public Map<String, Object> getPagingData(Swan swan) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = swanDao.findPagingCount(swan);
		List<Swan> list = swanDao.findPagingData(swan);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<Swan> getTreeAll(Swan swan) throws Exception {
		return swanDao.findTreeData(swan);
	}
}
