package com.hbsoft.wechat.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.wechat.dao.DeptReturnWorkDao;
import com.hbsoft.wechat.bean.DeptReturnWork;

@Service
public class DeptReturnWorkDaoService implements INewService<DeptReturnWork,Integer> {
	@Autowired
	private DeptReturnWorkDao deptReturnWorkDao;

	@Override
	public Boolean add(DeptReturnWork deptReturnWork) throws Exception {
		Integer i = deptReturnWorkDao.insert(deptReturnWork);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(DeptReturnWork deptReturnWork) throws Exception {
		Integer prikey = null;
		Integer i = deptReturnWorkDao.insertPrikey(deptReturnWork);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = deptReturnWork.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<DeptReturnWork> list) throws Exception {
		Integer i = deptReturnWorkDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(DeptReturnWork deptReturnWork) throws Exception {
		Integer i = deptReturnWorkDao.delete(deptReturnWork);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = deptReturnWorkDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = deptReturnWorkDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(DeptReturnWork deptReturnWork) throws Exception {
		Integer i = deptReturnWorkDao.update(deptReturnWork);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(DeptReturnWork deptReturnWork) throws Exception {
		Integer i = deptReturnWorkDao.updateEmpty(deptReturnWork);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public DeptReturnWork getById(Integer id) throws Exception {
		return deptReturnWorkDao.findById(id);
	}

	@Override
	public DeptReturnWork getByField(DeptReturnWork deptReturnWork) throws Exception {
		return deptReturnWorkDao.findByField(deptReturnWork);
	}

	@Override
	public List<DeptReturnWork> getAll(DeptReturnWork deptReturnWork) throws Exception {
		return deptReturnWorkDao.findAll(deptReturnWork);
	}

	@Override
	public Map<String, Object> getPagingData(DeptReturnWork deptReturnWork) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = deptReturnWorkDao.findPagingCount(deptReturnWork);
		List<DeptReturnWork> list = deptReturnWorkDao.findPagingData(deptReturnWork);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<DeptReturnWork> getTreeAll(DeptReturnWork deptReturnWork) throws Exception {
		return deptReturnWorkDao.findTreeData(deptReturnWork);
	}
}
