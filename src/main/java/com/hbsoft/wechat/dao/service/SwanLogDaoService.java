package com.hbsoft.wechat.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.wechat.dao.SwanLogDao;
import com.hbsoft.wechat.bean.SwanLog;

@Service
public class SwanLogDaoService implements INewService<SwanLog,Integer> {
	@Autowired
	private SwanLogDao swanLogDao;

	@Override
	public Boolean add(SwanLog swanLog) throws Exception {
		Integer i = swanLogDao.insert(swanLog);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(SwanLog swanLog) throws Exception {
		Integer prikey = null;
		Integer i = swanLogDao.insertPrikey(swanLog);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = swanLog.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<SwanLog> list) throws Exception {
		Integer i = swanLogDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(SwanLog swanLog) throws Exception {
		Integer i = swanLogDao.delete(swanLog);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = swanLogDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = swanLogDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(SwanLog swanLog) throws Exception {
		Integer i = swanLogDao.update(swanLog);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(SwanLog swanLog) throws Exception {
		Integer i = swanLogDao.updateEmpty(swanLog);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public SwanLog getById(Integer id) throws Exception {
		return swanLogDao.findById(id);
	}

	@Override
	public SwanLog getByField(SwanLog swanLog) throws Exception {
		return swanLogDao.findByField(swanLog);
	}

	@Override
	public List<SwanLog> getAll(SwanLog swanLog) throws Exception {
		return swanLogDao.findAll(swanLog);
	}

	@Override
	public Map<String, Object> getPagingData(SwanLog swanLog) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = swanLogDao.findPagingCount(swanLog);
		List<SwanLog> list = swanLogDao.findPagingData(swanLog);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<SwanLog> getTreeAll(SwanLog swanLog) throws Exception {
		return swanLogDao.findTreeData(swanLog);
	}
}
