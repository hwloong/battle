package com.hbsoft.wechat.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.wechat.dao.OutbreakLogDao;
import com.hbsoft.wechat.bean.OutbreakLog;

@Service
public class OutbreakLogDaoService implements INewService<OutbreakLog,Integer> {
	@Autowired
	private OutbreakLogDao outbreakLogDao;

	@Override
	public Boolean add(OutbreakLog outbreakLog) throws Exception {
		Integer i = outbreakLogDao.insert(outbreakLog);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(OutbreakLog outbreakLog) throws Exception {
		Integer prikey = null;
		Integer i = outbreakLogDao.insertPrikey(outbreakLog);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = outbreakLog.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<OutbreakLog> list) throws Exception {
		Integer i = outbreakLogDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(OutbreakLog outbreakLog) throws Exception {
		Integer i = outbreakLogDao.delete(outbreakLog);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = outbreakLogDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = outbreakLogDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(OutbreakLog outbreakLog) throws Exception {
		Integer i = outbreakLogDao.update(outbreakLog);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(OutbreakLog outbreakLog) throws Exception {
		Integer i = outbreakLogDao.updateEmpty(outbreakLog);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public OutbreakLog getById(Integer id) throws Exception {
		return outbreakLogDao.findById(id);
	}

	@Override
	public OutbreakLog getByField(OutbreakLog outbreakLog) throws Exception {
		return outbreakLogDao.findByField(outbreakLog);
	}

	@Override
	public List<OutbreakLog> getAll(OutbreakLog outbreakLog) throws Exception {
		return outbreakLogDao.findAll(outbreakLog);
	}

	@Override
	public Map<String, Object> getPagingData(OutbreakLog outbreakLog) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = outbreakLogDao.findPagingCount(outbreakLog);
		List<OutbreakLog> list = outbreakLogDao.findPagingData(outbreakLog);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<OutbreakLog> getTreeAll(OutbreakLog outbreakLog) throws Exception {
		return outbreakLogDao.findTreeData(outbreakLog);
	}
}
