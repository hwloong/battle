package com.hbsoft.wechat.dao.service;

import com.hbsoft.wechat.bean.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.wechat.dao.OrganizeDao;
import com.hbsoft.wechat.bean.Organize;

@Service
public class OrganizeDaoService implements INewService<Organize,Integer> {
	@Autowired
	private OrganizeDao organizeDao;

	@Override
	public Boolean add(Organize organize) throws Exception {
		Integer i = organizeDao.insert(organize);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(Organize organize) throws Exception {
		Integer prikey = null;
		Integer i = organizeDao.insertPrikey(organize);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = organize.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<Organize> list) throws Exception {
		Integer i = organizeDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(Organize organize) throws Exception {
		Integer i = organizeDao.delete(organize);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = organizeDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = organizeDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(Organize organize) throws Exception {
		Integer i = organizeDao.update(organize);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(Organize organize) throws Exception {
		Integer i = organizeDao.updateEmpty(organize);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Organize getById(Integer id) throws Exception {
		return organizeDao.findById(id);
	}

	@Override
	public Organize getByField(Organize organize) throws Exception {
		return organizeDao.findByField(organize);
	}

	@Override
	public List<Organize> getAll(Organize organize) throws Exception {
		return organizeDao.findAll(organize);
	}

	@Override
	public Map<String, Object> getPagingData(Organize organize) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = organizeDao.findPagingCount(organize);
		List<Organize> list = organizeDao.findPagingData(organize);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<Organize> getTreeAll(Organize organize) throws Exception {
		return organizeDao.findTreeData(organize);
	}

    public List<Organize> findOrganByOpendId(Organize organize) {
		return organizeDao.findOrganByOpendId(organize);
    }
}
