package com.hbsoft.wechat.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.wechat.dao.DeptYQDao;
import com.hbsoft.wechat.bean.DeptYQ;

@Service
public class DeptYQDaoService implements INewService<DeptYQ,Integer> {
	@Autowired
	private DeptYQDao deptYQDao;

	@Override
	public Boolean add(DeptYQ deptYQ) throws Exception {
		Integer i = deptYQDao.insert(deptYQ);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(DeptYQ deptYQ) throws Exception {
		Integer prikey = null;
		Integer i = deptYQDao.insertPrikey(deptYQ);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = deptYQ.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<DeptYQ> list) throws Exception {
		Integer i = deptYQDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(DeptYQ deptYQ) throws Exception {
		Integer i = deptYQDao.delete(deptYQ);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = deptYQDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = deptYQDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(DeptYQ deptYQ) throws Exception {
		Integer i = deptYQDao.update(deptYQ);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(DeptYQ deptYQ) throws Exception {
		Integer i = deptYQDao.updateEmpty(deptYQ);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public DeptYQ getById(Integer id) throws Exception {
		return deptYQDao.findById(id);
	}

	@Override
	public DeptYQ getByField(DeptYQ deptYQ) throws Exception {
		return deptYQDao.findByField(deptYQ);
	}

	@Override
	public List<DeptYQ> getAll(DeptYQ deptYQ) throws Exception {
		return deptYQDao.findAll(deptYQ);
	}

	@Override
	public Map<String, Object> getPagingData(DeptYQ deptYQ) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = deptYQDao.findPagingCount(deptYQ);
		List<DeptYQ> list = deptYQDao.findPagingData(deptYQ);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<DeptYQ> getTreeAll(DeptYQ deptYQ) throws Exception {
		return deptYQDao.findTreeData(deptYQ);
	}
}
