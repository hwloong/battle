package com.hbsoft.wechat.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.wechat.dao.BusInfoDao;
import com.hbsoft.wechat.bean.BusInfo;

@Service
public class BusInfoDaoService implements INewService<BusInfo,Integer> {
	@Autowired
	private BusInfoDao busInfoDao;

	@Override
	public Boolean add(BusInfo busInfo) throws Exception {
		Integer i = busInfoDao.insert(busInfo);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(BusInfo busInfo) throws Exception {
		Integer prikey = null;
		Integer i = busInfoDao.insertPrikey(busInfo);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = busInfo.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<BusInfo> list) throws Exception {
		Integer i = busInfoDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(BusInfo busInfo) throws Exception {
		Integer i = busInfoDao.delete(busInfo);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = busInfoDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = busInfoDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(BusInfo busInfo) throws Exception {
		Integer i = busInfoDao.update(busInfo);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(BusInfo busInfo) throws Exception {
		Integer i = busInfoDao.updateEmpty(busInfo);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public BusInfo getById(Integer id) throws Exception {
		return busInfoDao.findById(id);
	}

	@Override
	public BusInfo getByField(BusInfo busInfo) throws Exception {
		return busInfoDao.findByField(busInfo);
	}

	@Override
	public List<BusInfo> getAll(BusInfo busInfo) throws Exception {
		return busInfoDao.findAll(busInfo);
	}

	@Override
	public Map<String, Object> getPagingData(BusInfo busInfo) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = busInfoDao.findPagingCount(busInfo);
		List<BusInfo> list = busInfoDao.findPagingData(busInfo);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<BusInfo> getTreeAll(BusInfo busInfo) throws Exception {
		return busInfoDao.findTreeData(busInfo);
	}
}
