package com.hbsoft.wechat.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.wechat.dao.WXUserLoginInfoDao;
import com.hbsoft.wechat.bean.WXUserLoginInfo;

@Service
public class WXUserLoginInfoDaoService implements INewService<WXUserLoginInfo,Integer> {
	@Autowired
	private WXUserLoginInfoDao wXUserLoginInfoDao;

	@Override
	public Boolean add(WXUserLoginInfo wXUserLoginInfo) throws Exception {
		Integer i = wXUserLoginInfoDao.insert(wXUserLoginInfo);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(WXUserLoginInfo wXUserLoginInfo) throws Exception {
		Integer prikey = null;
		Integer i = wXUserLoginInfoDao.insertPrikey(wXUserLoginInfo);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = wXUserLoginInfo.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<WXUserLoginInfo> list) throws Exception {
		Integer i = wXUserLoginInfoDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(WXUserLoginInfo wXUserLoginInfo) throws Exception {
		Integer i = wXUserLoginInfoDao.delete(wXUserLoginInfo);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = wXUserLoginInfoDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = wXUserLoginInfoDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(WXUserLoginInfo wXUserLoginInfo) throws Exception {
		Integer i = wXUserLoginInfoDao.update(wXUserLoginInfo);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(WXUserLoginInfo wXUserLoginInfo) throws Exception {
		Integer i = wXUserLoginInfoDao.updateEmpty(wXUserLoginInfo);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public WXUserLoginInfo getById(Integer id) throws Exception {
		return wXUserLoginInfoDao.findById(id);
	}

	@Override
	public WXUserLoginInfo getByField(WXUserLoginInfo wXUserLoginInfo) throws Exception {
		return wXUserLoginInfoDao.findByField(wXUserLoginInfo);
	}

	@Override
	public List<WXUserLoginInfo> getAll(WXUserLoginInfo wXUserLoginInfo) throws Exception {
		return wXUserLoginInfoDao.findAll(wXUserLoginInfo);
	}

	@Override
	public Map<String, Object> getPagingData(WXUserLoginInfo wXUserLoginInfo) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = wXUserLoginInfoDao.findPagingCount(wXUserLoginInfo);
		List<WXUserLoginInfo> list = wXUserLoginInfoDao.findPagingData(wXUserLoginInfo);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<WXUserLoginInfo> getTreeAll(WXUserLoginInfo wXUserLoginInfo) throws Exception {
		return wXUserLoginInfoDao.findTreeData(wXUserLoginInfo);
	}
}
