package com.hbsoft.wechat.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.wechat.dao.DeptBindUserDao;
import com.hbsoft.wechat.bean.DeptBindUser;

@Service
public class DeptBindUserDaoService implements INewService<DeptBindUser,Integer> {
	@Autowired
	private DeptBindUserDao deptBindUserDao;

	@Override
	public Boolean add(DeptBindUser deptBindUser) throws Exception {
		Integer i = deptBindUserDao.insert(deptBindUser);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(DeptBindUser deptBindUser) throws Exception {
		Integer prikey = null;
		Integer i = deptBindUserDao.insertPrikey(deptBindUser);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = deptBindUser.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<DeptBindUser> list) throws Exception {
		Integer i = deptBindUserDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(DeptBindUser deptBindUser) throws Exception {
		Integer i = deptBindUserDao.delete(deptBindUser);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = deptBindUserDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = deptBindUserDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(DeptBindUser deptBindUser) throws Exception {
		Integer i = deptBindUserDao.update(deptBindUser);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(DeptBindUser deptBindUser) throws Exception {
		Integer i = deptBindUserDao.updateEmpty(deptBindUser);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public DeptBindUser getById(Integer id) throws Exception {
		return deptBindUserDao.findById(id);
	}

	@Override
	public DeptBindUser getByField(DeptBindUser deptBindUser) throws Exception {
		return deptBindUserDao.findByField(deptBindUser);
	}

	@Override
	public List<DeptBindUser> getAll(DeptBindUser deptBindUser) throws Exception {
		return deptBindUserDao.findAll(deptBindUser);
	}

	@Override
	public Map<String, Object> getPagingData(DeptBindUser deptBindUser) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = deptBindUserDao.findPagingCount(deptBindUser);
		List<DeptBindUser> list = deptBindUserDao.findPagingData(deptBindUser);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<DeptBindUser> getTreeAll(DeptBindUser deptBindUser) throws Exception {
		return deptBindUserDao.findTreeData(deptBindUser);
	}
}
