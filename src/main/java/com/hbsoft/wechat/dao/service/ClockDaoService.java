package com.hbsoft.wechat.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.wechat.dao.ClockDao;
import com.hbsoft.wechat.bean.Clock;

@Service
public class ClockDaoService implements INewService<Clock,Integer> {
	@Autowired
	private ClockDao clockDao;

	@Override
	public Boolean add(Clock clock) throws Exception {
		Integer i = clockDao.insert(clock);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(Clock clock) throws Exception {
		Integer prikey = null;
		Integer i = clockDao.insertPrikey(clock);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = clock.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<Clock> list) throws Exception {
		Integer i = clockDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(Clock clock) throws Exception {
		Integer i = clockDao.delete(clock);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = clockDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = clockDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(Clock clock) throws Exception {
		Integer i = clockDao.update(clock);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(Clock clock) throws Exception {
		Integer i = clockDao.updateEmpty(clock);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Clock getById(Integer id) throws Exception {
		return clockDao.findById(id);
	}

	@Override
	public Clock getByField(Clock clock) throws Exception {
		return clockDao.findByField(clock);
	}

	@Override
	public List<Clock> getAll(Clock clock) throws Exception {
		return clockDao.findAll(clock);
	}

	@Override
	public Map<String, Object> getPagingData(Clock clock) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = clockDao.findPagingCount(clock);
		List<Clock> list = clockDao.findPagingData(clock);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<Clock> getTreeAll(Clock clock) throws Exception {
		return clockDao.findTreeData(clock);
	}
}
