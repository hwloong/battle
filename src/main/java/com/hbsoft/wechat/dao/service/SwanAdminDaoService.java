package com.hbsoft.wechat.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.wechat.dao.SwanAdminDao;
import com.hbsoft.wechat.bean.SwanAdmin;

@Service
public class SwanAdminDaoService implements INewService<SwanAdmin,Integer> {
	@Autowired
	private SwanAdminDao swanAdminDao;

	@Override
	public Boolean add(SwanAdmin swanAdmin) throws Exception {
		Integer i = swanAdminDao.insert(swanAdmin);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(SwanAdmin swanAdmin) throws Exception {
		Integer prikey = null;
		Integer i = swanAdminDao.insertPrikey(swanAdmin);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = swanAdmin.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<SwanAdmin> list) throws Exception {
		Integer i = swanAdminDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(SwanAdmin swanAdmin) throws Exception {
		Integer i = swanAdminDao.delete(swanAdmin);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = swanAdminDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = swanAdminDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(SwanAdmin swanAdmin) throws Exception {
		Integer i = swanAdminDao.update(swanAdmin);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(SwanAdmin swanAdmin) throws Exception {
		Integer i = swanAdminDao.updateEmpty(swanAdmin);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public SwanAdmin getById(Integer id) throws Exception {
		return swanAdminDao.findById(id);
	}

	@Override
	public SwanAdmin getByField(SwanAdmin swanAdmin) throws Exception {
		return swanAdminDao.findByField(swanAdmin);
	}

	@Override
	public List<SwanAdmin> getAll(SwanAdmin swanAdmin) throws Exception {
		return swanAdminDao.findAll(swanAdmin);
	}

	@Override
	public Map<String, Object> getPagingData(SwanAdmin swanAdmin) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = swanAdminDao.findPagingCount(swanAdmin);
		List<SwanAdmin> list = swanAdminDao.findPagingData(swanAdmin);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<SwanAdmin> getTreeAll(SwanAdmin swanAdmin) throws Exception {
		return swanAdminDao.findTreeData(swanAdmin);
	}
}
