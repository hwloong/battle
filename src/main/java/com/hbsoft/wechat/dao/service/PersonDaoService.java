package com.hbsoft.wechat.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.wechat.dao.PersonDao;
import com.hbsoft.wechat.bean.Person;

@Service
public class PersonDaoService implements INewService<Person,Integer> {
	@Autowired
	private PersonDao personDao;

	@Override
	public Boolean add(Person person) throws Exception {
		Integer i = personDao.insert(person);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(Person person) throws Exception {
		Integer prikey = null;
		Integer i = personDao.insertPrikey(person);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = person.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<Person> list) throws Exception {
		Integer i = personDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(Person person) throws Exception {
		Integer i = personDao.delete(person);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = personDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = personDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(Person person) throws Exception {
		Integer i = personDao.update(person);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(Person person) throws Exception {
		Integer i = personDao.updateEmpty(person);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Person getById(Integer id) throws Exception {
		return personDao.findById(id);
	}

	@Override
	public Person getByField(Person person) throws Exception {
		return personDao.findByField(person);
	}

	@Override
	public List<Person> getAll(Person person) throws Exception {
		return personDao.findAll(person);
	}

	@Override
	public Map<String, Object> getPagingData(Person person) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = personDao.findPagingCount(person);
		List<Person> list = personDao.findPagingData(person);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<Person> getTreeAll(Person person) throws Exception {
		return personDao.findTreeData(person);
	}
}
