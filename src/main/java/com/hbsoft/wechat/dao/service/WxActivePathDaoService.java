package com.hbsoft.wechat.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.wechat.dao.WxActivePathDao;
import com.hbsoft.wechat.bean.WxActivePath;

@Service
public class WxActivePathDaoService implements INewService<WxActivePath,Integer> {
	@Autowired
	private WxActivePathDao wxActivePathDao;

	@Override
	public Boolean add(WxActivePath wxActivePath) throws Exception {
		Integer i = wxActivePathDao.insert(wxActivePath);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(WxActivePath wxActivePath) throws Exception {
		Integer prikey = null;
		Integer i = wxActivePathDao.insertPrikey(wxActivePath);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = wxActivePath.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<WxActivePath> list) throws Exception {
		Integer i = wxActivePathDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(WxActivePath wxActivePath) throws Exception {
		Integer i = wxActivePathDao.delete(wxActivePath);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = wxActivePathDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = wxActivePathDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(WxActivePath wxActivePath) throws Exception {
		Integer i = wxActivePathDao.update(wxActivePath);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(WxActivePath wxActivePath) throws Exception {
		Integer i = wxActivePathDao.updateEmpty(wxActivePath);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public WxActivePath getById(Integer id) throws Exception {
		return wxActivePathDao.findById(id);
	}

	@Override
	public WxActivePath getByField(WxActivePath wxActivePath) throws Exception {
		return wxActivePathDao.findByField(wxActivePath);
	}

	@Override
	public List<WxActivePath> getAll(WxActivePath wxActivePath) throws Exception {
		return wxActivePathDao.findAll(wxActivePath);
	}

	@Override
	public Map<String, Object> getPagingData(WxActivePath wxActivePath) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = wxActivePathDao.findPagingCount(wxActivePath);
		List<WxActivePath> list = wxActivePathDao.findPagingData(wxActivePath);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<WxActivePath> getTreeAll(WxActivePath wxActivePath) throws Exception {
		return wxActivePathDao.findTreeData(wxActivePath);
	}
}
