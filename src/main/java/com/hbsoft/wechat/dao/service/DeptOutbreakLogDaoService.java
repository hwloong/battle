package com.hbsoft.wechat.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.wechat.dao.DeptOutbreakLogDao;
import com.hbsoft.wechat.bean.DeptOutbreakLog;

@Service
public class DeptOutbreakLogDaoService implements INewService<DeptOutbreakLog,Integer> {
	@Autowired
	private DeptOutbreakLogDao deptOutbreakLogDao;

	@Override
	public Boolean add(DeptOutbreakLog deptOutbreakLog) throws Exception {
		Integer i = deptOutbreakLogDao.insert(deptOutbreakLog);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(DeptOutbreakLog deptOutbreakLog) throws Exception {
		Integer prikey = null;
		Integer i = deptOutbreakLogDao.insertPrikey(deptOutbreakLog);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = deptOutbreakLog.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<DeptOutbreakLog> list) throws Exception {
		Integer i = deptOutbreakLogDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(DeptOutbreakLog deptOutbreakLog) throws Exception {
		Integer i = deptOutbreakLogDao.delete(deptOutbreakLog);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = deptOutbreakLogDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = deptOutbreakLogDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(DeptOutbreakLog deptOutbreakLog) throws Exception {
		Integer i = deptOutbreakLogDao.update(deptOutbreakLog);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(DeptOutbreakLog deptOutbreakLog) throws Exception {
		Integer i = deptOutbreakLogDao.updateEmpty(deptOutbreakLog);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public DeptOutbreakLog getById(Integer id) throws Exception {
		return deptOutbreakLogDao.findById(id);
	}

	@Override
	public DeptOutbreakLog getByField(DeptOutbreakLog deptOutbreakLog) throws Exception {
		return deptOutbreakLogDao.findByField(deptOutbreakLog);
	}

	@Override
	public List<DeptOutbreakLog> getAll(DeptOutbreakLog deptOutbreakLog) throws Exception {
		return deptOutbreakLogDao.findAll(deptOutbreakLog);
	}

	@Override
	public Map<String, Object> getPagingData(DeptOutbreakLog deptOutbreakLog) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = deptOutbreakLogDao.findPagingCount(deptOutbreakLog);
		List<DeptOutbreakLog> list = deptOutbreakLogDao.findPagingData(deptOutbreakLog);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<DeptOutbreakLog> getTreeAll(DeptOutbreakLog deptOutbreakLog) throws Exception {
		return deptOutbreakLogDao.findTreeData(deptOutbreakLog);
	}
}
