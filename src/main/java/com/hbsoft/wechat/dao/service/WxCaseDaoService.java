package com.hbsoft.wechat.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.wechat.dao.WxCaseDao;
import com.hbsoft.wechat.bean.WxCase;

@Service
public class WxCaseDaoService implements INewService<WxCase,Integer> {
	@Autowired
	private WxCaseDao wxCaseDao;

	@Override
	public Boolean add(WxCase wxCase) throws Exception {
		Integer i = wxCaseDao.insert(wxCase);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(WxCase wxCase) throws Exception {
		Integer prikey = null;
		Integer i = wxCaseDao.insertPrikey(wxCase);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = wxCase.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<WxCase> list) throws Exception {
		Integer i = wxCaseDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(WxCase wxCase) throws Exception {
		Integer i = wxCaseDao.delete(wxCase);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = wxCaseDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = wxCaseDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(WxCase wxCase) throws Exception {
		Integer i = wxCaseDao.update(wxCase);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(WxCase wxCase) throws Exception {
		Integer i = wxCaseDao.updateEmpty(wxCase);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public WxCase getById(Integer id) throws Exception {
		return wxCaseDao.findById(id);
	}

	@Override
	public WxCase getByField(WxCase wxCase) throws Exception {
		return wxCaseDao.findByField(wxCase);
	}

	@Override
	public List<WxCase> getAll(WxCase wxCase) throws Exception {
		return wxCaseDao.findAll(wxCase);
	}


	@Override
	public Map<String, Object> getPagingData(WxCase wxCase) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = wxCaseDao.findPagingCount(wxCase);
		List<WxCase> list = wxCaseDao.findPagingData(wxCase);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<WxCase> getTreeAll(WxCase wxCase) throws Exception {
		return wxCaseDao.findTreeData(wxCase);
	}
}
