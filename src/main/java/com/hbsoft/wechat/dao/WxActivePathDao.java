package com.hbsoft.wechat.dao;

import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.wechat.bean.WxActivePath;

@Mapper
public interface WxActivePathDao extends INewDao<WxActivePath,Integer> {
}
