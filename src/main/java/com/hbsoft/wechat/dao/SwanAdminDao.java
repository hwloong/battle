package com.hbsoft.wechat.dao;

import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.wechat.bean.SwanAdmin;

@Mapper
public interface SwanAdminDao extends INewDao<SwanAdmin,Integer> {
}
