package com.hbsoft.wechat.dao;

import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.wechat.bean.DeptOutbreakLog;

@Mapper
public interface DeptOutbreakLogDao extends INewDao<DeptOutbreakLog,Integer> {
}
