package com.hbsoft.wechat.dao;

import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.wechat.bean.PerOrgan;

@Mapper
public interface PerOrganDao extends INewDao<PerOrgan,Integer> {
}
