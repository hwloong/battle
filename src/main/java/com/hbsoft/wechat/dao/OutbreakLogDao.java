package com.hbsoft.wechat.dao;

import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.wechat.bean.OutbreakLog;

@Mapper
public interface OutbreakLogDao extends INewDao<OutbreakLog,Integer> {
}
