package com.hbsoft.wechat.dao;

import com.hbsoft.wechat.bean.Person;
import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.wechat.bean.Organize;

import java.util.List;

@Mapper
public interface OrganizeDao extends INewDao<Organize,Integer> {
    List<Organize> findOrganByOpendId(Organize organize);
}
