package com.hbsoft.wechat.dao;

import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.wechat.bean.Swan;

@Mapper
public interface SwanDao extends INewDao<Swan,Integer> {
}
