package com.hbsoft.wechat.dao;

import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.wechat.bean.WxBattleRealTimeData;

@Mapper
public interface WxBattleRealTimeDataDao extends INewDao<WxBattleRealTimeData,Integer> {
}
