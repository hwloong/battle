package com.hbsoft.wechat.dao;

import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.wechat.bean.DeptYQ;

@Mapper
public interface DeptYQDao extends INewDao<DeptYQ,Integer> {
}
