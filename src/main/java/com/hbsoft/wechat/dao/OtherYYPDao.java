package com.hbsoft.wechat.dao;

import com.hbsoft.wechat.bean.SwanLog;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Mapper
public interface OtherYYPDao {
    /**
     * 获取我加入的部门信息
     *
     * @param openId 微信id
     * @return
     * @throws Exception
     */
    List<Map<String, String>> findMyDeptYQAll(@Param("openId") String openId) throws Exception;

    /**
     * 获取我的部门情况统计
     * @param deptId 部门id
     * @param date 日期
     * @return
     * @throws Exception
     */
    List<Map<String,Object>> findMyDeptYQStatistical(@Param("deptId") Integer deptId,@Param("date") Date date) throws Exception;

    /**
     * 获取我的部门情况统计(近一周情况)
     * @param deptId 部门id
     * @param date 日期
     * @return
     * @throws Exception
     */
    List<Map<String,Object>> findMyDeptYQStatistical1(@Param("deptId") Integer deptId,@Param("date") Date date) throws Exception;

    /**
     * 获取我的下级部门列表
     * @param deptNo 部门编号
     * @return
     * @throws Exception
     */
    List<Map<String,Object>> findNextDeptYQ(@Param("deptNo") String deptNo) throws Exception;

    /**
     * 获取首页数据信息
     * @return
     * @throws Exception
     */
    Map<String,Object> findIndexData() throws Exception;

    /**
     * 获取下级疫情日报汇总
     * @param dateStr 日期字符串
     * @param date 日期
     * @param deptNo 部门编号
     * @return
     * @throws Exception
     */
    List<Map<String,Object>> findNextDeptYQSummary(@Param("dateStr") String dateStr,@Param("date") Date date,@Param("deptNo") String deptNo) throws Exception;

    /**
     * 获取下级疫情概览查询
     * @param deptNo 部门编号
     * @return
     * @throws Exception
     */
    List<Map<String,Object>> findNextDeptYQSituation(@Param("deptNo") String deptNo) throws Exception;

    /**
     * 获取字典表数据
     * @param type 类型
     * @return
     * @throws Exception
     */
    List<Map<String,Object>> findReturnWorkDic(@Param("type") String type) throws Exception;

    /**
     * 添加用户信息返回主键
     * @param param
     * @return
     * @throws Exception
     */
    Integer insertUser(Map<String,Object> param) throws Exception;

    /**
     * 添加用户角色
     * @param roleId
     * @param userId
     * @return
     * @throws Exception
     */
    Integer insertUserRole(@Param("roleId") Integer roleId,@Param("userId") Integer userId) throws Exception;

    /**
     * 获取注册名称是否被
     * @param name
     * @return
     * @throws Exception
     */
    Integer findUserNameCount(@Param("name") String name) throws Exception;

    /**
     * 获取最后一次卡口打卡记录
     * @param swanId 卡口id
     * @param openId 微信id
     * @return
     * @throws Exception
     */
    SwanLog findUserLastSwanLog(@Param("swanId") Integer swanId,@Param("openId") String openId) throws Exception;

    /**
     *  根据卡口填写部门信息
     * @param swanId 卡口id
     * @param deptName 部门名称
     * @return
     * @throws Exception
     */
    List<Map<String,Object>> findSwanDeptName(@Param("swanId") Integer swanId,@Param("deptName") String deptName) throws Exception;
}
