package com.hbsoft.wechat.dao;

import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.wechat.bean.SwanLog;

@Mapper
public interface SwanLogDao extends INewDao<SwanLog,Integer> {
}
