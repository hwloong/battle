package com.hbsoft.wechat.dao;

import com.hbsoft.wechat.bean.Clock;
import com.hbsoft.wechat.bean.WxMapClock;
import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.wechat.bean.WxCase;

import java.util.List;

@Mapper
public interface WxCaseDao extends INewDao<WxCase,Integer> {
    List<WxCase> findAlls(WxCase wxCase);
    Integer findIsEndTimeClock(WxMapClock wxMapClock) throws Exception;
    Integer findEndClock(WxMapClock wxMapClock) throws Exception;
}
