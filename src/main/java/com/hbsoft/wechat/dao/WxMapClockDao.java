package com.hbsoft.wechat.dao;

import com.hbsoft.wechat.bean.WxCase;
import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.wechat.bean.WxMapClock;

import java.util.List;

@Mapper
public interface WxMapClockDao extends INewDao<WxMapClock,Integer> {
    List<WxMapClock> getAllsForWxMapClock(WxMapClock wxMapClock);
    WxMapClock getWxMapClockByOpenid(WxMapClock wxMapClock);
}
