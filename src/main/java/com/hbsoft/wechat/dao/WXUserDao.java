package com.hbsoft.wechat.dao;

import com.hbsoft.wechat.bean.*;
import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;

import java.util.List;
import java.util.Map;

@Mapper
public interface WXUserDao extends INewDao<WXUser,Integer> {
    List<WXUser> getOrganPerson(Organize organize);

    int findClockInInfoPagingCount(ImportUserClockInInfo importUserClockInInfo);

    List<ImportUserClockInInfo> findClockInInfoPagingData(ImportUserClockInInfo importUserClockInInfo);

    List<Map<String,Object>> findClockInInfoMapData(ImportUserClockInInfo importUserClockInInfo);

    List<Map<String,Object>> findSwanLogInfoMapData(SwanLog log);

    Map<String,Object> findHBUserByName(Map map);

    List<WxAppClockInfo> getClockInInfo(WxAppClockInfo wxAppClockInfo);
}
