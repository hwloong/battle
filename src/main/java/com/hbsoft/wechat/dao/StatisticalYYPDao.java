package com.hbsoft.wechat.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Mapper
public interface StatisticalYYPDao {
    /**
     * 组织日报
     *
     * @param organizeId 组织id
     * @param date       年月日
     * @return
     */
    List<Map<String, Integer>> findOrganizeDaily(@Param("organizeId") Integer organizeId, @Param("date") Date date);
}
