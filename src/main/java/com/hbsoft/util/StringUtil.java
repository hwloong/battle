package com.hbsoft.util;

import java.security.SecureRandom;

public class StringUtil {
	
	public static String hidePhone(String phone){
		if(null != phone && !"".equals(phone)){
			return phone.replaceAll("(\\d{3})\\d+(\\d{4})","$1****$2");
		}
		return null;
	}
	
	public static String hideIdCard(String idCard){
		if(null != idCard && !"".equals(idCard)){
			return idCard.replaceAll("(\\d{3})\\w+(\\w{4})","$1********$2");
		}
		return null;
	}

	public static String getCode(Integer num) {
		Integer code = Integer.valueOf(new SecureRandom().nextInt(999999));
		String zerofill = com.hb.util.StringUtil.getZerofill(num.intValue(), code.intValue());
		return zerofill;
	}
}
