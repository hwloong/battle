package com.hbsoft.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;


@Component
@Setter
@Getter
public class SysConfigInfo {

	@Value("${hb.taskFlag:false}")
	private boolean taskFlag;

	public static String uploadPath;
	
	@Value("${hb.wxapp.appid:}")
	private String appid;
	
	@Value("${hb.wxapp.appSecret:}")
	private String appSecret;

	@Value("${hb.wxapp.qrCodePath:}")
	private String qrCodePath;

	@Value("${hb.uploadPath:}")
	public void setUploadPath(String uploadPath) {
		SysConfigInfo.uploadPath = uploadPath;
	}

}
