package com.hbsoft.common.constant;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Constant {

    public static Map<String, String> tokenMap = new ConcurrentHashMap();

    public static Map<String,Object> battleMap = new ConcurrentHashMap<>();

    public static String qrCodetype_area = "小区";

    public static String qrCodetype_office = "写字楼";

    public static String qrCodetype_bus = "公交车";

    public static String clockHealth = "健康打卡";

}
