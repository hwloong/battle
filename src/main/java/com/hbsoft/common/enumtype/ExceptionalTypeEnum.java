package com.hbsoft.common.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum ExceptionalTypeEnum {
	
	EXCEPTIONAL_SIGN("1","积分安全码异常"),
	EXCEPTIONAL_SYNC("2","同步数据时积分异常"),
	EXCEPTIONAL_CONSUME("3","订单安全码异常");

	private static Map<String, ExceptionalTypeEnum> nameMap = new HashMap<String, ExceptionalTypeEnum>(10);
	private static Map<String, ExceptionalTypeEnum> codeMap = new HashMap<String, ExceptionalTypeEnum>(10);

	static {
		ExceptionalTypeEnum[] allValues = ExceptionalTypeEnum.values();
		for (ExceptionalTypeEnum obj : allValues) {
			nameMap.put(obj.getName(), obj);
			codeMap.put(obj.getCode(), obj);
		}
	}

	private String name;
	private String code;

	private ExceptionalTypeEnum(String code, String name) {
		this.name = name;
		this.code = code;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static ExceptionalTypeEnum parseByName(String name) {
		return nameMap.get(name);
	}

	public static ExceptionalTypeEnum parseByCode(String code) {
		return codeMap.get(code);
	}

	@Override
	public String toString() {
		return this.name;
	}
}
