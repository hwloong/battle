package com.hbsoft.common.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum GiftReserveStatusEnum {
	
	GIFT_RESERVE_CLIENT("1","客户预约"),
	GIFT_RESERVE_CANCEL("2","客户已取消"),
	GIFT_RESERVE_CS_OVERTIME("3","客服延长时间"),
	GIFT_RESERVE_CS("4","客服确认");	

	private static Map<String, GiftReserveStatusEnum> nameMap = new HashMap<String, GiftReserveStatusEnum>(10);
	private static Map<String, GiftReserveStatusEnum> codeMap = new HashMap<String, GiftReserveStatusEnum>(10);

	static {
		GiftReserveStatusEnum[] allValues = GiftReserveStatusEnum.values();
		for (GiftReserveStatusEnum obj : allValues) {
			nameMap.put(obj.getName(), obj);
			codeMap.put(obj.getCode(), obj);
		}
	}

	private String name;
	private String code;

	private GiftReserveStatusEnum(String code, String name) {
		this.name = name;
		this.code = code;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static GiftReserveStatusEnum parseByName(String name) {
		return nameMap.get(name);
	}

	public static GiftReserveStatusEnum parseByCode(String code) {
		return codeMap.get(code);
	}

	@Override
	public String toString() {
		return this.name;
	}
}
