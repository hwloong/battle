package com.hbsoft.common.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum LocationGiftTypeEnum {
	
	LOCATION_GIFT_HQ("1","总部"),
	LOCATION_GIFT_BRANCH("2","分部");	

	private static Map<String, LocationGiftTypeEnum> nameMap = new HashMap<String, LocationGiftTypeEnum>(10);
	private static Map<String, LocationGiftTypeEnum> codeMap = new HashMap<String, LocationGiftTypeEnum>(10);

	static {
		LocationGiftTypeEnum[] allValues = LocationGiftTypeEnum.values();
		for (LocationGiftTypeEnum obj : allValues) {
			nameMap.put(obj.getName(), obj);
			codeMap.put(obj.getCode(), obj);
		}
	}

	private String name;
	private String code;

	private LocationGiftTypeEnum(String code, String name) {
		this.name = name;
		this.code = code;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static LocationGiftTypeEnum parseByName(String name) {
		return nameMap.get(name);
	}

	public static LocationGiftTypeEnum parseByCode(String code) {
		return codeMap.get(code);
	}

	@Override
	public String toString() {
		return this.name;
	}
}
