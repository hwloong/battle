package com.hbsoft.common.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum GiftWarehouseTypeEnum {
	
	GIFT_IN_WAREHOUSE_HQ("1","总部入库"),
	GIFT_OUT_WAREHOUSE_HQ("2","总部出库"),
	GIFT_IN_WAREHOUSE_BRANCH("3","分部入库"),
	GIFT_OUT_WAREHOUSE_BRANCH("4","分部出库"),
	GIFT_BACK_WAREHOUSE_BRANCH("5","分部退回"),
	GIFT_BACK_WAREHOUSE_BRANCH_ZON("6","总部驳回"),
	GIFT_IN_WAREHOUSE_CS("7","客户取消兑换入库");
	

	private static Map<String, GiftWarehouseTypeEnum> nameMap = new HashMap<String, GiftWarehouseTypeEnum>(10);
	private static Map<String, GiftWarehouseTypeEnum> codeMap = new HashMap<String, GiftWarehouseTypeEnum>(10);

	static {
		GiftWarehouseTypeEnum[] allValues = GiftWarehouseTypeEnum.values();
		for (GiftWarehouseTypeEnum obj : allValues) {
			nameMap.put(obj.getName(), obj);
			codeMap.put(obj.getCode(), obj);
		}
	}

	private String name;
	private String code;

	private GiftWarehouseTypeEnum(String code, String name) {
		this.name = name;
		this.code = code;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static GiftWarehouseTypeEnum parseByName(String name) {
		return nameMap.get(name);
	}

	public static GiftWarehouseTypeEnum parseByCode(String code) {
		return codeMap.get(code);
	}

	@Override
	public String toString() {
		return this.name;
	}
}
