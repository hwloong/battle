package com.hbsoft.common.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum HandleStatusEnum {
	
	HANDLE_NO("1","未处理"),
	HANDLE_YES("2","已处理");

	private static Map<String, HandleStatusEnum> nameMap = new HashMap<String, HandleStatusEnum>(10);
	private static Map<String, HandleStatusEnum> codeMap = new HashMap<String, HandleStatusEnum>(10);

	static {
		HandleStatusEnum[] allValues = HandleStatusEnum.values();
		for (HandleStatusEnum obj : allValues) {
			nameMap.put(obj.getName(), obj);
			codeMap.put(obj.getCode(), obj);
		}
	}

	private String name;
	private String code;

	private HandleStatusEnum(String code, String name) {
		this.name = name;
		this.code = code;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static HandleStatusEnum parseByName(String name) {
		return nameMap.get(name);
	}

	public static HandleStatusEnum parseByCode(String code) {
		return codeMap.get(code);
	}

	@Override
	public String toString() {
		return this.name;
	}
}
