package com.hbsoft.common.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum CustomerServerTypeEnum {
	
	CUSTOMERSERVER_BANK("1","银行客服"),
	CUSTOMERSERVER_STORE("2","商户客服"),
	CUSTOMERSERVER_BOSS_STORE_BOSS("3","商户");
	private static Map<String, CustomerServerTypeEnum> nameMap = new HashMap<String, CustomerServerTypeEnum>(10);
	private static Map<String, CustomerServerTypeEnum> codeMap = new HashMap<String, CustomerServerTypeEnum>(10);

	static {
		CustomerServerTypeEnum[] allValues = CustomerServerTypeEnum.values();
		for (CustomerServerTypeEnum obj : allValues) {
			nameMap.put(obj.getName(), obj);
			codeMap.put(obj.getCode(), obj);
		}
	}

	private String name;
	private String code;

	private CustomerServerTypeEnum(String code, String name) {
		this.name = name;
		this.code = code;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static CustomerServerTypeEnum parseByName(String name) {
		return nameMap.get(name);
	}

	public static CustomerServerTypeEnum parseByCode(String code) {
		return codeMap.get(code);
	}

	@Override
	public String toString() {
		return this.name;
	}
}
