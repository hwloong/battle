package com.hbsoft.common.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum HandleTypeEnum {
	
	HANDLE_COVER("1","覆盖"),
	HANDLE_CS("2","人工处理");

	private static Map<String, HandleTypeEnum> nameMap = new HashMap<String, HandleTypeEnum>(10);
	private static Map<String, HandleTypeEnum> codeMap = new HashMap<String, HandleTypeEnum>(10);

	static {
		HandleTypeEnum[] allValues = HandleTypeEnum.values();
		for (HandleTypeEnum obj : allValues) {
			nameMap.put(obj.getName(), obj);
			codeMap.put(obj.getCode(), obj);
		}
	}

	private String name;
	private String code;

	private HandleTypeEnum(String code, String name) {
		this.name = name;
		this.code = code;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static HandleTypeEnum parseByName(String name) {
		return nameMap.get(name);
	}

	public static HandleTypeEnum parseByCode(String code) {
		return codeMap.get(code);
	}

	@Override
	public String toString() {
		return this.name;
	}
}
