package com.hbsoft.common.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum GiftMailStatusEnum {
	
	GIFT_MAIL_NO("1","待邮寄"),
	GIFT_MAIL_CANCEL("2","客户已取消"),
	GIFT_MAIL_YES("3","已邮寄"),
	GIFT_MAIL_CHECKING("4","审核中");

	private static Map<String, GiftMailStatusEnum> nameMap = new HashMap<String, GiftMailStatusEnum>(10);
	private static Map<String, GiftMailStatusEnum> codeMap = new HashMap<String, GiftMailStatusEnum>(10);

	static {
		GiftMailStatusEnum[] allValues = GiftMailStatusEnum.values();
		for (GiftMailStatusEnum obj : allValues) {
			nameMap.put(obj.getName(), obj);
			codeMap.put(obj.getCode(), obj);
		}
	}

	private String name;
	private String code;

	private GiftMailStatusEnum(String code, String name) {
		this.name = name;
		this.code = code;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static GiftMailStatusEnum parseByName(String name) {
		return nameMap.get(name);
	}

	public static GiftMailStatusEnum parseByCode(String code) {
		return codeMap.get(code);
	}

	@Override
	public String toString() {
		return this.name;
	}
}
