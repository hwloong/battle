package com.hbsoft.common.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum LocationTypeEnum {
	
	Location_BANK("1","银行"),
	Location_STORE("2","商户");
	private static Map<String, LocationTypeEnum> nameMap = new HashMap<String, LocationTypeEnum>(10);
	private static Map<String, LocationTypeEnum> codeMap = new HashMap<String, LocationTypeEnum>(10);

	static {
		LocationTypeEnum[] allValues = LocationTypeEnum.values();
		for (LocationTypeEnum obj : allValues) {
			nameMap.put(obj.getName(), obj);
			codeMap.put(obj.getCode(), obj);
		}
	}

	private String name;
	private String code;

	private LocationTypeEnum(String code, String name) {
		this.name = name;
		this.code = code;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static LocationTypeEnum parseByName(String name) {
		return nameMap.get(name);
	}

	public static LocationTypeEnum parseByCode(String code) {
		return codeMap.get(code);
	}

	@Override
	public String toString() {
		return this.name;
	}
}
