package com.hbsoft.common.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum IntegralSysnStatusEnum {

	INTEGRAL_SYSN_STATUS_FILE_MAKE("1","备份文件生成"),
	INTEGRAL_SYSN_STATUS_FILE_FINISH("2","备份文件生成");

	private static Map<String, IntegralSysnStatusEnum> nameMap = new HashMap<String, IntegralSysnStatusEnum>(10);
	private static Map<String, IntegralSysnStatusEnum> codeMap = new HashMap<String, IntegralSysnStatusEnum>(10);

	static {
		IntegralSysnStatusEnum[] allValues = IntegralSysnStatusEnum.values();
		for (IntegralSysnStatusEnum obj : allValues) {
			nameMap.put(obj.getName(), obj);
			codeMap.put(obj.getCode(), obj);
		}
	}

	private String name;
	private String code;

	private IntegralSysnStatusEnum(String code, String name) {
		this.name = name;
		this.code = code;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static IntegralSysnStatusEnum parseByName(String name) {
		return nameMap.get(name);
	}

	public static IntegralSysnStatusEnum parseByCode(String code) {
		return codeMap.get(code);
	}

	@Override
	public String toString() {
		return this.name;
	}
}
