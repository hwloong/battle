package com.hbsoft.common.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum SyncStatusEnum {
	
	SYNC_WAIT("0","等待同步"),
	SYNC_YES("1","同步成功"),
	SYNC_CALLBACK("2","待回调");

	private static Map<String, SyncStatusEnum> nameMap = new HashMap<String, SyncStatusEnum>(10);
	private static Map<String, SyncStatusEnum> codeMap = new HashMap<String, SyncStatusEnum>(10);

	static {
		SyncStatusEnum[] allValues = SyncStatusEnum.values();
		for (SyncStatusEnum obj : allValues) {
			nameMap.put(obj.getName(), obj);
			codeMap.put(obj.getCode(), obj);
		}
	}

	private String name;
	private String code;

	private SyncStatusEnum(String code, String name) {
		this.name = name;
		this.code = code;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static SyncStatusEnum parseByName(String name) {
		return nameMap.get(name);
	}

	public static SyncStatusEnum parseByCode(String code) {
		return codeMap.get(code);
	}

	@Override
	public String toString() {
		return this.name;
	}
}
