package com.hbsoft.common.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum ReportTypeEnum {

	REPORT_TYPE_DAILY("0","日报"),
	REPORT_TYPE_MONTH("1","月报"),
	REPORT_TYPE_YEAR("2","年报");

	private static Map<String, ReportTypeEnum> nameMap = new HashMap<String, ReportTypeEnum>(10);
	private static Map<String, ReportTypeEnum> codeMap = new HashMap<String, ReportTypeEnum>(10);

	static {
		ReportTypeEnum[] allValues = ReportTypeEnum.values();
		for (ReportTypeEnum obj : allValues) {
			nameMap.put(obj.getName(), obj);
			codeMap.put(obj.getCode(), obj);
		}
	}

	private String name;
	private String code;

	private ReportTypeEnum(String code, String name) {
		this.name = name;
		this.code = code;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static ReportTypeEnum parseByName(String name) {
		return nameMap.get(name);
	}

	public static ReportTypeEnum parseByCode(String code) {
		return codeMap.get(code);
	}

	@Override
	public String toString() {
		return this.name;
	}
}
