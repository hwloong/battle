package com.hbsoft.common.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum IntegralTypeEnum {

	INTEGRALTYPE_UNFINISHED_CANCEL("3","已取消"),
	CONSUMESTATUS_REVOKE_CANCEL("5","已撤销"),
	INTEGRALTYPE_COST("9", "兑换积分"),
	INTEGRALTYPE_CANCEL("6","冲正积分");

	private static Map<String, IntegralTypeEnum> nameMap = new HashMap<String, IntegralTypeEnum>(10);
	private static Map<String, IntegralTypeEnum> codeMap = new HashMap<String, IntegralTypeEnum>(10);

	static {
		IntegralTypeEnum[] allValues = IntegralTypeEnum.values();
		for (IntegralTypeEnum obj : allValues) {
			nameMap.put(obj.getName(), obj);
			codeMap.put(obj.getCode(), obj);
		}
	}

	private String name;
	private String code;

	private IntegralTypeEnum(String code, String name) {
		this.name = name;
		this.code = code;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static IntegralTypeEnum parseByName(String name) {
		return nameMap.get(name);
	}

	public static IntegralTypeEnum parseByCode(String code) {
		return codeMap.get(code);
	}

	@Override
	public String toString() {
		return this.name;
	}
}
