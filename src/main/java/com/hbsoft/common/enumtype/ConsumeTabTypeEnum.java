package com.hbsoft.common.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum ConsumeTabTypeEnum {
	
	CONSUMESTATUS_UNFINISHED("1","未完成"),
	CONSUMESTATUS_FINISHED("2","已完成");

	private static Map<String, ConsumeTabTypeEnum> nameMap = new HashMap<String, ConsumeTabTypeEnum>(10);
	private static Map<String, ConsumeTabTypeEnum> codeMap = new HashMap<String, ConsumeTabTypeEnum>(10);

	static {
		ConsumeTabTypeEnum[] allValues = ConsumeTabTypeEnum.values();
		for (ConsumeTabTypeEnum obj : allValues) {
			nameMap.put(obj.getName(), obj);
			codeMap.put(obj.getCode(), obj);
		}
	}

	private String name;
	private String code;

	private ConsumeTabTypeEnum(String code, String name) {
		this.name = name;
		this.code = code;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static ConsumeTabTypeEnum parseByName(String name) {
		return nameMap.get(name);
	}

	public static ConsumeTabTypeEnum parseByCode(String code) {
		return codeMap.get(code);
	}

	@Override
	public String toString() {
		return this.name;
	}
}
