package com.hbsoft.common.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum QRCodeTypeEnum {
	
	QRCODE_UERID("QRCODE_UERID","用户编号"),
	QRCODE_ORDERID("QRCODE_ORDERID","订单号");

	private static Map<String, QRCodeTypeEnum> nameMap = new HashMap<String, QRCodeTypeEnum>(10);
	private static Map<String, QRCodeTypeEnum> codeMap = new HashMap<String, QRCodeTypeEnum>(10);

	static {
		QRCodeTypeEnum[] allValues = QRCodeTypeEnum.values();
		for (QRCodeTypeEnum obj : allValues) {
			nameMap.put(obj.getName(), obj);
			codeMap.put(obj.getCode(), obj);
		}
	}

	private String name;
	private String code;

	private QRCodeTypeEnum(String code, String name) {
		this.name = name;
		this.code = code;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static QRCodeTypeEnum parseByName(String name) {
		return nameMap.get(name);
	}

	public static QRCodeTypeEnum parseByCode(String code) {
		return codeMap.get(code);
	}

	@Override
	public String toString() {
		return this.name;
	}
}
