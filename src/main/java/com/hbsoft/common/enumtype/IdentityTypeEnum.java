package com.hbsoft.common.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum IdentityTypeEnum {

	IDENTITY_CLIENT("1", "客户"),
	IDENTITY_CS("2","客服");
	

	private static Map<String, IdentityTypeEnum> nameMap = new HashMap<String, IdentityTypeEnum>(10);
	private static Map<String, IdentityTypeEnum> codeMap = new HashMap<String, IdentityTypeEnum>(10);

	static {
		IdentityTypeEnum[] allValues = IdentityTypeEnum.values();
		for (IdentityTypeEnum obj : allValues) {
			nameMap.put(obj.getName(), obj);
			codeMap.put(obj.getCode(), obj);
		}
	}

	private String name;
	private String code;

	private IdentityTypeEnum(String code, String name) {
		this.name = name;
		this.code = code;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static IdentityTypeEnum parseByName(String name) {
		return nameMap.get(name);
	}

	public static IdentityTypeEnum parseByCode(String code) {
		return codeMap.get(code);
	}

	@Override
	public String toString() {
		return this.name;
	}
}
