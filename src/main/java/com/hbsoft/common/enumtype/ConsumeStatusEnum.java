package com.hbsoft.common.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum ConsumeStatusEnum {
	
	CONSUMESTATUS_UNFINISHED("1","未完成"),
	CONSUMESTATUS_FINISHED("2","已完成"),
	CONSUMESTATUS_UNFINISHED_CANCEL("3","已取消"),
	CONSUMESTATUS_REVOKE_CANCEL("5","已撤销"),
	CONSUMESTATUS_CONSUME("9","兑换积分"),
	CONSUMESTATUS_CANCELED("7","已冲正"),
	CONSUMESTATUS_CANCEL("6","冲正积分");

    private static Map<String, ConsumeStatusEnum> nameMap = new HashMap<String, ConsumeStatusEnum>(10);
	private static Map<String, ConsumeStatusEnum> codeMap = new HashMap<String, ConsumeStatusEnum>(10);

	static {
		ConsumeStatusEnum[] allValues = ConsumeStatusEnum.values();
		for (ConsumeStatusEnum obj : allValues) {
			nameMap.put(obj.getName(), obj);
			codeMap.put(obj.getCode(), obj);
		}
	}

	private String name;
	private String code;

	private ConsumeStatusEnum(String code, String name) {
		this.name = name;
		this.code = code;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static ConsumeStatusEnum parseByName(String name) {
		return nameMap.get(name);
	}

	public static ConsumeStatusEnum parseByCode(String code) {
		return codeMap.get(code);
	}

	@Override
	public String toString() {
		return this.name;
	}
}
