package com.hbsoft.common.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum ActivityTypeEnum {

	ACTIVITY_TYPE_NO("1","不存在"),
	BUSINESSTYPE_NORMAL("2","正常"),
	BUSINESSTYPE_CANCEL("3","销户");

	private static Map<String, ActivityTypeEnum> nameMap = new HashMap<String, ActivityTypeEnum>(10);
	private static Map<String, ActivityTypeEnum> codeMap = new HashMap<String, ActivityTypeEnum>(10);

	static {
		ActivityTypeEnum[] allValues = ActivityTypeEnum.values();
		for (ActivityTypeEnum obj : allValues) {
			nameMap.put(obj.getName(), obj);
			codeMap.put(obj.getCode(), obj);
		}
	}

	private String name;
	private String code;

	private ActivityTypeEnum(String code, String name) {
		this.name = name;
		this.code = code;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static ActivityTypeEnum parseByName(String name) {
		return nameMap.get(name);
	}

	public static ActivityTypeEnum parseByCode(String code) {
		return codeMap.get(code);
	}

	@Override
	public String toString() {
		return this.name;
	}
}
