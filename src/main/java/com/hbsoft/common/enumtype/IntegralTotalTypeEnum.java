package com.hbsoft.common.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum IntegralTotalTypeEnum {
	
	INTEGRALTOTALTYPE_CREATE("1","产生积分"),
	INTEGRALTOTALTYPE_USE("2","使用");	

	private static Map<String, IntegralTotalTypeEnum> nameMap = new HashMap<String, IntegralTotalTypeEnum>(10);
	private static Map<String, IntegralTotalTypeEnum> codeMap = new HashMap<String, IntegralTotalTypeEnum>(10);

	static {
		IntegralTotalTypeEnum[] allValues = IntegralTotalTypeEnum.values();
		for (IntegralTotalTypeEnum obj : allValues) {
			nameMap.put(obj.getName(), obj);
			codeMap.put(obj.getCode(), obj);
		}
	}

	private String name;
	private String code;

	private IntegralTotalTypeEnum(String code, String name) {
		this.name = name;
		this.code = code;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static IntegralTotalTypeEnum parseByName(String name) {
		return nameMap.get(name);
	}

	public static IntegralTotalTypeEnum parseByCode(String code) {
		return codeMap.get(code);
	}

	@Override
	public String toString() {
		return this.name;
	}
}
