package com.hbsoft.common.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum IdentifyingStatusEnum {

	IDENTIFYING_NO("1", "未认证，验证码已发送"),
	IDENTIFYING_SUCCESS("2","认证成功"),
	IDENTIFYING_FAIL("3","认证失败");


	private static Map<String, IdentifyingStatusEnum> nameMap = new HashMap<String, IdentifyingStatusEnum>(10);
	private static Map<String, IdentifyingStatusEnum> codeMap = new HashMap<String, IdentifyingStatusEnum>(10);

	static {
		IdentifyingStatusEnum[] allValues = IdentifyingStatusEnum.values();
		for (IdentifyingStatusEnum obj : allValues) {
			nameMap.put(obj.getName(), obj);
			codeMap.put(obj.getCode(), obj);
		}
	}

	private String name;
	private String code;

	private IdentifyingStatusEnum(String code, String name) {
		this.name = name;
		this.code = code;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static IdentifyingStatusEnum parseByName(String name) {
		return nameMap.get(name);
	}

	public static IdentifyingStatusEnum parseByCode(String code) {
		return codeMap.get(code);
	}

	@Override
	public String toString() {
		return this.name;
	}
}
