package com.hbsoft.common.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum GetGiftTypeEnum {
	
	GETGIFYTYPE_CS("1","客户自取"),
	GETGIFYTYPE_RESERVE("2","客户预约"),
	GETGIFYTYPE_MAIL("3","客户邮寄"),
	GETGIFYTYPE_BANK_CS("4","银行客服"),
	GETGIFYTYPE_STORE_CS("5","商户客服");
	private static Map<String, GetGiftTypeEnum> nameMap = new HashMap<String, GetGiftTypeEnum>(10);
	private static Map<String, GetGiftTypeEnum> codeMap = new HashMap<String, GetGiftTypeEnum>(10);

	static {
		GetGiftTypeEnum[] allValues = GetGiftTypeEnum.values();
		for (GetGiftTypeEnum obj : allValues) {
			nameMap.put(obj.getName(), obj);
			codeMap.put(obj.getCode(), obj);
		}
	}

	private String name;
	private String code;

	private GetGiftTypeEnum(String code, String name) {
		this.name = name;
		this.code = code;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static GetGiftTypeEnum parseByName(String name) {
		return nameMap.get(name);
	}

	public static GetGiftTypeEnum parseByCode(String code) {
		return codeMap.get(code);
	}

	@Override
	public String toString() {
		return this.name;
	}
}
