package com.hbsoft.common.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum BusinessTypeEnum {
	
	BUSINESSTYPE_CS("1","客服查询"),
	BUSINESSTYPE_CLIENT("2","客户查询");

	private static Map<String, BusinessTypeEnum> nameMap = new HashMap<String, BusinessTypeEnum>(10);
	private static Map<String, BusinessTypeEnum> codeMap = new HashMap<String, BusinessTypeEnum>(10);

	static {
		BusinessTypeEnum[] allValues = BusinessTypeEnum.values();
		for (BusinessTypeEnum obj : allValues) {
			nameMap.put(obj.getName(), obj);
			codeMap.put(obj.getCode(), obj);
		}
	}

	private String name;
	private String code;

	private BusinessTypeEnum(String code, String name) {
		this.name = name;
		this.code = code;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static BusinessTypeEnum parseByName(String name) {
		return nameMap.get(name);
	}

	public static BusinessTypeEnum parseByCode(String code) {
		return codeMap.get(code);
	}

	@Override
	public String toString() {
		return this.name;
	}
}
